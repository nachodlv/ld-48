// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LD48 : ModuleRules
{
	public LD48(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"HeadMountedDisplay",
			"GameplayAbilities",
			"GameplayTags",
			"GameplayTasks",
			"PMTFiregunWeapons",
			"PMTCommon",
			"PMTGasUtilities",
			"PMTAI",
			"AIModule",
		});
	}
}
