﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SpawnManager.generated.h"

class ATargetPoint;
class ALD48Character;

USTRUCT()
struct FAISpawnConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category="LD48")
	int32 StartingRound;
	UPROPERTY(EditAnywhere, Category="LD48")
	int32 MaxAmount;
	UPROPERTY(EditAnywhere, Category="LD48")
	TSubclassOf<ALD48Character> Character;
};

UCLASS()
class LD48_API ASpawnManager : public AActor
{
	GENERATED_BODY()

public:
	ASpawnManager();

protected:
	virtual void BeginPlay() override;

public:

private:
	UFUNCTION()
	void RoundStart(int32 NewRound);
	UFUNCTION()
	void RoundEnd();
	UFUNCTION()
	void SpawnEnemy(int32 CurrentRound);

	UFUNCTION()
	void EnemyAIDied(ALD48Character* Character);

	UPROPERTY()
	TArray<ATargetPoint*> TargetPoints;

	UPROPERTY()
	TArray<AActor*> ActorsSpawned;

	UPROPERTY(EditDefaultsOnly, Category="LD48")
	TArray<FAISpawnConfig> SpawnConfigs;

	UPROPERTY(EditDefaultsOnly, Category="LD48")
	int32 StartingSpawnAmount;

	UPROPERTY(EditDefaultsOnly, Category="LD48")
	float PercentageRoundIncrease;

	UPROPERTY(EditDefaultsOnly, Category="LD48")
	float TimeBetweenSpawns = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category="LD48")
	float TimeBetweenSpawnsVariance = 2.0f;

	int32 SpawnedInRound = 0.0f;

	int32 CurrentSpawnAmount;

	FTimerHandle SpawnHandle;
};
