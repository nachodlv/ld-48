﻿#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameplayTagContainer.h"
#include "LD48AIController.generated.h"

class UGameplayEffect;
class ALD48Character;

UCLASS()
class LD48_API ALD48AIController : public AAIController
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALD48AIController();

	virtual void OnPossess(APawn* InPawn) override;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void PawnDied(ALD48Character* InPaw);


private:
	FGameplayTag AssaultRifleTag;

	UPROPERTY(EditAnywhere, Category="LD48", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* AIBehavior;

	UPROPERTY(EditAnywhere, Category="LD48", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UGameplayEffect> RoundGameplayEffect;
};
