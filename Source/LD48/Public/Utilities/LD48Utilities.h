// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "GameplayTagContainer.h"

#include "LD48Utilities.generated.h"

UENUM(BlueprintType)
enum class ELD48InputId : uint8
{
	Fire = 0,
	Jump = 1,
	Reload = 2,
	Crouch = 3,
	Ironsight = 4,
	ChangeWeapon = 5
};

/**
 * 
 */
UCLASS()
class LD48_API ULD48Utilities : public UObject
{
	GENERATED_BODY()

public:

	
	
};
