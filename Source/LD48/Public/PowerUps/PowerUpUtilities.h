// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "GameplayEffect.h"

#include "PowerUpUtilities.generated.h"

USTRUCT(BlueprintType)
struct FLD48PowerupDescriptor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText PowerupName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Level = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UGameplayEffect> GameplayEffectClass;

	friend bool operator==(const FLD48PowerupDescriptor& Lhs, const FLD48PowerupDescriptor& Rhs)
	{
		return Lhs.PowerupName.EqualTo(Rhs.PowerupName) && Lhs.Level == Rhs.Level;
	}

	friend bool operator!=(const FLD48PowerupDescriptor& Lhs, const FLD48PowerupDescriptor& Rhs)
	{
		return !(Lhs == Rhs);
	}
};

UENUM(BlueprintType)
enum class EPowerUpType : uint8
{
	Character = 0,
	Rifle = 1,
	Shotgun = 2
};

USTRUCT(BlueprintType)
struct FLD48ActivePowerup
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLD48PowerupDescriptor Descriptor;
	UPROPERTY(BlueprintReadWrite)
	FActiveGameplayEffectHandle SpecHandle;
	UPROPERTY(BlueprintReadWrite)
	EPowerUpType PowerupType = EPowerUpType::Character;

	friend bool operator==(const FLD48ActivePowerup& Lhs, const FLD48ActivePowerup& Rhs)
	{
		return Lhs.Descriptor == Rhs.Descriptor;
	}

	friend bool operator!=(const FLD48ActivePowerup& Lhs, const FLD48ActivePowerup& Rhs)
	{
		return !(Lhs == Rhs);
	}
};
