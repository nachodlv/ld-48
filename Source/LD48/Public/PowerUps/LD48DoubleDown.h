﻿#pragma once

#include "CoreMinimal.h"

#include "LD48PowerUp.h"

#include "GameFramework/Actor.h"
#include "LD48DoubleDown.generated.h"

UCLASS()
class LD48_API ALD48DoubleDown : public ALD48PowerUp
{
	GENERATED_BODY()

public:
	ALD48DoubleDown();

	virtual void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                                     UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                                     const FHitResult& SweepResult) override;

};
