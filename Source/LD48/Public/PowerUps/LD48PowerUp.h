// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "PowerUps/PowerUpUtilities.h"

#include "LD48PowerUp.generated.h"

class ALD48Character;
class ALD48PowerUp;
class UBoxComponent;
class UStaticMeshComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FLD48OnPowerupOverlap, ALD48PowerUp*, PowerUp, ALD48Character*, CharacterThatOverlapped);

UCLASS()
class LD48_API ALD48PowerUp : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALD48PowerUp();

	void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void SetPowerupDescriptor(FLD48PowerupDescriptor InDescriptor) { PowerupDescriptor = InDescriptor; }

	UFUNCTION()
	virtual void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION(BlueprintPure)
	const FLD48PowerupDescriptor& GetPowerupDescriptor() const;

	UFUNCTION(BlueprintCallable)
	void SetPowerUpLevel(int32 Level);

	UPROPERTY(BlueprintAssignable)
	FLD48OnPowerupOverlap OnPowerupOverlap;

protected:

	virtual void HandleOverlap(ALD48Character* InCharacter);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere)
	FLD48PowerupDescriptor PowerupDescriptor;

	UPROPERTY(EditAnywhere)
	FGameplayTag Cue;
};
