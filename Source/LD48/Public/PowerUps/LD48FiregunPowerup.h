// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PowerUps/LD48PowerUp.h"

#include "PowerUps/PowerUpUtilities.h"

#include "LD48FiregunPowerup.generated.h"

/**
 * 
 */
UCLASS()
class LD48_API ALD48FiregunPowerup : public ALD48PowerUp
{
	GENERATED_BODY()

protected:

	void HandleOverlap(ALD48Character* InCharacter) override;

	UPROPERTY(EditDefaultsOnly)
	EPowerUpType WeaponToAffect = EPowerUpType::Rifle;
	
};
