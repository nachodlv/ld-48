﻿#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "PowerUpSpawner.generated.h"

class ALD48PowerUp;
class ALD48Character;

UCLASS()
class LD48_API APowerUpSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APowerUpSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:

	UFUNCTION()
	void SpawnPowerUps();

	UFUNCTION()
	void PlayerDied(ALD48Character* Character);

	UFUNCTION()
	void PowerupGrabbed(ALD48PowerUp* PowerUp, ALD48Character* CharacterThatOverlapped);

	UFUNCTION()
	void StartNextRound();

	UFUNCTION()
	void DoubleDownGrabbed(ALD48PowerUp* PowerUp, ALD48Character* CharacterThatOverlapped);

	UPROPERTY()
	TArray<class ATargetPoint*> TargetPoints;

	UPROPERTY()
	ATargetPoint* DoubleDownSpawn;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<class ALD48PowerUp>> PowerUps;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ALD48DoubleDown> DoubleDown;

	UPROPERTY()
	TArray<ALD48PowerUp*> SpawnedPowerUps;

	UPROPERTY()
	ALD48DoubleDown* DoubleDownSpawned;

	int32 CurrentLevel = 1.0f;

	bool bPlayerBinded = false;
};
