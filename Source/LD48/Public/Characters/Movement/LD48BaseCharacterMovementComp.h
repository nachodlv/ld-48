// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "LD48BaseCharacterMovementComp.generated.h"

/**
 * 
 */
UCLASS()
class LD48_API ULD48BaseCharacterMovementComp : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	/** Get max speed from attributes. */
	float GetMaxSpeed() const override;
	
};
