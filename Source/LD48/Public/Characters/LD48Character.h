// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

// PMTFFW includes
#include "Interfaces/PMTFWFiregunProvider.h"

// GameplayAbilities includes
#include "AbilitySystemInterface.h"

#include "GameplayAbilities/Interfaces/MeleeMeshProvider.h"


#include "PowerUps/PowerUpUtilities.h"
#include "Utilities/PMTCCommonTypes.h"

#include "LD48Character.generated.h"

class ALD48Character;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterDie, ALD48Character*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBulletCountChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, int32, CurrentHealth, int32, MaxHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponChanged);

class UPMTFWFiregunTrackerComp;
class UPMTFWFiregunEquipComponent;
class UPMTFWAmmoBagComponent;
class UPMTGUAbilityRestorerComp;
class UPMTGUChainedAbilityRemovalComp;
struct FOnAttributeChangeData;

UCLASS(config=Game)
class ALD48Character : public ACharacter, public IPMTFWFiregunProvider, public IAbilitySystemInterface, public IMeleeMeshProvider
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ALD48Character(const FObjectInitializer& InObjectInitializer);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPMTFWAmmoBagComponent* GetAmmoBagComponent() const { return AmmoBagComponent; }

	virtual void PossessedBy(AController* NewController) override;
	void UnPossessed() override;

	FOnCharacterDie OnCharacterDie;

	// ~ Begin IAbilitySystemInterface

	/** Retrieve ASC used by the base character. */
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	// ~ End IAbilitySystemInterface

	// ~ Begin IPMTFWFiregunProvider

	void NotifyBulletCountChanged(const FPMTFWFiregunInstanceDataItem& ItemThatChanged) override;
	/** Check if there's any firegun equipped. */
	virtual bool HasAnyFiregunEquipped() const override;
	/** Get a mutable reference to the current equipped firegun. */
	virtual FPMTFWFiregunInstanceDataItem& GetMutableCurrentEquippedFiregun() override;
	/** Get a const reference to the current equipped firegun. */
	virtual const FPMTFWFiregunInstanceDataItem& GetCurrentEquippedFiregun() const override;
	/** Get the current physical representation in the world of the firegun. */
	virtual APMTFWFiregun* GetMutableCurrentPhysicalFiregun() const override;
	/** Get the proxy associated with the current equipped firegun. */
	virtual APMTFWActiveFiregunsProxy* GetMutableCurrentFiregunProxy() const override;

	// ~ End IPMTFWFiregunProvider

	void ModifyBulletCount(FGameplayTag AmmoTag, uint16 Bullets);
	/** Increase the amount of bullets considering the limit of the bag component. */
	void HandleAmmoPickup(FGameplayTag AmmoTag, float InValue);

	// ~ Begin IMeleeMeshProvider
	virtual USkeletalMeshComponent* GetMeleeMesh() override { return WeaponSkeletalMesh; }
	// ~ End IMeleeMeshProvider

	UFUNCTION(BlueprintImplementableEvent)
	void AfterBeginPlay();

	UFUNCTION(BlueprintPure)
	float GetWalkMaxSpeed() const;
	UFUNCTION(BlueprintPure)
	float GetCrouchMaxSpeed() const;
	UFUNCTION(BlueprintPure)
	float GetIronsightMaxSpeed() const;
	UFUNCTION(BlueprintPure)
	float GetCrouchIronsightMaxSpeed() const;

	UFUNCTION(BlueprintPure)
	bool CanMoveAtAll() const;
	/** Verifies if the character is crouched. */
	UFUNCTION(BlueprintPure)
	bool IsCrouched() const;
	/** Verifies if the character has ironsights. */
	UFUNCTION(BlueprintPure)
	bool HasIronsights() const;
	/** Verifies if the character is in a jumping state. */
	UFUNCTION(BlueprintPure)
	bool IsInJumpingState() const;

	UFUNCTION(BlueprintCallable)
	FLD48ActivePowerup RemoveRandomPowerup(bool &bRemoved);

	UPROPERTY(BlueprintAssignable)
	FBulletCountChanged OnBulletCountChanged;
	UPROPERTY(BlueprintAssignable)
	FOnHealthChanged OnHealthChanged;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponChanged OnWeaponChanged;

protected:

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnDie();

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void OnIronsightTagChanged(const FGameplayTag TagChanged, int32 NewCount);
	void OnCrouchTagChanged(const FGameplayTag TagChanged, int32 NewCount);

	UFUNCTION(BlueprintPure)
	void QueryCurrentFiregunAttribute(FGameplayTag& WeaponTag, int32& CurrentBulletsInMagazine, int32& BulletsInBag);

	UFUNCTION(BlueprintPure)
	void QueryHealth(int32& Health, int32& MaxHealth);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/** Applies the given powerup descriptor. */
	UFUNCTION(BlueprintCallable)
	void ApplyPowerup(FLD48PowerupDescriptor InPowerupDescriptor, EPowerUpType PowerupType = EPowerUpType::Character);

	UFUNCTION(BlueprintPure)
	const TArray<FLD48ActivePowerup>& GetAllActivePowerups() const { return ActivePowerups; }

	UPMTFWFiregunTrackerComp* GetFiregunTrackerComponent() const { return FiregunTrackerComponent; }

	UFUNCTION(BlueprintCallable)
	void Reset();

	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return bDie; }

private:
	void KillSelf();

	void HealthChanged(const FOnAttributeChangeData& Data);

	/** ASC cached from player state. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAbilitySystemComponent* AbilitySystemComponent;

	/** Component used to track the firegun instances we have. */
	UPROPERTY(VisibleAnywhere, Category = "Firegun")
	UPMTFWFiregunTrackerComp* FiregunTrackerComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPMTFWFiregunEquipComponent* FiregunEquipComponent;

	UPROPERTY(Transient, VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	APMTFWFiregun* EquippedFiregun = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPMTFWAmmoBagComponent* AmmoBagComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPMTGUAbilityRestorerComp* AbilityRestorerComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPMTGUChainedAbilityRemovalComp* ChainedAbilityRemovalComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<FPMTCTagAttribute> AmmoBullets;

	/** Class used for firegun physical representation. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<APMTFWFiregun> ActiveFiregunClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UGameplayEffect> ResetGameplayEffect;

	UPROPERTY()
	class ULD48AttributeSet* AttributeSet;

	FDelegateHandle CrouchingDelegateHandle;
	FDelegateHandle IronsightDelegateHandle;

	FDelegateHandle HealthChangedDelegateHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* WeaponSkeletalMesh;

	UPROPERTY(EditAnywhere)
	FName WeaponBoneAttach;

	UPROPERTY(EditAnywhere)
	FVector StartingLocation;

	// All active powerups we have.
	UPROPERTY(Transient)
	TArray<FLD48ActivePowerup> ActivePowerups;

	bool bDie = false;
};

