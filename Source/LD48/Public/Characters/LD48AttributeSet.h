﻿

#pragma once

#include "CoreMinimal.h"

#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "LD48AttributeSet.generated.h"


// Uses macros from AttributeSet.h
#define LD48_ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 *
 */
UCLASS()
class LD48_API ULD48AttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	/** Set default values. */
	ULD48AttributeSet();

	/** Reset the value of all the attributes to 0.0 so we can reapply gameplay effects that modified these attributes.
	* This should be called after all active gameplay effects were removed. */
	virtual void ResetAttributes();

	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;

	virtual bool PreGameplayEffectExecute(struct FGameplayEffectModCallbackData &Data) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Health;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, Health);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxHealth;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, MaxHealth);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Damage;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, Damage);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Stamina;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, Stamina);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxStamina;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, MaxStamina);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MovementSpeed;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, MovementSpeed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData CrouchMovementSpeed;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, CrouchMovementSpeed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData IronsightMovementSpeed;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, IronsightMovementSpeed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData CrouchIronsightMovementSpeed;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, CrouchIronsightMovementSpeed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData JumpHeight;
	LD48_ATTRIBUTE_ACCESSORS(ULD48AttributeSet, JumpHeight);
};
