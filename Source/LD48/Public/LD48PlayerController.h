﻿#pragma once

#include "CoreMinimal.h"

#include "GameplayTagContainer.h"
#include "Firegun/PMTFWFiregunUtilities.h"
#include "Firegun/PMTFWFiregunData.h"

#include "LD48PlayerController.generated.h"

class APMTFWActiveFiregunsProxy;

UCLASS()
class LD48_API ALD48PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALD48PlayerController();

	APMTFWActiveFiregunsProxy* GetProxyByHandle(const FPMTFWFiregunInstanceHandle& Handle);

protected:
	virtual void OnPossess(APawn* InPawn) override;

public:
	UFUNCTION(BlueprintCallable, Category = "LD48")
	FPMTFWFiregunInstanceHandle AddFiregunWeaponToCharacter(TSubclassOf<UPMTFWFiregunData> Data, const TArray<FPMTFWCurrentAmmoForBulletType>& InitialBullets);

	/** Add ammo for the given weapon handle. */
	UFUNCTION(BlueprintCallable, Category = "LD48")
    void AddAmmoToFiregunToCharacter(const FGameplayTag& AmmoTag, int32 Ammo);

	UFUNCTION(BlueprintCallable, Category = "LD48")
	void EquipFiregunInstance(const FPMTFWFiregunInstanceHandle& Handle);

	UFUNCTION(exec, BlueprintCallable, Category = "LD48", meta = (OverrideNativeName = "PMTFWFiregunWeapons.RemoveFiregunFromCharacter"))
    void RemoveFiregunWeaponFromCharacter(const FPMTFWFiregunInstanceHandle& Handle);

private:

    UPROPERTY(Transient, VisibleInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    TArray<APMTFWActiveFiregunsProxy*> ActiveFireguns;
};
