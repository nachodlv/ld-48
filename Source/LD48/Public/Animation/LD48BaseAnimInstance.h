// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"

#include "GameplayTagContainer.h"
#include "Runtime/Engine/Public/Animation/AnimInstanceProxy.h"

#include "LD48BaseAnimInstance.generated.h"

class ULD48BaseAnimInstance;
class ALD48Character;

USTRUCT(BlueprintType)
struct FLD48AnimSet
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBlendSpaceBase* IdleRunBlendSpace;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBlendSpaceBase* CrouchIdleRunBlendSpace;
	/** Blendspace used for idle-run animations with ironsights. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBlendSpaceBase* IdleRunIronsightBlendSpace;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBlendSpaceBase* CrouchIdleRunIronsightBlendSpace;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimSequenceBase* IdleAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimSequenceBase* IdleIronsightAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimSequenceBase* IdleCrouchAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimSequenceBase* IdleCrouchIronsightAnimation;
};

USTRUCT(BlueprintType)
struct FLD48BaseAnimInstanceProxy : public FAnimInstanceProxy
{
	GENERATED_USTRUCT_BODY()

	/**
	* Default constructor.
	*/
	FLD48BaseAnimInstanceProxy();

	/**
	* Gets the owner actor if it's exists from the animation instance.
	* @param Instance - Animation instance where this proxy will live.
	*/
	FLD48BaseAnimInstanceProxy(UAnimInstance* Instance);

	/**
	* Update all the variables related with the animated character.
	* @param DeltaSeconds - Unused.
	*/
	void Update(float DeltaSeconds) override;

public:

	/** Length of the velocity vector of the animated character.*/
	UPROPERTY(Transient, BlueprintReadOnly)
	float Speed = 0.0f;
	/** Boolean that it's true if the character is jumping. */
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bIsJumping = false;
	/** Boolean that it's true if the character is in the air. */
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bIsInAir = false;
	/** Whenever we should use the full body for the animation or not. */
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bFullBody = false;
	/** Blendspace used for the idle-run animations. */
	UPROPERTY(Transient, BlueprintReadOnly)
	FLD48AnimSet CurrentAnimSet;
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bIsIronsights = false;
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bIsCrouching = false;

protected:

	/**
	* Sets the animated character reference. This method can be called more than once.
	* @param InAnimInstance - Animation instance where this proxy will live.
	*/
	void Initialize(UAnimInstance* InAnimInstance) override;

	/** Cached reference of the animated character */
	UPROPERTY(Transient, BlueprintReadOnly)
	ALD48Character* AnimInstanceOwnedCharacter = nullptr;
	/** Store the cached pointer to the animation instance. */
	UPROPERTY(Transient, BlueprintReadOnly)
	ULD48BaseAnimInstance* CachedBaseAnimInstance = nullptr;
};

/**
 * 
 */
UCLASS()
class LD48_API ULD48BaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	/** Do not do anything since it's a member class. */
	void DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy) override;
	/** Empty implementation on purpose.
	* Make sure to not create anything new since the proxy is a member of this class.
	* @return Proxy member. */
	FAnimInstanceProxy* CreateAnimInstanceProxy() override;
	/** Retrieve current anim set. */
	FLD48AnimSet* GetCurrentAnimSet();

protected:

	UPROPERTY(EditAnywhere)
	TMap<FGameplayTag, FLD48AnimSet> AnimDataAssets;

	/** Default anim set in case there's no anim data asset available. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Proxy")
	FLD48AnimSet DefaultAnimSet;
	UPROPERTY(Transient, BlueprintReadOnly, Category = "Proxy")
	FLD48BaseAnimInstanceProxy BaseProxy;
	
	friend struct FLD48BaseAnimInstanceProxy;
};
