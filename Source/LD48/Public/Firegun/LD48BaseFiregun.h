// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Firegun/PMTFWFiregun.h"
#include "LD48BaseFiregun.generated.h"

/**
 * 
 */
UCLASS()
class LD48_API ALD48BaseFiregun : public APMTFWFiregun
{
	GENERATED_BODY()

public:

	ALD48BaseFiregun(const FObjectInitializer& InObjectInitializer);
	
};
