// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "LD48IronsightBaseAbility.generated.h"

class UAbilityTask_WaitInputPress;
class UAbilityTask_WaitInputRelease;

/**
 * 
 */
UCLASS()
class LD48_API ULD48IronsightBaseAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:

	ULD48IronsightBaseAbility();

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		bool bReplicateEndAbility, bool bWasCancelled) override;

protected:

	UFUNCTION()
	void OnInputRelease(float ElapsedTime);
	UFUNCTION()
	void OnInputPressed(float ElapsedTime);

	UPROPERTY(EditDefaultsOnly)
	bool bStopIronsightOnRelease = true;

	UPROPERTY(EditDefaultsOnly)
	float CameraSpringArmExtra = -200.0f;

private:

	UPROPERTY(Transient)
	UAbilityTask_WaitInputPress* WaitInputPressedTask = nullptr;
	UPROPERTY(Transient)
	UAbilityTask_WaitInputRelease* WaitInputReleaseTask = nullptr;
};
