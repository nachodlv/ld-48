// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "LD48ReloadAbility.generated.h"

class UAnimMontage;
class UAbilityTask_PlayMontageAndWait;

/**
 * 
 */
UCLASS()
class LD48_API ULD48ReloadAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:

	ULD48ReloadAbility();

	bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayTagContainer* SourceTags = nullptr,
		const FGameplayTagContainer* TargetTags = nullptr,
		OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* OwnerInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

protected:

	UFUNCTION()
	void OnReloadSucceeded();
	UFUNCTION()
	void OnReloadFailed();

	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* ReloadAnimation;

	UPROPERTY(Transient)
	UAbilityTask_PlayMontageAndWait* PlayMontageTask = nullptr;
	
};
