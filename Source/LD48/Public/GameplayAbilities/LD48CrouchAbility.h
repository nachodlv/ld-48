// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "LD48CrouchAbility.generated.h"

class UAbilityTask_WaitInputPress;
class UAbilityTask_WaitInputRelease;

/**
 * 
 */
UCLASS()
class LD48_API ULD48CrouchAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:

	ULD48CrouchAbility();

	bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayTagContainer* SourceTags = nullptr,
		const FGameplayTagContainer* TargetTags = nullptr,
		OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* OwnerInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		bool bReplicateEndAbility, bool bWasCancelled) override;

protected:

	UFUNCTION()
	void OnInputRelease(float ElapsedTime);
	UFUNCTION()
	void OnInputPressed(float ElapsedTime);

	UPROPERTY(Transient)
	UAbilityTask_WaitInputPress* WaitInputPressTask = nullptr;
	UPROPERTY(Transient)
	UAbilityTask_WaitInputRelease* WaitInputReleaseTask = nullptr;

	/** Whenever we end crouching when released or we wait for input press again. */
	UPROPERTY(EditDefaultsOnly)
	bool bEndCrouchWhenReleased = false;
	
};
