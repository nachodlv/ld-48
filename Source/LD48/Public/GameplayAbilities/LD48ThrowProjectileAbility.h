﻿#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "LD48ThrowProjectileAbility.generated.h"

/**
 *
 */
UCLASS()
class LD48_API ULD48ThrowProjectileAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	                     const FGameplayAbilityActorInfo* OwnerInfo,
	                     const FGameplayAbilityActivationInfo ActivationInfo,
	                     const FGameplayEventData* TriggerEventData) override;
protected:
	UFUNCTION()
	void EventReceived(FGameplayTag EventTag, FGameplayEventData EventData);

	UFUNCTION()
	void OnMontageEnded(FGameplayTag EventTag, FGameplayEventData EventData);

private:
	FVector GetTargetLocation() const;

	UPROPERTY(EditAnywhere, Category="LD48")
	UAnimMontage* AnimMontage;

	UPROPERTY(EditAnywhere, Category="LD48")
	TSubclassOf<AActor> Projectile;

	UPROPERTY(EditAnywhere, Category="LD48")
	FName SpawnSocket;

	UPROPERTY(EditAnywhere, Category="LD48")
	FVector Offset;

	UPROPERTY(EditAnywhere, Category="LD48")
	float Arc = 0.5f;
};
