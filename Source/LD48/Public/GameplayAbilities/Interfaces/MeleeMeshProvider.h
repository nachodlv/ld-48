﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MeleeMeshProvider.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UMeleeMeshProvider : public UInterface
{
	GENERATED_BODY()
};

class USkeletalMeshComponent;

/**
 *
 */
class LD48_API IMeleeMeshProvider
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual USkeletalMeshComponent* GetMeleeMesh() = 0;
};
