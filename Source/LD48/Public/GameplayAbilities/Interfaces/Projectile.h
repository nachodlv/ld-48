﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Projectile.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UProjectile : public UInterface
{
	GENERATED_BODY()
};

class UAbilitySystemComponent;

/**
 *
 */
class LD48_API IProjectile
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintImplementableEvent)
    void SetSourceAbilitySystemComponent(UAbilitySystemComponent* ASC);
};
