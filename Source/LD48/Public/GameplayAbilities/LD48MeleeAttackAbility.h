﻿#pragma once

#include "CoreMinimal.h"

#include "Abilities/GameplayAbility.h"

#include "UObject/Object.h"
#include "LD48MeleeAttackAbility.generated.h"

/**
 *
 */
UCLASS()
class LD48_API ULD48MeleeAttackAbility : public UGameplayAbility
{
	GENERATED_BODY()
public:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	                     const FGameplayAbilityActivationInfo ActivationInfo,
	                     const FGameplayEventData* TriggerEventData) override;

protected:
	UFUNCTION()
	void EventReceived(FGameplayTag EventTag, FGameplayEventData EventData);

	UFUNCTION()
	void OnMontageEnded(FGameplayTag EventTag, FGameplayEventData EventData);

private:
	UPROPERTY(EditAnywhere, Category="LD48")
	FName FromTraceSocket;

	UPROPERTY(EditAnywhere, Category="LD48")
	FName ToTraceSocket;

	UPROPERTY(EditAnywhere, Category="LD48")
	FName HitProfile;

	UPROPERTY(EditAnywhere, Category="LD48")
	UAnimMontage* AnimMontage;

	UPROPERTY(EditAnywhere, Category="LD48")
	TSubclassOf<UGameplayEffect> GameplayEffect;

	UPROPERTY(EditAnywhere, Category="LD48")
	FGameplayTag StartAttackingCue;

	UPROPERTY(EditAnywhere, Category="LD48")
	FGameplayTag HitCue;
};
