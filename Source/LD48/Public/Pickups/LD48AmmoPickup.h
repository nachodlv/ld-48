// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/LD48Pickup.h"
#include "LD48AmmoPickup.generated.h"

USTRUCT(BlueprintType)
struct FLD48AmmoRecover
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	FScalableFloat ValueAtLevel;
	UPROPERTY(EditAnywhere)
	FGameplayTag AmmoTag;
};

/**
 * 
 */
UCLASS()
class LD48_API ALD48AmmoPickup : public ALD48Pickup
{
	GENERATED_BODY()

protected:

	void HandleOverlap(ALD48Character* InCharacter) override;

	UPROPERTY(EditAnywhere)
	TArray<FLD48AmmoRecover> BulletsToGive;
	
};
