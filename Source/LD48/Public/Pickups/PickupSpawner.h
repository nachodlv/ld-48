﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupSpawner.generated.h"

class ALD48Pickup;
class ATargetPoint;

UCLASS()
class LD48_API APickupSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickupSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void DestroyPickUps();

	UFUNCTION()
	void SpawnPickUps(int32 Round);

	UPROPERTY(EditAnywhere)
	int32 QuantityOfSpawnsPerRound;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<ALD48Pickup>> PickupClasses;

	UPROPERTY()
	TArray<ALD48Pickup*> PickupsSpawned;

	UPROPERTY()
	TArray<ATargetPoint*> TargetPoints;
};
