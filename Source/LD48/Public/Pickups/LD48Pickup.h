// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "GameplayEffect.h"

#include "LD48Pickup.generated.h"

class ALD48Character;
class UBoxComponent;
class UStaticMeshComponent;
class ALD48Pickup;

UCLASS()
class LD48_API ALD48Pickup : public AActor
{
	GENERATED_BODY()

public:

	ALD48Pickup();

	UFUNCTION(BlueprintCallable)
	void SetEffectLevel(float InLevel) { EffectLevel = InLevel; }

protected:

	void BeginPlay() override;

	virtual void HandleOverlap(ALD48Character* InCharacter);

	virtual void ExecuteCue(UAbilitySystemComponent* ASC);

	UFUNCTION()
	void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
	float EffectLevel = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<UGameplayEffect>> GameplayEffectsToApply;
	UPROPERTY(EditAnywhere)
	FGameplayTag PickupCue;
};
