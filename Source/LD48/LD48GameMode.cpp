// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD48GameMode.h"
#include "UObject/ConstructorHelpers.h"

ALD48GameMode::ALD48GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/BP_MainCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ALD48GameMode::SetPlayer(ALD48Character* InPlayer)
{
	Player = InPlayer;
}

ALD48Character* ALD48GameMode::GetPlayer() const
{
	return Player;
}

void ALD48GameMode::RoundStart()
{
	++CurrentRound;
	OnRoundStart.Broadcast(CurrentRound);
}

void ALD48GameMode::RoundEnd()
{
	OnRoundEnd.Broadcast();
}
