// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameModeBase.h"
#include "LD48GameMode.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRoundStart, int32, Round);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRoundEnd);

class ALD48Character;

UCLASS(minimalapi)
class ALD48GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALD48GameMode();

	void SetPlayer(ALD48Character* InPlayer);

	UFUNCTION(BlueprintPure)
	ALD48Character* GetPlayer() const;

	UFUNCTION(BlueprintPure)
	int32 GetCurrentRound() const { return CurrentRound; }

	UFUNCTION(BlueprintCallable)
	void RoundStart();

	UFUNCTION(BlueprintCallable)
    void RoundEnd();

	UPROPERTY(BlueprintAssignable)
	FOnRoundStart OnRoundStart;

	UPROPERTY(BlueprintAssignable)
	FOnRoundEnd OnRoundEnd;

private:

	UPROPERTY(EditDefaultsOnly)
	int32 CurrentRound = 100;

	UPROPERTY()
	ALD48Character* Player;
};



