// Fill out your copyright notice in the Description page of Project Settings.


#include "Animation/LD48BaseAnimInstance.h"

// LD48 Includes
#include "Characters/LD48Character.h"

// UE Includes
#include "GameFramework/CharacterMovementComponent.h"

namespace
{
	/** Name of the curve to enable full body animations. */
	const FName NAME_FullBodyCurveName = "FullBody";
	/** Name of the curve to consider if we ignore the speed threshold
	* to trigger full body animations. */
	const FName NAME_IgnoreSpeedForFullBodyCurveName = "IgnoreSpeed";

	/** Value used to determine the threshold to consider if the player
	* has no movement at all so full body animations can be played.
	* If there's a curve with the name of NAME_IgnoreSpeedForFullBodyCurveName
	* then this threshold is ignored and only NAME_FullBodyCurveName is considered. */
	constexpr float FullBodySpeedThreshold = 2.0f;
}


FLD48BaseAnimInstanceProxy::FLD48BaseAnimInstanceProxy()
{}

FLD48BaseAnimInstanceProxy::FLD48BaseAnimInstanceProxy(UAnimInstance* Instance)
{
	AnimInstanceOwnedCharacter = Cast<ALD48Character>(Instance->GetOwningActor());
	CachedBaseAnimInstance = Cast<ULD48BaseAnimInstance>(Instance);
}

void FLD48BaseAnimInstanceProxy::Update(float DeltaSeconds)
{
	if (AnimInstanceOwnedCharacter)
	{
		const FVector Velocity = AnimInstanceOwnedCharacter->GetVelocity();
		UCharacterMovementComponent* CharacterMovement = AnimInstanceOwnedCharacter->GetCharacterMovement();
		Speed = Velocity.Size();
		bIsInAir = CharacterMovement && CharacterMovement->IsFalling();
		// If the player did press the jump button, then we can play the start jump animation.
		bIsJumping = AnimInstanceOwnedCharacter->IsInJumpingState();
		bIsIronsights = AnimInstanceOwnedCharacter->HasIronsights();
		bIsCrouching = AnimInstanceOwnedCharacter->IsCrouched();
		if (CachedBaseAnimInstance)
		{
			bFullBody = CachedBaseAnimInstance->GetCurveValue(NAME_FullBodyCurveName) > 0.0f;
			// We use 0.95f just to be sure that the curve value is picked up.
			// These curves must have a constant value of 1 (see metadata curves for animations).
			bFullBody = bFullBody || (Speed <= FullBodySpeedThreshold && CachedBaseAnimInstance->GetCurveValue(NAME_IgnoreSpeedForFullBodyCurveName) < 0.95f);
			CurrentAnimSet = *CachedBaseAnimInstance->GetCurrentAnimSet();
		}
	}
}

void FLD48BaseAnimInstanceProxy::Initialize(UAnimInstance* InAnimInstance)
{
	FAnimInstanceProxy::Initialize(InAnimInstance);
	AnimInstanceOwnedCharacter = Cast<ALD48Character>(InAnimInstance->GetOwningActor());
	CachedBaseAnimInstance = Cast<ULD48BaseAnimInstance>(InAnimInstance);
}


void ULD48BaseAnimInstance::DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy)
{
}

FAnimInstanceProxy* ULD48BaseAnimInstance::CreateAnimInstanceProxy()
{
	return &BaseProxy;
}

FLD48AnimSet* ULD48BaseAnimInstance::GetCurrentAnimSet()
{
	// TODO(Brian): Use asset.
	return &DefaultAnimSet;
}
