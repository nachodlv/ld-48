﻿#include "LD48PlayerController.h"


#include "ActorComponents/PMTFWAmmoBagComponent.h"
#include "ActorComponents/PMTFWFiregunEquipComponent.h"
#include "ActorComponents/PMTFWFiregunTrackerComp.h"
#include "ActorComponents/PMTGUAbilityRestorerComp.h"
#include "ActorComponents/PMTGUChainedAbilityRemovalComp.h"

#include "Characters/LD48Character.h"


#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregun.h"


class ALD48Character;

namespace
{
	APMTFWFiregun* GetFiregunFromActor(AActor* InActor)
	{
		if (InActor)
		{
			TArray<AActor*> OutActors;
			InActor->GetAttachedActors(OutActors);
			for (AActor* SingleActor : OutActors)
			{
				APMTFWFiregun* Firegun = Cast<APMTFWFiregun>(SingleActor);
				if (Firegun)
				{
					return Firegun;
				}
			}
		}
		return nullptr;
	}

}

// Sets default values
ALD48PlayerController::ALD48PlayerController()
{
}

void ALD48PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	ActiveFireguns = APMTFWActiveFiregunsProxy::CreateFiregunProxiesForPlayer(this, APMTFWActiveFiregunsProxy::StaticClass(), 4);
}

FPMTFWFiregunInstanceHandle ALD48PlayerController::AddFiregunWeaponToCharacter(TSubclassOf<UPMTFWFiregunData> Data, const TArray<FPMTFWCurrentAmmoForBulletType>& InitialBullets)
{
	if (!Data)
	{
		return FPMTFWFiregunInstanceHandle();
	}
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		UPMTFWFiregunTrackerComp* TrackerComponent = Cast<UPMTFWFiregunTrackerComp>(ControlledPawn->GetComponentByClass(UPMTFWFiregunTrackerComp::StaticClass()));
		if (TrackerComponent)
		{
			TUniquePtr<FPMTFWFiregunInstanceCreationParams> CreationParams(new FPMTFWFiregunInstanceCreationParams());
			CreationParams->FiregunDataClass = Data;
			CreationParams->UniqueSeed = 1; // TODO do we need a real seed?
			CreationParams->CurrentBulletsInMagazine = InitialBullets;
			FPMTFWFiregunInstanceHandle Handle = TrackerComponent->AddFiregunInstance(MoveTemp(CreationParams));
			UE_LOG(LogTemp, Warning, TEXT("Added firegun '%s' to character '%s' with handle num: '%d'"), *Data->GetName(), *ControlledPawn->GetName(), Handle.Handle);

			APMTFWActiveFiregunsProxy* Proxy = GetProxyByHandle(Handle);
			if (!Proxy)
			{
				for (APMTFWActiveFiregunsProxy* ActiveFiregun : ActiveFireguns)
				{
					if (!ActiveFiregun->DoesProxyContainFiregunInstance())
					{
						Proxy = ActiveFiregun;
						break;
					}
				}
				if (Proxy)
				{
					UAbilitySystemComponent* OwnerASC = Cast<UAbilitySystemComponent>(ControlledPawn->GetComponentByClass(UPMTFWFiregunTrackerComp::StaticClass()));
					Proxy->ChangeOrUpdateFiregunInstance(OwnerASC, TrackerComponent->GetInstanceDataItem(Handle), TrackerComponent);
				}
			}
			return Handle;
		}
	}
	return FPMTFWFiregunInstanceHandle();
}

void ALD48PlayerController::AddAmmoToFiregunToCharacter(const FGameplayTag& AmmoTag, int32 Ammo)
{
	if (!AmmoTag.IsValid())
	{
		return;
	}
	ALD48Character* ControlledPawn = Cast<ALD48Character>(GetPawn());
	if (ControlledPawn)
	{
		if (UPMTFWAmmoBagComponent* AmmoBagComponent = ControlledPawn->GetAmmoBagComponent())
		{
			AmmoBagComponent->IncrementBulletCount(AmmoTag, Ammo);
		}
	}
}

void ALD48PlayerController::EquipFiregunInstance(const FPMTFWFiregunInstanceHandle& Handle)
{
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		// TODO - we can cast to LD48Character and use the getters
		UAbilitySystemComponent* OwnerASC = ControlledPawn->FindComponentByClass<UAbilitySystemComponent>();
		UPMTFWFiregunTrackerComp* TrackerComp = ControlledPawn->FindComponentByClass<UPMTFWFiregunTrackerComp>();
		UPMTFWFiregunEquipComponent* EquipComponent = ControlledPawn->FindComponentByClass<UPMTFWFiregunEquipComponent>();
		UPMTGUAbilityRestorerComp* AbilityRestorerComp = ControlledPawn->FindComponentByClass<UPMTGUAbilityRestorerComp>();
		UPMTGUChainedAbilityRemovalComp* ChainedAbilityRemovalComp = ControlledPawn->FindComponentByClass<UPMTGUChainedAbilityRemovalComp>();
		ALD48Character* ControlledCharacter = Cast<ALD48Character>(ControlledPawn);
		if (!ControlledCharacter)
		{
			return;
		}
		APMTFWFiregun* Firegun = ControlledCharacter->GetMutableCurrentPhysicalFiregun();
		if (!OwnerASC || !TrackerComp || !EquipComponent || !AbilityRestorerComp || !Firegun || !ChainedAbilityRemovalComp || !TrackerComp->IsHandleValid(Handle))
		{
			return;
		}
		const FPMTFWFiregunInstanceDataItem& Instance = TrackerComp->GetInstanceDataItem(Handle);

		APMTFWActiveFiregunsProxy* Proxy = GetProxyByHandle(Handle);

		if (!Proxy)
		{
			for (APMTFWActiveFiregunsProxy* ActiveFiregun : ActiveFireguns)
			{
				if (!ActiveFiregun->DoesProxyContainFiregunInstance())
				{
					Proxy = ActiveFiregun;
					break;
				}
			}
			if (Proxy)
			{
				if (EquipComponent->HasAnyFiregunEquipped())
				{
					APMTFWActiveFiregunsProxy* ActiveProxy = GetProxyByHandle(EquipComponent->GetCurrentFiregunHandle());
					if (ActiveProxy)
					{
						ActiveProxy->UnEquipCurrentFiregunInstance(OwnerASC, AbilityRestorerComp, TArray<FActiveGameplayEffectHandle>());
					}
				}
				Proxy->ChangeOrUpdateFiregunInstance(OwnerASC, Instance, TrackerComp);
			}
			else
			{
				return;
			}
		}
		else
		{
			if (EquipComponent->HasAnyFiregunEquipped())
			{
				APMTFWActiveFiregunsProxy* ActiveProxy = GetProxyByHandle(EquipComponent->GetCurrentFiregunHandle());
				if (ActiveProxy)
				{
					ActiveProxy->UnEquipCurrentFiregunInstance(OwnerASC, AbilityRestorerComp, TArray<FActiveGameplayEffectHandle>());
				}
			}
		}
		Firegun->SetFiregunSkeletalMesh(Instance.FiregunDataClass.GetDefaultObject()->GetSkeletalMesh());
		EquipComponent->EquipFiregun(Handle);
		Proxy->EquipCurrentFiregunInstance(OwnerASC, AbilityRestorerComp, ChainedAbilityRemovalComp, TrackerComp);
		UE_LOG(LogTemp, Warning, TEXT("Weapon equipped successfully."));
	}
}

void ALD48PlayerController::RemoveFiregunWeaponFromCharacter(const FPMTFWFiregunInstanceHandle& Handle)
{
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		UPMTFWFiregunTrackerComp* TrackerComponent = Cast<UPMTFWFiregunTrackerComp>(ControlledPawn->GetComponentByClass(UPMTFWFiregunTrackerComp::StaticClass()));
		if (TrackerComponent)
		{
			if (TrackerComponent->IsHandleValid(Handle))
			{
				TrackerComponent->RemoveFiregunInstance(Handle);
				UE_LOG(LogTemp, Warning, TEXT("Removed firegun from character '%s' with handle num: '%d'"), *ControlledPawn->GetName(), Handle.Handle);
			}
		}
	}
}

APMTFWActiveFiregunsProxy* ALD48PlayerController::GetProxyByHandle(const FPMTFWFiregunInstanceHandle& Handle)
{
	for (APMTFWActiveFiregunsProxy* ActiveFiregun : ActiveFireguns)
	{
		if (ActiveFiregun->DoesProxyContainFiregunInstanceHandle(Handle))
		{
			return ActiveFiregun;
		}
	}
	return nullptr;
}
