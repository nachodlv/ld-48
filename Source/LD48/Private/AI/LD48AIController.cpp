﻿#include "AI/LD48AIController.h"

#include "AbilitySystemComponent.h"
#include "BrainComponent.h"
#include "LD48PlayerController.h"



#include "BehaviorTree/BlackboardComponent.h"

#include "Characters/LD48Character.h"


#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregunData.h"


#include "GameFramework/Character.h"

#include "Kismet/GameplayStatics.h"

#include "LD48/LD48GameMode.h"

// Sets default values
ALD48AIController::ALD48AIController()
{
}

void ALD48AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	RunBehaviorTree(AIBehavior);

	ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());

	if (UBlackboardComponent* BlackboardComponent = GetBlackboardComponent())
	{
		BlackboardComponent->SetValueAsObject(TEXT("Target"), GameMode->GetPlayer());
	}
	if (ALD48Character* CharacterPossessed = Cast<ALD48Character>(InPawn))
	{
		UAbilitySystemComponent* ASC = CharacterPossessed->GetAbilitySystemComponent();
		if (ASC)
		{
			const FGameplayEffectContextHandle Handle = ASC->MakeEffectContext();
			ASC->ApplyGameplayEffectToSelf(RoundGameplayEffect.GetDefaultObject(), GameMode->GetCurrentRound(), Handle);
		}

		CharacterPossessed->OnCharacterDie.AddDynamic(this, &ALD48AIController::PawnDied);
	}
}

void ALD48AIController::BeginPlay()
{
	Super::BeginPlay();
	AssaultRifleTag = FGameplayTag::RequestGameplayTag(FName("Firegun.AssaultRifle"));
}

void ALD48AIController::PawnDied(ALD48Character* InPaw)
{
	if (BrainComponent)
	{
		BrainComponent->StopLogic(FString(TEXT("Pawn died")));
	}

	ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
	USkeletalMeshComponent* MeshComponent = InPaw->GetMesh();
	InPaw->SetLifeSpan(40);

	if (GameMode && MeshComponent)
	{
		ALD48Character* Player = GameMode->GetPlayer();

		const FPMTFWFiregunInstanceDataItem Firegun = Player->GetCurrentEquippedFiregun();
		ALD48PlayerController* Controller = Cast<ALD48PlayerController>(Player->GetController());
		const FGameplayTag WeaponType = Firegun.FiregunDataClass.GetDefaultObject()->GetFiregunWeaponType();
		APMTFWActiveFiregunsProxy* Proxy = Controller->GetProxyByHandle(Firegun.Handle);
		float Force = Proxy->GetFiregunDamage() * 10000;
		if (WeaponType == AssaultRifleTag)
		{
			Force /= 10;
		}
		FVector Location;
		FRotator Rotation;
		Controller->GetPlayerViewPoint(Location, Rotation);
		const FVector Direction = Player->GetActorLocation() - Location;
		MeshComponent->AddImpulseToAllBodiesBelow(Direction.GetSafeNormal() * Force, TEXT("Bip001"));
	}
}

