﻿
#include "AI/SpawnManager.h"

#include "EngineUtils.h"

#include "Characters/LD48Character.h"

#include "Engine/TargetPoint.h"

#include "LD48/LD48GameMode.h"

ASpawnManager::ASpawnManager()
{
	bReplicates = false;
}

void ASpawnManager::BeginPlay()
{
	Super::BeginPlay();
	CurrentSpawnAmount = StartingSpawnAmount;
	for (TActorIterator<ATargetPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ATargetPoint* TargetPoint = *ActorItr;
		if (TargetPoint && TargetPoint->ActorHasTag(TEXT("AISpawn")))
		{
			TargetPoints.Add(TargetPoint);
		}
	}

	ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());

	GameMode->OnRoundStart.AddDynamic(this, &ASpawnManager::RoundStart);
	GameMode->OnRoundEnd.AddDynamic(this, &ASpawnManager::RoundEnd);

	// Validate the spawn config
	bool bHasValidSpawnConfig = false;
	for (FAISpawnConfig SpawnConfig : SpawnConfigs)
	{
		if (SpawnConfig.StartingRound <= 1)
		{
			bHasValidSpawnConfig = true;
		}
	}
	check(bHasValidSpawnConfig);
}

void ASpawnManager::RoundStart(int32 NewRound)
{
	SpawnedInRound = 0;
	SpawnEnemy(NewRound);
	CurrentSpawnAmount += CurrentSpawnAmount * PercentageRoundIncrease;
}

void ASpawnManager::RoundEnd()
{
	for (AActor* Spawned : ActorsSpawned)
	{
		Spawned->Destroy(); // TODO we might wanna call Die() or something like that
	}
	ActorsSpawned.Reset();
}

void ASpawnManager::SpawnEnemy(int32 CurrentRound)
{
	if (SpawnedInRound >= CurrentSpawnAmount)
	{
		return;
	}
	FAISpawnConfig* Config = nullptr;
	while (!Config)
	{
		const int32 RandomConfig = FMath::RandRange(0, SpawnConfigs.Num() - 1);
		FAISpawnConfig& CurrentConfig = SpawnConfigs[RandomConfig];
		if (CurrentConfig.StartingRound <= CurrentRound)
		{
			Config = &CurrentConfig;
		}
	}
	ATargetPoint* TargetPoint = TargetPoints[FMath::RandRange(0, TargetPoints.Num() - 1)];
	ALD48Character* SpawnActor = GetWorld()->SpawnActor<ALD48Character>(Config->Character, TargetPoint->GetTransform());
	if (SpawnActor)
	{
		++SpawnedInRound;
		SpawnActor->SpawnDefaultController();
		SpawnActor->OnCharacterDie.AddDynamic(this, &ASpawnManager::EnemyAIDied);
		ActorsSpawned.Add(SpawnActor);
	}

	// Spawn the next enemy
	const FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ASpawnManager::SpawnEnemy, CurrentRound);
	const float SpawnTime = FMath::FRandRange(TimeBetweenSpawns - TimeBetweenSpawnsVariance, TimeBetweenSpawns + TimeBetweenSpawnsVariance);
	GetWorld()->GetTimerManager().SetTimer(SpawnHandle, TimerDelegate, SpawnTime, false);
}

void ASpawnManager::EnemyAIDied(ALD48Character* Character)
{
	ActorsSpawned.Remove(Character);
	if (SpawnedInRound >= CurrentSpawnAmount && ActorsSpawned.Num() == 0)
	{
		ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
		GameMode->RoundEnd();
	}
}


