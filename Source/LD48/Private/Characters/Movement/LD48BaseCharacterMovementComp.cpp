// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/Movement/LD48BaseCharacterMovementComp.h"

#include "Characters/LD48Character.h"

float ULD48BaseCharacterMovementComp::GetMaxSpeed() const
{
	const ALD48Character* OwnerCharacter = Cast<ALD48Character>(GetCharacterOwner());
	if (!OwnerCharacter)
	{
		return Super::GetMaxSpeed();
	}
	if (!OwnerCharacter->CanMoveAtAll())
	{
		return 0.0f;
	}
	if (OwnerCharacter->IsCrouched())
	{
		if (OwnerCharacter->HasIronsights())
		{
			return OwnerCharacter->GetCrouchIronsightMaxSpeed();
		}
		return OwnerCharacter->GetCrouchMaxSpeed();
	}
	if (OwnerCharacter->HasIronsights())
	{
		return OwnerCharacter->GetIronsightMaxSpeed();
	}
	return OwnerCharacter->GetWalkMaxSpeed();
}
