﻿
#include "Characters/LD48AttributeSet.h"
#include "GameplayEffectExtension.h"

ULD48AttributeSet::ULD48AttributeSet()
{
	InitHealth(1.0f);
	InitMaxHealth(1.0f);
	InitDamage(0.0f);
	InitStamina(0.0f);
	InitMaxStamina(0.0f);
	InitMovementSpeed(600.0f);
	InitCrouchMovementSpeed(300.0f);
	InitIronsightMovementSpeed(450.0f);
	InitCrouchIronsightMovementSpeed(150.0f);
	InitJumpHeight(200.0f);
}

void ULD48AttributeSet::ResetAttributes()
{
	SetHealth(1.0f);
	SetMaxHealth(1.0f);
	SetDamage(0.0f);
	SetStamina(0.0f);
	SetMaxStamina(0.0f);
	SetMovementSpeed(600.0f);
	SetCrouchMovementSpeed(300.0f);
	SetIronsightMovementSpeed(450.0f);
	SetCrouchIronsightMovementSpeed(150.0f);
	SetJumpHeight(200.0f);
}

void ULD48AttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);
	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;
	if (ModifiedAttribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
	}
}

bool ULD48AttributeSet::PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data)
{
	if (!Super::PreGameplayEffectExecute(Data))
	{
		return false;
	}

	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;
	if (ModifiedAttribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
	}
	return true;
}
