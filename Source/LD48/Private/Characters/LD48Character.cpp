// Copyright Epic Games, Inc. All Rights Reserved.


#include "Characters/LD48Character.h"

#include "GameplayTagsManager.h"
#include "HeadMountedDisplayFunctionLibrary.h"


#include "ActorComponents/PMTFWAmmoBagComponent.h"
#include "ActorComponents/PMTFWFiregunEquipComponent.h"
#include "ActorComponents/PMTFWFiregunTrackerComp.h"
#include "ActorComponents/PMTGUAbilityRestorerComp.h"
#include "ActorComponents/PMTGUChainedAbilityRemovalComp.h"

#include "Camera/CameraComponent.h"

#include "LD48PlayerController.h"

#include "Characters/LD48AttributeSet.h"
#include "Characters/Movement/LD48BaseCharacterMovementComp.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"

#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregun.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "LD48/LD48GameMode.h"


#include "Utilities/LD48Utilities.h"

//////////////////////////////////////////////////////////////////////////
// ALD48Character

ALD48Character::ALD48Character(const FObjectInitializer& InObjectInitializer) :
	Super(InObjectInitializer.SetDefaultSubobjectClass<ULD48BaseCharacterMovementComp>(
		ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character)
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)


	FiregunTrackerComponent = CreateDefaultSubobject<UPMTFWFiregunTrackerComp>(TEXT("FiregunTrackerComponent"));
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	FiregunEquipComponent = CreateDefaultSubobject<UPMTFWFiregunEquipComponent>(TEXT("FiregunEquipComponent"));
	AmmoBagComponent = CreateDefaultSubobject<UPMTFWAmmoBagComponent>(TEXT("AmmoBagComponent"));
	ChainedAbilityRemovalComponent = CreateDefaultSubobject<UPMTGUChainedAbilityRemovalComp>(TEXT("ChainedAbilityRemovalComponent"));
	AbilityRestorerComponent = CreateDefaultSubobject<UPMTGUAbilityRestorerComp>(TEXT("AbilityRestorerComponent"));
	AttributeSet = CreateDefaultSubobject<ULD48AttributeSet>(TEXT("LD48AttrbuteSet"));

	HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(this, &ALD48Character::HealthChanged);

	WeaponSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponSkeletalMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, WeaponBoneAttach);
}

void ALD48Character::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AbilitySystemComponent->InitAbilityActorInfo(this, this);
	IronsightDelegateHandle = AbilitySystemComponent->RegisterGameplayTagEvent(
		FGameplayTag::RequestGameplayTag(FName("State.Ironsight")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &ALD48Character::OnIronsightTagChanged);
	CrouchingDelegateHandle = AbilitySystemComponent->RegisterGameplayTagEvent(
		FGameplayTag::RequestGameplayTag(FName("State.Movement.Crouching")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &ALD48Character::OnCrouchTagChanged);

	if (IsPlayerControlled())
	{
		ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->SetPlayer(this);
		}
	}
}

void ALD48Character::UnPossessed()
{
	AbilitySystemComponent->RegisterGameplayTagEvent(
		FGameplayTag::RequestGameplayTag(FName("State.Ironsight")), EGameplayTagEventType::NewOrRemoved).Remove(IronsightDelegateHandle);
	AbilitySystemComponent->RegisterGameplayTagEvent(
		FGameplayTag::RequestGameplayTag(FName("State.Movement.Crouching")), EGameplayTagEventType::NewOrRemoved).Remove(CrouchingDelegateHandle);
	Super::UnPossessed();
}

UAbilitySystemComponent* ALD48Character::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ALD48Character::NotifyBulletCountChanged(const FPMTFWFiregunInstanceDataItem& ItemThatChanged)
{
	OnBulletCountChanged.Broadcast();
}

bool ALD48Character::HasAnyFiregunEquipped() const
{
	return FiregunEquipComponent->GetCurrentFiregunHandle().IsValid();
}

FPMTFWFiregunInstanceDataItem& ALD48Character::GetMutableCurrentEquippedFiregun()
{
	return FiregunTrackerComponent->GetMutableInstanceDataItem(FiregunEquipComponent->GetCurrentFiregunHandle());
}

const FPMTFWFiregunInstanceDataItem& ALD48Character::GetCurrentEquippedFiregun() const
{
	return FiregunTrackerComponent->GetInstanceDataItem(FiregunEquipComponent->GetCurrentFiregunHandle());
}

APMTFWFiregun* ALD48Character::GetMutableCurrentPhysicalFiregun() const
{
	return EquippedFiregun;
}

APMTFWActiveFiregunsProxy* ALD48Character::GetMutableCurrentFiregunProxy() const
{
	if (!FiregunEquipComponent->HasAnyFiregunEquipped())
	{
		return nullptr;
	}
	FPMTFWFiregunInstanceHandle Handle = FiregunEquipComponent->GetCurrentFiregunHandle();
	AController* OwningController = GetController();
	for (AActor* Child : OwningController->Children)
	{
		APMTFWActiveFiregunsProxy* Proxy = Cast<APMTFWActiveFiregunsProxy>(Child);
		if (Proxy && Proxy->DoesProxyContainFiregunInstanceHandle(Handle))
		{
			return Proxy;
		}
	}
	return nullptr;
}

void ALD48Character::ModifyBulletCount(FGameplayTag AmmoTag, uint16 Bullets)
{
	FPMTFWFiregunInstanceDataItem& MutableDataItem = GetMutableCurrentEquippedFiregun();
	MutableDataItem.ModifyBulletCount(AmmoTag, Bullets);
	// TODO(Brian): Display bullets in UI.
}

void ALD48Character::HandleAmmoPickup(FGameplayTag AmmoTag, float InValue)
{
	AmmoBagComponent->IncrementBulletCount(AmmoTag, static_cast<int32>(InValue));
}

float ALD48Character::GetWalkMaxSpeed() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetMovementSpeed();
	}
	return 0.0f;
}

float ALD48Character::GetCrouchMaxSpeed() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetCrouchMovementSpeed();
	}
	return 0.0f;
}

float ALD48Character::GetIronsightMaxSpeed() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetIronsightMovementSpeed();
	}
	return 0.0f;
}

float ALD48Character::GetCrouchIronsightMaxSpeed() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetCrouchIronsightMovementSpeed();
	}
	return 0.0f;
}

bool ALD48Character::CanMoveAtAll() const
{
	return
		!AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.InputDisabled")))) &&
		!AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.Dead")))) &&
		!AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.Stunned"))));
}

bool ALD48Character::IsCrouched() const
{
	// These tags were defined in project settings.
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.Movement.Crouching"))));
}

bool ALD48Character::HasIronsights() const
{
	// These tags were defined in project settings.
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.Ironsight"))));
}

bool ALD48Character::IsInJumpingState() const
{
	// These tags were defined in project settings.
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName(TEXT("State.Movement.Jumping"))));
}

FLD48ActivePowerup ALD48Character::RemoveRandomPowerup(bool& bRemoved)
{
	if (ActivePowerups.Num() == 0)
	{
		bRemoved = false;
		return FLD48ActivePowerup();
	}
	ALD48PlayerController* OurController = Cast<ALD48PlayerController>(GetController());
	bRemoved = true;
	const int32 RandomPowerupIndex = FMath::RandRange(0, ActivePowerups.Num() - 1);
	FLD48ActivePowerup PowerupToRemove = ActivePowerups[RandomPowerupIndex];
	if (PowerupToRemove.PowerupType == EPowerUpType::Character)
	{
		AbilitySystemComponent->RemoveActiveGameplayEffect(PowerupToRemove.SpecHandle, 1);
	}
	else
	{
		TArray<FPMTFWFiregunInstanceHandle> Handles = FiregunTrackerComponent->GetAllFiregunHandles();
		for (FPMTFWFiregunInstanceHandle SingleHandle : Handles)
		{
			FPMTFWFiregunInstanceDataItem& DataItem = FiregunTrackerComponent->GetMutableInstanceDataItem(SingleHandle);
			const FGameplayTag WeaponTypeTag = DataItem.FiregunDataClass.GetDefaultObject()->GetFiregunWeaponType();
			if (WeaponTypeTag == FGameplayTag::RequestGameplayTag(TEXT("Firegun.Shotgun")) && PowerupToRemove.PowerupType == EPowerUpType::Shotgun)
			{
				APMTFWActiveFiregunsProxy* Proxy = OurController->GetProxyByHandle(SingleHandle);

				Proxy->RemoveGameplayEffect(PowerupToRemove.SpecHandle, SingleHandle);
				break;
			}
			else if (WeaponTypeTag == FGameplayTag::RequestGameplayTag(TEXT("Firegun.AssaultRifle")) && PowerupToRemove.PowerupType == EPowerUpType::Rifle)
			{
				APMTFWActiveFiregunsProxy* Proxy = OurController->GetProxyByHandle(SingleHandle);
				Proxy->RemoveGameplayEffect(PowerupToRemove.SpecHandle, SingleHandle);
				break;
			}
		}
	}
	ActivePowerups.RemoveAt(RandomPowerupIndex);
	return PowerupToRemove;
}

void ALD48Character::BeginPlay()
{
	Super::BeginPlay();

	StartingLocation = GetActorLocation();

	if (AmmoBagComponent)
	{
		AmmoBagComponent->AddAmmoAttributes(AmmoBullets);
	}

	if (ActiveFiregunClass)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		EquippedFiregun = GetWorld()->SpawnActor<APMTFWFiregun>(ActiveFiregunClass, FTransform());
		EquippedFiregun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName(TEXT("firegunSocket")));
	}

	if (WeaponSkeletalMesh)
	{
		WeaponSkeletalMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, WeaponBoneAttach);
	}

	AfterBeginPlay();
}


//////////////////////////////////////////////////////////////////////////
// Input

void ALD48Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ALD48Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ALD48Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ALD48Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ALD48Character::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ALD48Character::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ALD48Character::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ALD48Character::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ALD48Character::OnResetVR);

	AbilitySystemComponent->BindAbilityActivationToInputComponent(PlayerInputComponent,
		FGameplayAbilityInputBinds(FString(TEXT("")), FString(TEXT("")), FString(TEXT("ELD48InputId")), INDEX_NONE, INDEX_NONE));

	PlayerInputComponent->BindAction("Reset", IE_Pressed, this, &ALD48Character::KillSelf);

}

void ALD48Character::ApplyPowerup(FLD48PowerupDescriptor InPowerupDescriptor, EPowerUpType PowerupType)
{
	ALD48PlayerController* OurController = Cast<ALD48PlayerController>(GetController());
	FActiveGameplayEffectHandle AppliedEffect;
	UGameplayEffect* DefaultGameplayEffect = InPowerupDescriptor.GameplayEffectClass.GetDefaultObject();
	if (PowerupType == EPowerUpType::Character)
	{
		AppliedEffect = AbilitySystemComponent->ApplyGameplayEffectToSelf(
			InPowerupDescriptor.GameplayEffectClass.GetDefaultObject(), InPowerupDescriptor.Level, AbilitySystemComponent->MakeEffectContext());
	}
	else
	{
		TArray<FPMTFWFiregunInstanceHandle> Handles = FiregunTrackerComponent->GetAllFiregunHandles();
		for (FPMTFWFiregunInstanceHandle SingleHandle : Handles)
		{
			FPMTFWFiregunInstanceDataItem& DataItem = FiregunTrackerComponent->GetMutableInstanceDataItem(SingleHandle);
			const FGameplayTag WeaponTypeTag = DataItem.FiregunDataClass.GetDefaultObject()->GetFiregunWeaponType();
			if (WeaponTypeTag == FGameplayTag::RequestGameplayTag(TEXT("Firegun.Shotgun")) && PowerupType == EPowerUpType::Shotgun)
			{
				APMTFWActiveFiregunsProxy* Proxy = OurController->GetProxyByHandle(SingleHandle);
				AppliedEffect = Proxy->ApplyGameplayEffect(FGameplayEffectSpec(DefaultGameplayEffect, FGameplayEffectContextHandle(), InPowerupDescriptor.Level), SingleHandle);
				break;
			}
			else if (WeaponTypeTag == FGameplayTag::RequestGameplayTag(TEXT("Firegun.AssaultRifle")) && PowerupType == EPowerUpType::Rifle)
			{
				APMTFWActiveFiregunsProxy* Proxy = OurController->GetProxyByHandle(SingleHandle);
				AppliedEffect = Proxy->ApplyGameplayEffect(FGameplayEffectSpec(DefaultGameplayEffect, FGameplayEffectContextHandle(), InPowerupDescriptor.Level), SingleHandle);
				break;
			}
		}
	}
	FLD48ActivePowerup ActivePowerup;
	ActivePowerup.Descriptor = InPowerupDescriptor;
	ActivePowerup.SpecHandle = AppliedEffect;
	ActivePowerup.PowerupType = PowerupType;

	ActivePowerups.Add(ActivePowerup);
}

void ALD48Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ALD48Character::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ALD48Character::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ALD48Character::OnIronsightTagChanged(const FGameplayTag TagChanged, int32 NewCount)
{

}

void ALD48Character::OnCrouchTagChanged(const FGameplayTag TagChanged, int32 NewCount)
{

}

void ALD48Character::QueryCurrentFiregunAttribute(FGameplayTag& WeaponTag, int32& CurrentBulletsInMagazine, int32& BulletsInBag)
{
	const FPMTFWFiregunInstanceDataItem& CurrentItem = GetCurrentEquippedFiregun();
	if (!ensure(CurrentItem.CurrentBulletsInMagazine.Num() > 0))
	{
		return;
	}
	FGameplayTag CurrentBulletType = CurrentItem.CurrentBulletsInMagazine[0].AmmoType;
	CurrentBulletsInMagazine = CurrentItem.CurrentBulletsInMagazine[0].BulletsInMagazine;
	BulletsInBag = static_cast<int32>(AmmoBagComponent->GetCountForBulletType(CurrentBulletType));
	WeaponTag = CurrentItem.FiregunDataClass.GetDefaultObject()->GetFiregunWeaponType();
}

void ALD48Character::QueryHealth(int32& Health, int32& MaxHealth)
{
	Health = AttributeSet ? static_cast<int32>(AttributeSet->GetHealth()) : 0;
	MaxHealth = AttributeSet ? static_cast<int32>(AttributeSet->GetMaxHealth()) : 0;
}

void ALD48Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ALD48Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ALD48Character::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ALD48Character::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ALD48Character::Reset()
{
	if (USkeletalMeshComponent* SkeletalMeshComponent = GetMesh())
	{
		SkeletalMeshComponent->SetSimulatePhysics(false);
		SkeletalMeshComponent->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepWorldTransform);
		SkeletalMeshComponent->SetRelativeLocation(FVector(0, 0, -86));
		SkeletalMeshComponent->SetRelativeRotation(FRotator(0, -90, 0));
	}
	if (UCapsuleComponent* Capsule = GetCapsuleComponent())
	{
		Capsule->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	EnableInput(Cast<APlayerController>(GetController()));
	bDie = false;
	TeleportTo(StartingLocation, GetActorRotation(), false, true);
}

void ALD48Character::HealthChanged(const FOnAttributeChangeData& Data)
{
	UE_LOG(LogTemp, Warning, TEXT("%s health changed to: %s"), *GetName(), *FString::SanitizeFloat(Data.NewValue));

	const float MaxHealth = AttributeSet ? AttributeSet->GetMaxHealth() : 0.0f;
	OnHealthChanged.Broadcast(Data.NewValue, static_cast<int32>(MaxHealth));
	if (!bDie && Data.NewValue <= 0.0f)
	{
		if (USkeletalMeshComponent* SkeletalMeshComponent = GetMesh())
		{
			SkeletalMeshComponent->SetSimulatePhysics(true);
		}
		if (UCapsuleComponent* Capsule = GetCapsuleComponent())
		{
			Capsule->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		DisableInput(Cast<APlayerController>(GetController()));
		OnCharacterDie.Broadcast(this);
		OnDie();
		bDie = true;
	}
}

void ALD48Character::KillSelf()
{
	if (AbilitySystemComponent)
	{
		const FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		AbilitySystemComponent->ApplyGameplayEffectToSelf(ResetGameplayEffect.GetDefaultObject(), 1, EffectContext);
	}
}
