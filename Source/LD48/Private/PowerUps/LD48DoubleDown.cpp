﻿

#include "PowerUps/LD48DoubleDown.h"

#include "Characters/LD48Character.h"


// Sets default values
ALD48DoubleDown::ALD48DoubleDown()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ALD48DoubleDown::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ALD48Character* Character = Cast<ALD48Character>(OtherActor);
	if (Character && Character->IsPlayerControlled())
	{
		OnPowerupOverlap.Broadcast(this, Character);
		OnPowerupOverlap.Clear();
		Destroy();
	}
}

