// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUps/LD48PowerUp.h"

#include "Characters/LD48Character.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

ALD48PowerUp::ALD48PowerUp()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));

	RootComponent = BoxComponent;
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetGenerateOverlapEvents(true);
	StaticMeshComponent->SetupAttachment(BoxComponent);
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ALD48PowerUp::BeginPlay()
{
	Super::BeginPlay();
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ALD48PowerUp::OnComponentBeginOverlap);
}

void ALD48PowerUp::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult)
{
	ALD48Character* Character = Cast<ALD48Character>(OtherActor);
	if (Character && Character->IsPlayerControlled())
	{
		if (UAbilitySystemComponent* ASC = Character->GetAbilitySystemComponent())
		{
			FGameplayCueParameters Parameters;
			Parameters.Location = Character->GetActorLocation();
			ASC->ExecuteGameplayCue(Cue, Parameters);
		}
		HandleOverlap(Character);
	}
}

const FLD48PowerupDescriptor& ALD48PowerUp::GetPowerupDescriptor() const
{
	return PowerupDescriptor;
}

void ALD48PowerUp::SetPowerUpLevel(int32 Level)
{
	PowerupDescriptor.Level = Level;
}

void ALD48PowerUp::HandleOverlap(ALD48Character* InCharacter)
{
	InCharacter->ApplyPowerup(PowerupDescriptor, EPowerUpType::Character);
	OnPowerupOverlap.Broadcast(this, InCharacter);
	OnPowerupOverlap.Clear();
	Destroy();
}
