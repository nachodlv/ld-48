﻿#include "PowerUps/PowerUpSpawner.h"

#include "EngineUtils.h"

#include "Characters/LD48Character.h"



#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"

#include "LD48/LD48GameMode.h"

#include "PowerUps/LD48DoubleDown.h"

APowerUpSpawner::APowerUpSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = false;
}

void APowerUpSpawner::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ATargetPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ATargetPoint* TargetPoint = *ActorItr;
		if (TargetPoint)
		{
			if (TargetPoint->ActorHasTag(TEXT("PowerupSpawn")))
			{
				TargetPoints.Add(TargetPoint);
			} else if (TargetPoint->ActorHasTag(TEXT("DoubleDownSpawn")))
			{
				DoubleDownSpawn = TargetPoint;
			}
		}
	}

	ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
	GameMode->OnRoundEnd.AddDynamic(this, &APowerUpSpawner::SpawnPowerUps);
}

void APowerUpSpawner::SpawnPowerUps()
{
	if (!bPlayerBinded)
	{
		ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
		if (ALD48Character* Character = GameMode->GetPlayer())
		{
			Character->OnCharacterDie.AddDynamic(this, &APowerUpSpawner::PlayerDied);
			bPlayerBinded = true;
		}
	}

	UWorld* World = GetWorld();

	// Spawn powerups
	TArray<TSubclassOf<ALD48PowerUp>> PowerUpsAvailable = PowerUps;
	for (ATargetPoint* TargetPoint : TargetPoints)
	{
		if (PowerUpsAvailable.Num() == 0)
		{
			break;
		}

		const int32 RandomIndex = FMath::RandRange(0, PowerUpsAvailable.Num() - 1);
		const TSubclassOf<ALD48PowerUp>& PowerUp = PowerUpsAvailable[RandomIndex];
		ALD48PowerUp* SpawnedPowerUp = World->SpawnActorDeferred<ALD48PowerUp>(PowerUp.Get(), TargetPoint->GetTransform(), nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		SpawnedPowerUp->SetPowerUpLevel(CurrentLevel);
		UGameplayStatics::FinishSpawningActor(SpawnedPowerUp, TargetPoint->GetTransform());
		SpawnedPowerUp->OnPowerupOverlap.AddDynamic(this, &APowerUpSpawner::PowerupGrabbed);
		SpawnedPowerUps.Add(SpawnedPowerUp);
		PowerUpsAvailable.RemoveAt(RandomIndex);
	}

	// Spawn double down
	FActorSpawnParameters Parameters;
	Parameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	DoubleDownSpawned = World->SpawnActor<ALD48DoubleDown>(DoubleDown.Get(), DoubleDownSpawn->GetTransform(), Parameters);
	DoubleDownSpawned->OnPowerupOverlap.AddDynamic(this, &APowerUpSpawner::DoubleDownGrabbed);
}

void APowerUpSpawner::PlayerDied(ALD48Character* Character)
{
	CurrentLevel = 1;
}

void APowerUpSpawner::PowerupGrabbed(ALD48PowerUp* PowerUp, ALD48Character* CharacterThatOverlapped)
{
	CurrentLevel = 1;
	StartNextRound();
}

void APowerUpSpawner::StartNextRound()
{
	if (DoubleDownSpawned)
	{
		DoubleDownSpawned->Destroy();
	}

	for (ALD48PowerUp* SpawnedPowerUp : SpawnedPowerUps)
	{
		if (SpawnedPowerUp)
		{
			SpawnedPowerUp->Destroy();
		}
	}


	if (ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode()))
	{
		GameMode->RoundStart();
	}
}

void APowerUpSpawner::DoubleDownGrabbed(ALD48PowerUp* PowerUp, ALD48Character* CharacterThatOverlapped)
{
	CurrentLevel++;
	StartNextRound();
}

