// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUps/LD48FiregunPowerup.h"

#include "Characters/LD48Character.h"

// UE Includes
#include "AbilitySystemComponent.h"

void ALD48FiregunPowerup::HandleOverlap(ALD48Character* InCharacter)
{
	InCharacter->ApplyPowerup(PowerupDescriptor, WeaponToAffect);
	OnPowerupOverlap.Broadcast(this, InCharacter);
	Destroy();
}
