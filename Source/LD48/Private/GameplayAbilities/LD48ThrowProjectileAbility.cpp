﻿#include "GameplayAbilities/LD48ThrowProjectileAbility.h"

#include "AIController.h"

#include "BehaviorTree/BlackboardComponent.h"



#include "GameFramework/ProjectileMovementComponent.h"


#include "GameplayAbilities/AbilityTasks/LD48_PlayMontageAndWaitForEvent.h"
#include "GameplayAbilities/Interfaces/Projectile.h"
#include "GameplayAbilities/Interfaces/RangeMeshProvider.h"

#include "Kismet/GameplayStatics.h"

void ULD48ThrowProjectileAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                                  const FGameplayAbilityActorInfo* OwnerInfo,
                                                  const FGameplayAbilityActivationInfo ActivationInfo,
                                                  const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(OwnerInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, OwnerInfo, ActivationInfo))
		{
			return;
		}
		ULD48_PlayMontageAndWaitForEvent* Task = ULD48_PlayMontageAndWaitForEvent::PlayMontageAndWaitForEvent(
			this, NAME_None, AnimMontage, FGameplayTagContainer(), 1.0f, NAME_None, true, 1.0f);
		Task->EventReceived.AddDynamic(this, &ULD48ThrowProjectileAbility::EventReceived);
		Task->OnCompleted.AddDynamic(this, &ULD48ThrowProjectileAbility::OnMontageEnded);
		Task->OnBlendOut.AddDynamic(this, &ULD48ThrowProjectileAbility::OnMontageEnded);
		Task->OnCancelled.AddDynamic(this, &ULD48ThrowProjectileAbility::OnMontageEnded);
		Task->OnInterrupted.AddDynamic(this, &ULD48ThrowProjectileAbility::OnMontageEnded);
		Task->ReadyForActivation();
	}
}

void ULD48ThrowProjectileAbility::EventReceived(FGameplayTag EventTag, FGameplayEventData EventData)
{
	AActor* Actor = CurrentActorInfo->AvatarActor.Get();
	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (Actor && Actor->GetClass()->ImplementsInterface(URangeMeshProvider::StaticClass()))
	{
		USkeletalMeshComponent* Mesh = IRangeMeshProvider::Execute_GetRangeMesh(Actor);
		if (Mesh)
		{
			const FVector& SocketLocation = Mesh->GetSocketLocation(SpawnSocket);
			AActor* SpawnedActor = GetWorld()->SpawnActor(Projectile.Get(), &SocketLocation);
			if (SpawnedActor->GetClass()->ImplementsInterface(UProjectile::StaticClass()))
			{
				IProjectile::Execute_SetSourceAbilitySystemComponent(SpawnedActor, ASC);
			}
			UProjectileMovementComponent* MovementComponent = Cast<UProjectileMovementComponent>( SpawnedActor->GetComponentByClass(UProjectileMovementComponent::StaticClass()));
			const FVector& TargetLocation = GetTargetLocation();
			FVector Speed = FVector::ZeroVector;
			UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, Speed, SocketLocation, TargetLocation, 0, Arc);
			MovementComponent->SetVelocityInLocalSpace(Speed);
			MovementComponent->Activate();
			EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		}
	}
}

void ULD48ThrowProjectileAbility::OnMontageEnded(FGameplayTag EventTag, FGameplayEventData EventData)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

FVector ULD48ThrowProjectileAbility::GetTargetLocation() const
{
	if (APawn* Actor = Cast<APawn>(CurrentActorInfo->AvatarActor.Get()))
	{
		if (AAIController* AIController = Cast<AAIController>(Actor->GetController()))
		{
			if (UBlackboardComponent* BB = AIController->GetBlackboardComponent())
			{
				if (AActor* Target = Cast<AActor>(BB->GetValueAsObject(TEXT("Target"))))
				{
					return Target->GetActorLocation();
				}
			}
		}
	}

	return FVector();
}
