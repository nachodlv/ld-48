﻿#include "GameplayAbilities/LD48MeleeAttackAbility.h"

#include "AbilitySystemComponent.h"


#include "Characters/LD48Character.h"


#include "GameplayAbilities/AbilityTasks/LD48_PlayMontageAndWaitForEvent.h"
#include "GameplayAbilities/Interfaces/MeleeMeshProvider.h"
#include "Components/SkeletalMeshComponent.h"

#include "Kismet/KismetSystemLibrary.h"

void ULD48MeleeAttackAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                              const FGameplayAbilityActorInfo* ActorInfo,
                                              const FGameplayAbilityActivationInfo ActivationInfo,
                                              const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		ULD48_PlayMontageAndWaitForEvent* Task = ULD48_PlayMontageAndWaitForEvent::PlayMontageAndWaitForEvent(
			this, NAME_None, AnimMontage, FGameplayTagContainer(), 1.0f, NAME_None, true, 1.0f);
		Task->EventReceived.AddDynamic(this, &ULD48MeleeAttackAbility::EventReceived);
		Task->OnCompleted.AddDynamic(this, &ULD48MeleeAttackAbility::OnMontageEnded);
		Task->OnBlendOut.AddDynamic(this, &ULD48MeleeAttackAbility::OnMontageEnded);
		Task->OnCancelled.AddDynamic(this, &ULD48MeleeAttackAbility::OnMontageEnded);
		Task->OnInterrupted.AddDynamic(this, &ULD48MeleeAttackAbility::OnMontageEnded);
		Task->ReadyForActivation();

		UAbilitySystemComponent* ASC = ActorInfo->AbilitySystemComponent.Get();
		if (ASC && ActorInfo->AvatarActor.Get())
		{
			FGameplayCueParameters Parameters;
			Parameters.Location = ActorInfo->AvatarActor->GetActorLocation();
			ASC->ExecuteGameplayCue(StartAttackingCue, Parameters);
		}
	}
}

void ULD48MeleeAttackAbility::EventReceived(FGameplayTag EventTag, FGameplayEventData EventData)
{
	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (!CurrentActorInfo->AvatarActor.IsValid() || !ASC)
	{
		return;
	}
	IMeleeMeshProvider* MeshProvider = Cast<IMeleeMeshProvider>(CurrentActorInfo->AvatarActor.Get());
	USkeletalMeshComponent* MeleeWeapon = MeshProvider ? MeshProvider->GetMeleeMesh() : nullptr;
	if (!MeleeWeapon)
	{
		return;
	}

	const FVector& From = MeleeWeapon->GetSocketLocation(FromTraceSocket);
	const FVector& To = MeleeWeapon->GetSocketLocation(ToTraceSocket);
	TArray<FHitResult> Hits;
	UKismetSystemLibrary::SphereTraceMultiByProfile(CurrentActorInfo->AvatarActor.Get(),
	                                                From, To, 10,
	                                                HitProfile, false, {},
	                                                EDrawDebugTrace::ForDuration, Hits, true, FLinearColor::Green,
	                                                FLinearColor::Red, 5.0f);

	for (FHitResult& Hit : Hits)
	{
		ALD48Character* Character = Hit.Actor.IsValid() ? Cast<ALD48Character>(Hit.Actor) : nullptr;
		if (Character && Character->IsPlayerControlled())
		{
			UAbilitySystemComponent* TargetASC = Character->GetAbilitySystemComponent();
			const FGameplayEffectContextHandle Handle = ASC->MakeEffectContext();
			ASC->ApplyGameplayEffectToTarget(GameplayEffect.GetDefaultObject(), TargetASC, 1, Handle);
			FGameplayCueParameters Parameters;
			Parameters.Location = Hit.Location;
			ASC->ExecuteGameplayCue(HitCue, Parameters);
			EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		}
	}
}

void ULD48MeleeAttackAbility::OnMontageEnded(FGameplayTag EventTag, FGameplayEventData EventData)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
