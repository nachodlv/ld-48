// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilities/LD48SwitchWeaponAbility.h"

#include "ActorComponents/PMTFWFiregunTrackerComp.h"
#include "Characters/LD48Character.h"
#include "LD48PlayerController.h"

ULD48SwitchWeaponAbility::ULD48SwitchWeaponAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void ULD48SwitchWeaponAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		ALD48Character* OurCharacter = Cast<ALD48Character>(GetAvatarActorFromActorInfo());
		if (OurCharacter)
		{
			UPMTFWFiregunTrackerComp* TrackerComp = OurCharacter->GetFiregunTrackerComponent();
			TArray<FPMTFWFiregunInstanceHandle> AllHandles = TrackerComp->GetAllFiregunHandles();
			FPMTFWFiregunInstanceHandle AvailableHandle;
			for (FPMTFWFiregunInstanceHandle SingleHandle : AllHandles)
			{
				if (OurCharacter->GetCurrentEquippedFiregun().Handle != SingleHandle)
				{
					AvailableHandle = SingleHandle;
					break;
				}
			}
			if (AvailableHandle.IsValid())
			{
				ALD48PlayerController* PlayerController = Cast<ALD48PlayerController>(OurCharacter->GetController());
				if (PlayerController)
				{
					PlayerController->EquipFiregunInstance(AvailableHandle);
					OurCharacter->OnWeaponChanged.Broadcast();
				}
			}
		}
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
	}
}
