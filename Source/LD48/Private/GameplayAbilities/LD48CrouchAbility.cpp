// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilities/LD48CrouchAbility.h"

// UE Includes
#include "Abilities/Tasks/AbilityTask_WaitInputPress.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"


ULD48CrouchAbility::ULD48CrouchAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

bool ULD48CrouchAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	const ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
	const UCharacterMovementComponent* CharacterMovementComponent = OurCharacter ? OurCharacter->GetCharacterMovement() : nullptr;
	return Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags) && OurCharacter && OurCharacter->CanCrouch();
}

void ULD48CrouchAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
		if (bEndCrouchWhenReleased)
		{
			WaitInputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this, true);
			WaitInputReleaseTask->OnRelease.AddDynamic(this, &ULD48CrouchAbility::OnInputRelease);
			WaitInputReleaseTask->ReadyForActivation();
		}
		else
		{
			WaitInputPressTask = UAbilityTask_WaitInputPress::WaitInputPress(this, false);
			WaitInputPressTask->OnPress.AddDynamic(this, &ULD48CrouchAbility::OnInputPressed);
			WaitInputPressTask->ReadyForActivation();
		}
	}
}

void ULD48CrouchAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	if (WaitInputPressTask)
	{
		WaitInputPressTask->OnPress.RemoveDynamic(this, &ULD48CrouchAbility::OnInputPressed);
		WaitInputPressTask->EndTask();
		WaitInputPressTask = nullptr;
	}
	else if (WaitInputReleaseTask)
	{
		WaitInputReleaseTask->OnRelease.RemoveDynamic(this, &ULD48CrouchAbility::OnInputRelease);
		WaitInputReleaseTask->EndTask();
		WaitInputReleaseTask = nullptr;
	}
	ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
	if (OurCharacter)
	{
		OurCharacter->UnCrouch(true);
	}
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void ULD48CrouchAbility::OnInputRelease(float ElapsedTime)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void ULD48CrouchAbility::OnInputPressed(float ElapsedTime)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
