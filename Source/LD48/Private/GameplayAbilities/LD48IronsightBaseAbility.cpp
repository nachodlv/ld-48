// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilities/LD48IronsightBaseAbility.h"

#include "Characters/LD48Character.h"

// UE Includes
#include "Abilities/Tasks/AbilityTask_WaitInputPress.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"

ULD48IronsightBaseAbility::ULD48IronsightBaseAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void ULD48IronsightBaseAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		ALD48Character* OurCharacter = Cast<ALD48Character>(GetAvatarActorFromActorInfo());
		if (OurCharacter)
		{
			OurCharacter->GetCameraBoom()->TargetArmLength = OurCharacter->GetCameraBoom()->TargetArmLength + CameraSpringArmExtra;
		}
		if (bStopIronsightOnRelease)
		{
			WaitInputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this, true);
			WaitInputReleaseTask->OnRelease.AddDynamic(this, &ULD48IronsightBaseAbility::OnInputRelease);
			WaitInputReleaseTask->ReadyForActivation();
		}
		else
		{
			WaitInputPressedTask = UAbilityTask_WaitInputPress::WaitInputPress(this, false);
			WaitInputPressedTask->OnPress.AddDynamic(this, &ULD48IronsightBaseAbility::OnInputPressed);
			WaitInputPressedTask->ReadyForActivation();
		}
	}
}

void ULD48IronsightBaseAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	ALD48Character* OurCharacter = Cast<ALD48Character>(GetAvatarActorFromActorInfo());
	if (OurCharacter)
	{
		OurCharacter->GetCameraBoom()->TargetArmLength = OurCharacter->GetCameraBoom()->TargetArmLength - CameraSpringArmExtra;
	}
	if (WaitInputPressedTask)
	{
		WaitInputPressedTask->OnPress.RemoveDynamic(this, &ULD48IronsightBaseAbility::OnInputPressed);
		WaitInputPressedTask->EndTask();
		WaitInputPressedTask = nullptr;
	}
	else if (WaitInputReleaseTask)
	{
		WaitInputReleaseTask->OnRelease.RemoveDynamic(this, &ULD48IronsightBaseAbility::OnInputRelease);
		WaitInputReleaseTask->EndTask();
		WaitInputReleaseTask = nullptr;
	}
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void ULD48IronsightBaseAbility::OnInputRelease(float ElapsedTime)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void ULD48IronsightBaseAbility::OnInputPressed(float ElapsedTime)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
