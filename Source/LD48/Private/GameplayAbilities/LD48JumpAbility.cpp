// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilities/LD48JumpAbility.h"

// UE Includes
#include "GameFramework/Character.h"

ULD48JumpAbility::ULD48JumpAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

bool ULD48JumpAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	const ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
	return Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags) && OurCharacter && OurCharacter->CanJump();
}

void ULD48JumpAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
		OurCharacter->LandedDelegate.AddDynamic(this, &ULD48JumpAbility::OnLanded);
		OurCharacter->Jump();
	}
}

void ULD48JumpAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	ACharacter* OurCharacter = Cast<ACharacter>(CurrentActorInfo->AvatarActor.Get());
	OurCharacter->LandedDelegate.RemoveDynamic(this, &ULD48JumpAbility::OnLanded);
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void ULD48JumpAbility::OnLanded(const FHitResult& Hit)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
