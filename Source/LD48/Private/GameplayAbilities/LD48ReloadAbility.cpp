// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilities/LD48ReloadAbility.h"

#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "ActorComponents/PMTFWAmmoBagComponent.h"
#include "ActorComponents/PMTFWFiregunEquipComponent.h"
#include "ActorComponents/PMTFWFiregunTrackerComp.h"

#include "Characters/LD48Character.h"

#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregunData.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "LD48PlayerController.h"


ULD48ReloadAbility::ULD48ReloadAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

bool ULD48ReloadAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	ALD48Character* OurCharacter = Cast<ALD48Character>(ActorInfo->AvatarActor.Get());
	ALD48PlayerController* PlayerController = Cast<ALD48PlayerController>(OurCharacter->GetController());
	if (!OurCharacter || !PlayerController || !OurCharacter->HasAnyFiregunEquipped() || !ReloadAnimation)
	{
		return false;
	}

	UPMTFWAmmoBagComponent* AmmoBag = OurCharacter->GetAmmoBagComponent();
	FPMTFWFiregunInstanceDataItem& MutableDataItem = OurCharacter->GetMutableCurrentEquippedFiregun();
	APMTFWActiveFiregunsProxy* Proxy = PlayerController->GetProxyByHandle(MutableDataItem.Handle);

	if (!Proxy)
	{
		return false;
	}

	const FGameplayTag AmmoTag = MutableDataItem.FiregunDataClass.GetDefaultObject()->GetFiregunAmmoType();
	const int32 CurrentBulletsForType = static_cast<int32>(AmmoBag->GetCountForBulletType(AmmoTag));
	if (CurrentBulletsForType > 0 && MutableDataItem.GetCurrentBulletsForAmmoType(AmmoTag) < Proxy->GetFiregunMagazineSize())
	{
		return Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags);
	}
	return false;
}

void ULD48ReloadAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}

		ALD48Character* OurCharacter = Cast<ALD48Character>(ActorInfo->AvatarActor.Get());
		ALD48PlayerController* PlayerController = Cast<ALD48PlayerController>(OurCharacter->GetController());

		FPMTFWFiregunInstanceDataItem& MutableDataItem = OurCharacter->GetMutableCurrentEquippedFiregun();
		APMTFWActiveFiregunsProxy* Proxy = PlayerController->GetProxyByHandle(MutableDataItem.Handle);
		const float ReloadRate = Proxy->GetFiregunReloadSpeed();
		PlayMontageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, NAME_None, ReloadAnimation, ReloadRate);
		PlayMontageTask->OnBlendOut.AddDynamic(this, &ULD48ReloadAbility::OnReloadSucceeded);
		PlayMontageTask->OnCompleted.AddDynamic(this, &ULD48ReloadAbility::OnReloadSucceeded);
		PlayMontageTask->OnCancelled.AddDynamic(this, &ULD48ReloadAbility::OnReloadFailed);
		PlayMontageTask->OnInterrupted.AddDynamic(this, &ULD48ReloadAbility::OnReloadFailed);
		PlayMontageTask->ReadyForActivation();
	}
}

void ULD48ReloadAbility::OnReloadSucceeded()
{
	ALD48Character* OurCharacter = Cast<ALD48Character>(GetAvatarActorFromActorInfo());
	ALD48PlayerController* PlayerController = Cast<ALD48PlayerController>(OurCharacter->GetController());
	if (OurCharacter && PlayerController)
	{
		UPMTFWAmmoBagComponent* AmmoBag = OurCharacter->GetAmmoBagComponent();
		FPMTFWFiregunInstanceDataItem& MutableDataItem = OurCharacter->GetMutableCurrentEquippedFiregun();
		APMTFWActiveFiregunsProxy* Proxy = PlayerController->GetProxyByHandle(MutableDataItem.Handle);

		const FGameplayTag AmmoTag = MutableDataItem.FiregunDataClass.GetDefaultObject()->GetFiregunAmmoType();
		const int32 CurrentBulletsForType = static_cast<int32>(AmmoBag->GetCountForBulletType(AmmoTag));
		const int32 MagazineSize = static_cast<int32>(Proxy->GetFiregunMagazineSize());
		const int32 CurrentBulletsInWeapon = MutableDataItem.GetCurrentBulletsForAmmoType(AmmoTag);
		const int32 DifferenceWithMagazineSize = MagazineSize - CurrentBulletsInWeapon;
		if (DifferenceWithMagazineSize > 0 && CurrentBulletsForType > 0)
		{
			const int32 MaxBulletsToGive = FMath::Clamp(DifferenceWithMagazineSize, 0, CurrentBulletsForType);
			OurCharacter->ModifyBulletCount(AmmoTag, CurrentBulletsInWeapon + MaxBulletsToGive);
			AmmoBag->IncrementBulletCount(AmmoTag, -1 * MaxBulletsToGive);
		}
	}

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void ULD48ReloadAbility::OnReloadFailed()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
