// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/LD48Pickup.h"

#include "AbilitySystemComponent.h"

#include "Characters/LD48Character.h"

// UE Includes
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

ALD48Pickup::ALD48Pickup()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));

	RootComponent = BoxComponent;
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetGenerateOverlapEvents(true);
	StaticMeshComponent->SetupAttachment(BoxComponent);
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ALD48Pickup::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ALD48Pickup::OnComponentBeginOverlap);
}

void ALD48Pickup::HandleOverlap(ALD48Character* InCharacter)
{
	UAbilitySystemComponent* ASC = InCharacter->GetAbilitySystemComponent();
	for (TSubclassOf<UGameplayEffect> SingleEffect : GameplayEffectsToApply)
	{
		ASC->ApplyGameplayEffectToSelf(SingleEffect.GetDefaultObject(), EffectLevel, ASC->MakeEffectContext());
	}
}

void ALD48Pickup::ExecuteCue(UAbilitySystemComponent* ASC)
{
	FGameplayCueParameters Parameters;
	Parameters.Location = GetActorLocation();
	ASC->ExecuteGameplayCue(PickupCue, Parameters);
}

void ALD48Pickup::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                          UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                          const FHitResult& SweepResult)
{
	ALD48Character* Character = Cast<ALD48Character>(OtherActor);
	if (Character && Character->IsPlayerControlled())
	{
		HandleOverlap(Character);
		ExecuteCue(Character->GetAbilitySystemComponent());
		Destroy();
	}
}
