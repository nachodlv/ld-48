// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/LD48AmmoPickup.h"

#include "ActorComponents/PMTFWAmmoBagComponent.h"
#include "Characters/LD48Character.h"


void ALD48AmmoPickup::HandleOverlap(ALD48Character* InCharacter)
{
	for (const FLD48AmmoRecover& SingleAmmo : BulletsToGive)
	{
		InCharacter->HandleAmmoPickup(SingleAmmo.AmmoTag, SingleAmmo.ValueAtLevel.GetValueAtLevel(EffectLevel));
	}
}
