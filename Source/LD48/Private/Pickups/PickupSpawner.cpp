﻿#include "Pickups/PickupSpawner.h"

#include "EngineUtils.h"


#include "Engine/TargetPoint.h"

#include "LD48/LD48GameMode.h"

#include "Pickups/LD48Pickup.h"


APickupSpawner::APickupSpawner()
{
	bReplicates = false;
}

// Called when the game starts or when spawned
void APickupSpawner::BeginPlay()
{
	Super::BeginPlay();
	check(PickupClasses.Num() > 0);
	ALD48GameMode* GameMode = Cast<ALD48GameMode>(GetWorld()->GetAuthGameMode());
	for (TActorIterator<ATargetPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ATargetPoint* TargetPoint = *ActorItr;
		if (TargetPoint && TargetPoint->ActorHasTag(TEXT("Pickup")))
		{
			TargetPoints.Add(TargetPoint);
		}
	}

	if (GameMode)
	{
		GameMode->OnRoundStart.AddDynamic(this, &APickupSpawner::SpawnPickUps);
		GameMode->OnRoundEnd.AddDynamic(this, &APickupSpawner::DestroyPickUps);
	}
}

void APickupSpawner::DestroyPickUps()
{
	for (ALD48Pickup* Pickup : PickupsSpawned)
	{
		if (Pickup)
		{
			Pickup->Destroy();
		}
	}
}

void APickupSpawner::SpawnPickUps(int32 Round)
{
	UWorld* World = GetWorld();
	TArray<ATargetPoint*> AvailableTargetPoints = TargetPoints;
	for(int32 i = 0; i < QuantityOfSpawnsPerRound; ++i)
	{
		if (AvailableTargetPoints.Num() == 0)
		{
			break;
		}

		TSubclassOf<ALD48Pickup> Pickup = PickupClasses[FMath::RandRange(0, PickupClasses.Num() - 1)];
		const int32 IndexTargetPoint = FMath::RandRange(0,AvailableTargetPoints.Num() - 1);
		ATargetPoint* TargetPoint = AvailableTargetPoints[IndexTargetPoint];
		const FTransform Transform = TargetPoint->GetTransform();
		PickupsSpawned.Add(World->SpawnActor<ALD48Pickup>(Pickup, Transform));
		AvailableTargetPoints.RemoveAt(IndexTargetPoint);
	}
}


