// Fill out your copyright notice in the Description page of Project Settings.


#include "Firegun/LD48BaseFiregun.h"

ALD48BaseFiregun::ALD48BaseFiregun(const FObjectInitializer& InObjectInitializer) :
	Super(
		InObjectInitializer.DoNotCreateDefaultSubobject(GetCapsuleCollisionComponentName()).
		DoNotCreateDefaultSubobject(GetStaticMeshComponentName()))
{

}
