﻿#include "FunctionLibraries/PMTAIFunctionLibrary.h"

#include "AbilitySystemInterface.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

UAbilitySystemComponent* UPMTAIFunctionLibrary::GetAbilitySystemComponent(UBehaviorTreeComponent* OwnerComp)
{
	AAIController* AIController = OwnerComp ? OwnerComp->GetAIOwner() : nullptr;
	IAbilitySystemInterface* AbilitySystemInterface =
		Cast<IAbilitySystemInterface>(AIController ? AIController->GetPawn() : nullptr);
	return AbilitySystemInterface ? AbilitySystemInterface->GetAbilitySystemComponent() : nullptr;
}
