﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMTAI.h"

#define LOCTEXT_NAMESPACE "FPMTAIModule"

class FPMTAIModule : public IPMTAIModule
{
public:

	/** IModuleInterface implementation */
	void StartupModule() override
	{
		// This is loaded upon first request
	}

	void ShutdownModule() override
	{
	}
};

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FPMTAIModule, PMTAI)
