﻿#include "Tasks/GameplayAbility/BTTask_ApplyGameplayEffect.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AIController.h"

// PMTAI Includes
#include "BehaviorTree/BlackboardComponent.h"
#include "FunctionLibraries/PMTAIFunctionLibrary.h"

UBTTask_ApplyGameplayEffect::UBTTask_ApplyGameplayEffect()
{
	GameplayEffectKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_ApplyGameplayEffect, GameplayEffectKey), UGameplayEffect::StaticClass());
}

EBTNodeResult::Type UBTTask_ApplyGameplayEffect::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* AIController = OwnerComp.GetAIOwner();
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);

	if (ASC)
	{
		UGameplayEffect* GE;
		if (bUseGameplayEffectKey && BlackboardComponent)
		{
			GE = Cast<UGameplayEffect>(BlackboardComponent->GetValueAsObject(GameplayEffectKey.SelectedKeyName));
		}
		else
		{
			GE = GameplayEffect.GetDefaultObject();
		}
		FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
		EffectContext.AddSourceObject(AIController);
		ASC->ApplyGameplayEffectToSelf(GE, Level, EffectContext);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
