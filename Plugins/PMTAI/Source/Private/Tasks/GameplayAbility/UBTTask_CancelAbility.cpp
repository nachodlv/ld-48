﻿
#include "Tasks/GameplayAbility/UBTTask_CancelAbility.h"

#include "AbilitySystemComponent.h"


#include "FunctionLibraries/PMTAIFunctionLibrary.h"

EBTNodeResult::Type UUBTTask_CancelAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (!ASC)
	{
		return EBTNodeResult::Failed;
	}
	ASC->CancelAbilities(&TagContainer);
	return EBTNodeResult::Succeeded;
}
