﻿
#include "Tasks/BTService_IsInDistance.h"

#include "AIController.h"


#include "BehaviorTree/BlackboardComponent.h"

UBTService_IsInDistance::UBTService_IsInDistance()
{
	Result.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_IsInDistance, Result));

	Target.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_IsInDistance, Target), AActor::StaticClass());
}

void UBTService_IsInDistance::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIController* Controller = OwnerComp.GetAIOwner();
	APawn* Pawn = Controller->GetPawn();
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	bool bInRange = false;
	if (BlackboardComponent && Pawn)
	{
		AActor* Actor = Cast<AActor>(BlackboardComponent->GetValueAsObject(Target.SelectedKeyName));
		if (Actor)
		{
			const float CurrentDistance = FVector::Dist(Actor->GetActorLocation(), Pawn->GetActorLocation());
			bInRange = CurrentDistance < Distance;
		}
	}

	BlackboardComponent->SetValueAsBool(Result.SelectedKeyName, bInRange);
}
