﻿

#pragma once

#include "CoreMinimal.h"


#include "BehaviorTree/BTService.h"
#include "BehaviorTree/BTTaskNode.h"

#include "UObject/Object.h"
#include "BTService_IsInDistance.generated.h"

/**
 *
 */
UCLASS()
class PMTAI_API UBTService_IsInDistance : public UBTService
{
	GENERATED_BODY()

public:
	UBTService_IsInDistance();

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector Result;

	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector Target;

	UPROPERTY(EditAnywhere)
	float Distance = 100.0f;
};
