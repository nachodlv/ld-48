﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_ActivateInstantAbility.generated.h"

class UGameplayAbility;

/** Task that activates an instant ability */
UCLASS()
class PMTAI_API UBTTask_ActivateInstantAbility : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_ActivateInstantAbility();

protected:
	/** The ability to be activated */
	UPROPERTY(EditAnywhere, Category="PMTAI")
	TSubclassOf<UGameplayAbility> Ability;

	/** Starts this task, should return Succeeded, Failed or InProgress */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** @return string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;
};
