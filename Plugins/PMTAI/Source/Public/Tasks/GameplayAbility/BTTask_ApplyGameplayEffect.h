﻿#pragma once

// UE Includes
#include "BehaviorTree/BTTaskNode.h"
#include "CoreMinimal.h"

#include "BTTask_ApplyGameplayEffect.generated.h"

/**
 * Applies a gameplay effect to the pawn
 */
UCLASS()
class PMTAI_API UBTTask_ApplyGameplayEffect : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_ApplyGameplayEffect();

	/** Applies the gameplay effect */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Gameplay effect that will be applied */
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (EditCondition = "!bUseGameplayEffectKey"))
	TSubclassOf<class UGameplayEffect> GameplayEffect;
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (EditCondition = "bUseGameplayEffectKey"))
	FBlackboardKeySelector GameplayEffectKey;
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (InlineEditConditionToggle))
	bool bUseGameplayEffectKey = false;

	/** Level of the gameplay effect */
	UPROPERTY(EditAnywhere, Category="PMTAI")
	float Level = 1.0f;
};
