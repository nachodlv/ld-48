﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTTaskNode.h"

#include "UObject/Object.h"
#include "UBTTask_CancelAbility.generated.h"

/**
 *
 */
UCLASS()
class PMTAI_API UUBTTask_CancelAbility : public UBTTaskNode
{
	GENERATED_BODY()

public:
	/** Starts this task, should return Succeeded, Failed or InProgress */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere, Category="LD48")
	FGameplayTagContainer TagContainer;
};
