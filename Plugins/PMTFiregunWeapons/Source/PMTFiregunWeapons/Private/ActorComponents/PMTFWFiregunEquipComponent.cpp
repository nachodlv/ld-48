// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTFWFiregunEquipComponent.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunData.h"

UPMTFWFiregunEquipComponent::UPMTFWFiregunEquipComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	bWantsInitializeComponent = true;
}

void UPMTFWFiregunEquipComponent::EquipFiregun(FPMTFWFiregunInstanceHandle InHandle)
{
	EquippedFiregunHandle = InHandle;
}

bool UPMTFWFiregunEquipComponent::HasAnyFiregunEquipped() const
{
	return GetCurrentFiregunHandle().IsValid();
}

FPMTFWFiregunInstanceHandle UPMTFWFiregunEquipComponent::GetCurrentFiregunHandle() const
{
	return EquippedFiregunHandle;
}

void UPMTFWFiregunEquipComponent::InitializeComponent()
{
	Super::InitializeComponent();
	IPMTFWFiregunTrackerInterface* TrackerInterface = Cast<IPMTFWFiregunTrackerInterface>(GetOwner());
	if (!TrackerInterface)
	{
		TArray<UActorComponent*> TrackerComponents = GetOwner()->GetComponentsByInterface(UPMTFWFiregunTrackerInterface::StaticClass());
		check(TrackerComponents.Num() > 0);
		// TODO(Brian): Fix log category.
		UActorComponent* FirstComponent = TrackerComponents[0];
		UE_LOG(LogTemp, Verbose, TEXT("'%s': Grabbing first component found '%s' from a total of '%d'"), ANSI_TO_TCHAR(__FUNCTION__), *FirstComponent->GetName(), TrackerComponents.Num());
		Tracker = FirstComponent;
	}
	else
	{
		Tracker = GetOwner();
	}
	check(Tracker);
	InstanceRemovedDelegateHandle =
		Tracker->GetFiregunInstanceRemovedDelegate().AddUObject(this, &UPMTFWFiregunEquipComponent::OnFiregunInstanceRemoved);
}

void UPMTFWFiregunEquipComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (Tracker)
	{
		Tracker->GetFiregunInstanceRemovedDelegate().Remove(InstanceRemovedDelegateHandle);
	}
	Super::EndPlay(EndPlayReason);
}

void UPMTFWFiregunEquipComponent::OnFiregunInstanceRemoved(const FPMTFWFiregunInstanceDataItem& FiregunInstance)
{
	if (FiregunInstance.Handle == EquippedFiregunHandle)
	{
		EquippedFiregunHandle.Reset();
	}
}
