// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTFWFiregunTrackerComp.h"

// PMTFW Includes
#include "Globals/PMTFWGlobals.h"
#include "Firegun/Attachments/PMTFWAttachmentTypes.h"
#include "Firegun/Attachments/PMTFWSimpleAttachment.h"

// PMTC Includes
#include "Globals/PMTCGlobals.h"

// UE Includes
#include "Net/UnrealNetwork.h"

namespace
{
	// Used as return type for functions returning a reference.
	static FPMTFWFiregunInstanceDataItem InvalidInstanceItem;
}

UPMTFWFiregunTrackerComp::UPMTFWFiregunTrackerComp()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;

	SetIsReplicatedByDefault(true);
}

FPMTFWFiregunInstanceHandle UPMTFWFiregunTrackerComp::AddFiregunInstance(TUniquePtr<FPMTFWFiregunInstanceCreationParams> CreationParameters)
{
	check(CreationParameters.IsValid());
	FPMTFWFiregunInstanceDataItem DataItem;
	DataItem.FiregunDataClass = CreationParameters->FiregunDataClass;
	DataItem.CurrentBulletsInMagazine = CreationParameters->CurrentBulletsInMagazine;
	DataItem.CustomFiregunData = CreationParameters->CustomFiregunData;
	DataItem.Handle = FPMTFWFiregunInstanceHandle::GenerateNewHandle();

	FiregunInstances.AddItem(DataItem);
	OnRep_FiregunInstances();

	return DataItem.Handle;
}

FPMTFWFiregunInstanceHandle UPMTFWFiregunTrackerComp::AddExistingFiregunInstance(const FPMTFWFiregunInstanceDataItem& FiregunInstanceToAdd)
{
	check(!IsHandleValid(FiregunInstanceToAdd.Handle));
	FiregunInstances.AddItem(FiregunInstanceToAdd);
	OnRep_FiregunInstances();

	return FiregunInstanceToAdd.Handle;
}

void UPMTFWFiregunTrackerComp::RemoveFiregunInstance(FPMTFWFiregunInstanceHandle InInstanceHandle)
{
	if (FiregunInstances.RemoveItem(InInstanceHandle, true))
	{
		OnRep_FiregunInstances();
	}
}

bool UPMTFWFiregunTrackerComp::IsHandleValid(FPMTFWFiregunInstanceHandle InInstanceHandle) const
{
	if (InInstanceHandle.IsValid())
	{
		for (const FPMTFWFiregunInstanceDataItem& SingleItem : FiregunInstances.Fireguns)
		{
			if (SingleItem.Handle == InInstanceHandle)
			{
				return true;
			}
		}
	}
	return false;
}

const FPMTFWFiregunInstanceDataItem& UPMTFWFiregunTrackerComp::GetInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle) const
{
	check(IsHandleValid(InInstanceHandle));
	for (const FPMTFWFiregunInstanceDataItem& SingleItem : FiregunInstances.Fireguns)
	{
		if (SingleItem.Handle == InInstanceHandle)
		{
			return SingleItem;
		}
	}
	return InvalidInstanceItem;
}

FPMTFWFiregunInstanceDataItem& UPMTFWFiregunTrackerComp::GetMutableInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle)
{
	check(IsHandleValid(InInstanceHandle));
	for (FPMTFWFiregunInstanceDataItem& SingleItem : FiregunInstances.Fireguns)
	{
		if (SingleItem.Handle == InInstanceHandle)
		{
			return SingleItem;
		}
	}
	return InvalidInstanceItem;
}

FPMTFWAttachmentInstanceHandle UPMTFWFiregunTrackerComp::AddAttachmentToInstance(
	FPMTFWFiregunInstanceHandle InInstanceHandle, TSubclassOf<UPMTFWSimpleAttachment> InAttachmentClass, int32 InUniqueSeed,
	FGameplayAbilityTargetDataHandle AttachmentCustomData)
{
	if (IsHandleValid(InInstanceHandle) && InAttachmentClass)
	{
		FPMTFWFiregunInstanceDataItem& MutableInstance = GetMutableInstanceDataItem(InInstanceHandle);
		const UPMTFWSimpleAttachment* const AttachmentCDO = InAttachmentClass.GetDefaultObject();
		if (CanAddAttachment(MutableInstance, AttachmentCDO, AttachmentCustomData))
		{
			FPMTFWFiregunAttachmentDataItem AttachmentDataItem;
			AttachmentDataItem.AttachmentClass = InAttachmentClass;
			AttachmentDataItem.AttachmentCustomData = AttachmentCustomData;
			AttachmentDataItem.AttachmentHandle = FPMTFWAttachmentInstanceHandle::GenerateNewHandle();
			AttachmentDataItem.UniqueSeed = InUniqueSeed;

			FPMTFWFiregunAttachmentDataItem& ReturnedDataItem = MutableInstance.FiregunAttachments.Attachments.Emplace_GetRef(AttachmentDataItem);
			MutableInstance.FiregunAttachments.MarkItemDirty(ReturnedDataItem);
			// Make sure to replicate the newly added attachment to the already owned firegun.
			FiregunInstances.MarkItemDirty(MutableInstance);

			MutableInstance.PostReplicatedChange(FiregunInstances);

			return ReturnedDataItem.AttachmentHandle;
		}
	}
	return FPMTFWAttachmentInstanceHandle();
}

bool UPMTFWFiregunTrackerComp::RemoveAttachmentFromInstance(
	FPMTFWFiregunInstanceHandle InInstanceHandle, FPMTFWFiregunInstanceHandle InAttachmentHandle)
{
	if (IsHandleValid(InInstanceHandle))
	{
		// TODO(Brian): Fix me.
		return false;
	}
	return false;
}

FPMTFWFiregunInstanceAddedDelegate& UPMTFWFiregunTrackerComp::GetFiregunInstanceAddedDelegate()
{
	return FiregunInstanceAddedDelegate;
}

FPMTFWFiregunInstanceRemovedDelegate& UPMTFWFiregunTrackerComp::GetFiregunInstanceRemovedDelegate()
{
	return FiregunInstanceRemovedDelegate;
}

FPMTFWFiregunInstanceChangedDelegate& UPMTFWFiregunTrackerComp::GetFiregunInstanceChangedDelegate()
{
	return FiregunInstanceChangedDelegate;
}

FPMTFWFiregunAttachmentInstanceAddedDelegate& UPMTFWFiregunTrackerComp::GetFiregunAttachmentInstanceAddedDelegate()
{
	return FiregunAttachmentInstanceAddedDelegate;
}

FPMTFWFiregunAttachmentInstanceRemovedDelegate& UPMTFWFiregunTrackerComp::GetFiregunAttachmentInstanceRemovedDelegate()
{
	return FiregunAttachmentInstanceRemovedDelegate;
}

FPMTFWFiregunAttachmentInstanceChangedDelegate& UPMTFWFiregunTrackerComp::GetFiregunAttachmentInstanceChangedDelegate()
{
	return FiregunAttachmentInstanceChangedDelegate;
}

TArray<FPMTFWFiregunInstanceHandle> UPMTFWFiregunTrackerComp::GetAllFiregunHandles() const
{
	TArray<FPMTFWFiregunInstanceHandle> Handles;
	for (const FPMTFWFiregunInstanceDataItem& DataItem : FiregunInstances.Fireguns)
	{
		Handles.Add(DataItem.Handle);
	}
	return Handles;
}

void UPMTFWFiregunTrackerComp::BeginPlay()
{
	Super::BeginPlay();
#if WITH_EDITOR
	PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, bDebugDataInScreen);
#endif // WITH_EDITOR
}

void UPMTFWFiregunTrackerComp::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
#if WITH_EDITOR
	PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, false);
#endif // WITH_EDITOR
	Super::EndPlay(EndPlayReason);
}

void UPMTFWFiregunTrackerComp::InitializeComponent()
{
	Super::InitializeComponent();
	FiregunInstances.Owner = this;
}

void UPMTFWFiregunTrackerComp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	if (TrackReplicationMode == EPMTFWFiregunTrackReplicationMode::All)
	{
		DOREPLIFETIME(UPMTFWFiregunTrackerComp, FiregunInstances);
	}
	else if (TrackReplicationMode == EPMTFWFiregunTrackReplicationMode::OwnerOnly)
	{
		DOREPLIFETIME_CONDITION(UPMTFWFiregunTrackerComp, FiregunInstances, COND_OwnerOnly);
	}
	else
	{
		DISABLE_REPLICATED_PROPERTY(UPMTFWFiregunTrackerComp, FiregunInstances);
	}
}

void UPMTFWFiregunTrackerComp::OnRep_FiregunInstances()
{
	FiregunInstanceUpdateDelegate.ExecuteIfBound(this, FiregunInstances);
}

bool UPMTFWFiregunTrackerComp::CanAddAttachment(const FPMTFWFiregunInstanceDataItem& InInstance, const UPMTFWSimpleAttachment* InAttachment,
	FGameplayAbilityTargetDataHandle AttachmentCustomData)
{
	for (const FPMTFWFiregunAttachmentDataItem& SingleAttachment : InInstance.FiregunAttachments.Attachments)
	{
		if (InAttachment->GetAttachmentType() == SingleAttachment.AttachmentClass.GetDefaultObject()->GetAttachmentType())
		{
			return false;
		}
	}
	return true;
}

#if WITH_EDITOR

void UPMTFWFiregunTrackerComp::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (PropertyChangedEvent.Property && PropertyChangedEvent.Property->GetNameCPP() == GET_MEMBER_NAME_STRING_CHECKED(UPMTFWFiregunTrackerComp, bDebugDataInScreen))
	{
		PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, bDebugDataInScreen);
	}
}

#endif // WITH_EDITOR
