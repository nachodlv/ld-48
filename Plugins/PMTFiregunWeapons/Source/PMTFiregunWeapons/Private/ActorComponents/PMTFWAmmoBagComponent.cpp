// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTFWAmmoBagComponent.h"

// PMTC Includes
#include "FunctionLibraries/PMTCAttributeTagFuncLibrary.h"

// UE Includes
#include "Net/UnrealNetwork.h"

UPMTFWAmmoBagComponent::UPMTFWAmmoBagComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UPMTFWAmmoBagComponent::NotifyTagAttributeChanged(const FPMTCTagAttribute& InTagAttribute)
{
	BulletCountChanged.Broadcast(InTagAttribute);
}

void UPMTFWAmmoBagComponent::AddAmmoAttributes(const TArray<FPMTCTagAttribute>& AmmoTypes)
{
	for (const FPMTCTagAttribute& NewAmmoType : AmmoTypes)
	{
		BulletsPerAmmoType.AddTagAttribute(NewAmmoType);
	}
}

bool UPMTFWAmmoBagComponent::IncrementBulletCount(FGameplayTag AmmoTag, int32 InValue)
{
	check(GetOwner()->HasAuthority());
	const int32 CurrentValue = static_cast<int32>(BulletsPerAmmoType.GetAttributeCurrentValue(AmmoTag));
	const int32 FinalValue = CurrentValue + InValue;
	// Technically, we shouldn't have negative values as bullet count.
	constexpr float BulletMinClamp = 0.0f;
	const bool bSet = UPMTCAttributeTagFuncLibrary::SetTagAttributeCurrentValueInArray(BulletsPerAmmoType, AmmoTag, static_cast<float>(FinalValue), BulletMinClamp);
	if (bSet)
	{
		for (const FPMTCTagAttribute& TagAttribute : BulletsPerAmmoType.Attributes)
		{
			if (TagAttribute.Tag == AmmoTag)
			{
				NotifyTagAttributeChanged(TagAttribute);
				break;
			}
		}
	}
	return bSet;
}

bool UPMTFWAmmoBagComponent::SetBulletCountInitialValue(FGameplayTag AmmoTag, int32 InValue)
{
	check(GetOwner()->HasAuthority());
	return UPMTCAttributeTagFuncLibrary::SetTagAttributeInitialValueInArray(BulletsPerAmmoType, AmmoTag, static_cast<float>(InValue));
}

bool UPMTFWAmmoBagComponent::AddModifierToAmmo(FGameplayTag AmmoTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue)
{
	check(GetOwner()->HasAuthority());
	check(InModifierClass);
	return UPMTCAttributeTagFuncLibrary::AddModifierToTagAttributeInArray(BulletsPerAmmoType, AmmoTag, InModifierClass, InValue);
}

bool UPMTFWAmmoBagComponent::RemoveModifierFromAmmo(FGameplayTag AmmoTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove)
{
	constexpr int32 AllStacks = -1;
	check(GetOwner()->HasAuthority());
	check(InModifierClass);
	check(StacksToRemove == AllStacks || StacksToRemove > 0);
	return UPMTCAttributeTagFuncLibrary::RemoveModifierFromTagAttributeInArray(BulletsPerAmmoType, AmmoTag, InModifierClass, StacksToRemove);
}

float UPMTFWAmmoBagComponent::GetCountForBulletType(FGameplayTag AmmoTag)
{
	return BulletsPerAmmoType.GetAttributeCurrentValue(AmmoTag);
}

void UPMTFWAmmoBagComponent::InitializeComponent()
{
	Super::InitializeComponent();
	BulletsPerAmmoType.Owner = this;
}

void UPMTFWAmmoBagComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UPMTFWAmmoBagComponent, BulletsPerAmmoType);
}

void UPMTFWAmmoBagComponent::OnRep_BulletsPerAmmoType()
{

}
