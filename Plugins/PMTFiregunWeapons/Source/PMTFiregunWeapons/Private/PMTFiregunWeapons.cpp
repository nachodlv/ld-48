// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMTFiregunWeapons.h"

// PMTFW Includes
#include "Globals/PMTFWGlobals.h"

#define LOCTEXT_NAMESPACE "FPMTFiregunWeaponsModule"

class FPMTFiregunWeaponsModule : public IPMTFiregunWeaponsModule
{
public:

	/** IModuleInterface implementation */
	void StartupModule() override
	{
		// This is loaded upon first request
		FiregunWeaponGlobals = nullptr;
		GetFiregunWeaponsGlobals()->InitGlobalData();
	}

	void ShutdownModule() override
	{
		if (FiregunWeaponGlobals && FiregunWeaponGlobals->IsRooted())
		{
			FiregunWeaponGlobals->RemoveFromRoot();
		}
		FiregunWeaponGlobals = nullptr;
		/*if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
		{
			SettingsModule->UnregisterSettings("Project", "PMTPlugins", "FiregunWeapons");
		}*/
	}

	UPMTFWGlobals* GetFiregunWeaponsGlobals() override
	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_IPMTFiregunWeaponsModule_GetFiregunWeaponsGlobals);
		// Defer loading of globals to the first time it is requested
		if (!FiregunWeaponGlobals)
		{
			FSoftClassPath FiregunWeaponsClassName = (UPMTFWGlobals::StaticClass()->GetDefaultObject<UPMTFWGlobals>())->PMTFWGlobalsClassName;

			UClass* SingletonClass = FiregunWeaponsClassName.TryLoadClass<UObject>();
			checkf(SingletonClass != nullptr, TEXT("Firegun Weapons config value PMTFWGlobalsClassName is not a valid class name."));

			FiregunWeaponGlobals = NewObject<UPMTFWGlobals>(GetTransientPackage(), SingletonClass, NAME_None);
			FiregunWeaponGlobals->AddToRoot();
		}
		check(FiregunWeaponGlobals);
		return FiregunWeaponGlobals;
	}

	bool IsFiregunWeaponsGlobalsAvailable() override
	{
		return FiregunWeaponGlobals != nullptr;
	}

	// Module members.
	UPMTFWGlobals* FiregunWeaponGlobals = nullptr;
};

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FPMTFiregunWeaponsModule, PMTFiregunWeapons)
