// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilities/PMTFWInstantFireAbility.h"

// PMTFW Includes
#include "Globals/PMTFWGlobals.h"
#include "Firegun/Ammo/PMTFWAmmunition.h"
#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregun.h"
#include "Firegun/PMTFWFiregunData.h"
#include "Firegun/PMTFWFiregunUtilities.h"
#include "FunctionLibraries/PMTFWFiregunFunctionLibrary.h"
#include "Interfaces/PMTFWFiregunProvider.h"

// PMTGU Includes
#include "GameplayAbilityTasks/PMTGUWaitTargetData.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

UPMTFWInstantFireAbility::UPMTFWInstantFireAbility()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

void UPMTFWInstantFireAbility::FireBullet()
{
	UAbilitySystemComponent* OwnerASC = CurrentActorInfo->AbilitySystemComponent.Get();
	FScopedPredictionWindow	ScopedPrediction(OwnerASC, !OwnerASC->ScopedPredictionKey.IsValidForMorePrediction());
	// We check here since client produces target data that server receives and applies.
	// If server sees that it doesn't fulfill the requirements, it cancels the ability instead.
	if (!CheckCost(CurrentSpecHandle, CurrentActorInfo) || !CheckCooldown(CurrentSpecHandle, CurrentActorInfo))
	{
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
		return;
	}
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(CurrentActorInfo->AvatarActor.Get());
	check(FiregunProvider);
	if (ProjectileType == EPMTFWProjectileType::Trace)
	{
		const FPMTFWFiregunInstanceDataItem& CurrentInstance = FiregunProvider->GetCurrentEquippedFiregun();
		APMTFWFiregun* CurrentFiregun = FiregunProvider->GetMutableCurrentPhysicalFiregun();

		FPMTFWFiregunTraceParameters TraceParameters = GenerateTraceParameters(CurrentInstance, FiregunProvider->GetMutableCurrentFiregunProxy(), CurrentFiregun);

		FPMTFWFiregunTraceResults TraceResults = UPMTFWFiregunFunctionLibrary::DoTrace(CurrentFiregun, CurrentActorInfo->PlayerController.Get(), TraceParameters);
		FPMTFWBulletHitTargetData TargetData = UPMTFWFiregunFunctionLibrary::GenerateHitTargetData(TraceResults);
		FPMTFWBulletHitTargetData* CopyTargetData = new FPMTFWBulletHitTargetData();
		*CopyTargetData = TargetData;
		FGameplayAbilityTargetDataHandle TargetDataHandle;
		TargetDataHandle.Add(CopyTargetData);
		OwnerASC->CallServerSetReplicatedTargetData(CurrentSpecHandle, CurrentActivationInfo.GetActivationPredictionKey(), TargetDataHandle, FGameplayTag(), OwnerASC->GetPredictionKeyForNewAction());
		if (IsPredictingClient())
		{
			// Make sure to call it in the client.
			OnValidTargetData(TargetDataHandle);
		}
	}
	else
	{
		ensureAlwaysMsgf(false, TEXT("Not implemented."));
	}
}

void UPMTFWInstantFireAbility::ExternalEndAbility()
{
	constexpr bool bReplicateEndAbility = true;
	constexpr bool bWasCancelled = false;
	EndAbility(CurrentSpecHandle, CurrentActorInfo,
		CurrentActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UPMTFWInstantFireAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(OwnerInfo, &ActivationInfo))
	{
		WaitTargetDataTask = UPMTGUWaitTargetData::WaitClientTargetData(this, FName(), false);
		WaitTargetDataTask->ValidData.AddDynamic(this, &UPMTFWInstantFireAbility::OnValidTargetData);
		WaitTargetDataTask->ReadyForActivation();

		FireBullet();
	}
}

void UPMTFWInstantFireAbility::OnValidTargetData(const FGameplayAbilityTargetDataHandle& Data)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo))
	{
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
		return;
	}
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(CurrentActorInfo->AvatarActor.Get());
	check(FiregunProvider);
	const FPMTFWFiregunInstanceDataItem& CurrentInstance = FiregunProvider->GetCurrentEquippedFiregun();
	// We make a copy so we can properly cache the hit results and send it.
	FPMTFWBulletHitTargetData CopyBulletTargetData = *(static_cast<const FPMTFWBulletHitTargetData*>(Data.Get(0)));
	// We are server receiving data from client.
	if (CopyBulletTargetData.bNeedRegeneration)
	{
		APMTFWFiregun* CurrentFiregun = FiregunProvider->GetMutableCurrentPhysicalFiregun();

		FPMTFWFiregunTraceParameters TraceParameters = GenerateTraceParameters(CurrentInstance, FiregunProvider->GetMutableCurrentFiregunProxy(), CurrentFiregun);
		FPMTFWFiregunTraceResults TraceResults = UPMTFWFiregunFunctionLibrary::SimulateTrace(CurrentFiregun, CopyBulletTargetData, TraceParameters);
		CopyBulletTargetData.HitResults = TraceResults.Hits;
		CopyBulletTargetData.bNeedRegeneration = false;
	}

	UPMTFWGlobals& FiregunWeaponGlobals = UPMTFWGlobals::Get();
	UPMTFWAmmunition* Ammunition = FiregunWeaponGlobals.GetAmmunition(AmmoType);
	//ensureAlwaysMsgf(Ammunition->GetAttributesGameplayEffect().IsValid(), TEXT("Ammunition attributes gameplay effect type '%s' was not loaded. Will do a sync load."), *Ammunition->AmmoTypeName.ToString());
	UGameplayEffect* AmmoAttributeGE = Ammunition->GetAttributesGameplayEffect().LoadSynchronous()->GetDefaultObject<UGameplayEffect>();
	//ensureAlwaysMsgf(Ammunition->GetHitGameplayEffect().IsValid(), TEXT("Ammunition gameplay effect type '%s' was not loaded. Will do a sync load."), *Ammunition->AmmoTypeName.ToString());
	UGameplayEffect* GameplayEffectToApply = Ammunition->GetHitGameplayEffect().LoadSynchronous()->GetDefaultObject<UGameplayEffect>();
	APMTFWActiveFiregunsProxy* ActiveProxy = FiregunProvider->GetMutableCurrentFiregunProxy();
	check(ActiveProxy);

	UAbilitySystemComponent* OwnerASC = GetAbilitySystemComponentFromActorInfo_Checked();
	// Apply temporary gameplay effect for the bullet and remove it once we apply the hit to all targets.
	FActiveGameplayEffectHandle EffectHandle = ActiveProxy->ApplyGameplayEffect(FGameplayEffectSpec(AmmoAttributeGE, OwnerASC->MakeEffectContext()), CurrentInstance.Handle);

	check(EffectHandle.IsValid());
	for (const FHitResult& SingleHit : CopyBulletTargetData.HitResults)
	{
		FGameplayCueParameters CueParameters (GetContextFromOwner(Data));
		CueParameters.Instigator = GetActorInfo().OwnerActor;
		CueParameters.Location = SingleHit.Location;
		IAbilitySystemInterface* AbilitySystemInterface = Cast<IAbilitySystemInterface>(SingleHit.GetActor());
		if (AbilitySystemInterface)
		{

			UAbilitySystemComponent* HitASC = AbilitySystemInterface->GetAbilitySystemComponent();
			if (HitASC)
			{
				CueParameters.SourceObject =  HitASC->GetOwner();
				FGameplayEffectContextHandle Context = OwnerASC->MakeEffectContext();
				Context.AddHitResult(SingleHit);
				Context.AddSourceObject(OwnerASC);
				OwnerASC->ApplyGameplayEffectSpecToTarget(FGameplayEffectSpec(GameplayEffectToApply, Context), HitASC,
					CurrentActivationInfo.GetActivationPredictionKey());
			}
		}
		OwnerASC->ExecuteGameplayCue(ImpactCue, CueParameters);
	}
	FGameplayCueParameters CueParameters (GetContextFromOwner(Data));
	CueParameters.Instigator = GetActorInfo().OwnerActor;
	OwnerASC->ExecuteGameplayCue(MuzzleCue, CueParameters);
	const bool bRemoved = ActiveProxy->RemoveGameplayEffect(EffectHandle, CurrentInstance.Handle);
	check(bRemoved);
}

FPMTFWFiregunTraceParameters UPMTFWInstantFireAbility::GenerateTraceParameters(
	const FPMTFWFiregunInstanceDataItem& Instance, const APMTFWActiveFiregunsProxy* ActiveProxy, APMTFWFiregun* Firegun)
{
	FPMTFWFiregunTraceParameters TraceParameters = UPMTFWFiregunFunctionLibrary::GetTraceParameters(Instance, ActiveProxy, Firegun);
	TraceParameters.ActorsToIgnore.Add(Firegun);
	AActor* AttachParentActor = Firegun->GetAttachParentActor();
	if (AttachParentActor)
	{
		TraceParameters.ActorsToIgnore.Add(AttachParentActor);
	}
	TraceParameters.TraceProfile = ProfileName;
	TraceParameters.bComplexTrace = bComplexTrace;
	TraceParameters.bDebug = bDebug;
	TraceParameters.bIgnoreBlockingHits = bIgnoreBlockingHits;
	return TraceParameters;
}
