// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilities/PMTFWLocalFireAbility.h"

// PMTFW Includes
#include "GameplayAbilities/PMTFWInstantFireAbility.h"
#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregunData.h"
#include "Interfaces/PMTFWFiregunProvider.h"

// UE Includes
#include "AbilitySystemComponent.h"

namespace
{
	float GetCurrentEquippedWeaponFireRate(AActor* Actor)
	{
		return Cast<IPMTFWFiregunProvider>(Actor)->GetMutableCurrentFiregunProxy()->GetFiregunFireRate();
	}
}

UPMTFWLocalFireAbility::UPMTFWLocalFireAbility()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalOnly;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

void UPMTFWLocalFireAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, true);
		return;
	}
	UAbilitySystemComponent* OwnerASC = GetAbilitySystemComponentFromActorInfo_Checked();
	FGameplayAbilitySpec* Spec = OwnerASC->FindAbilitySpecFromClass(InstantFireAbilityClass);
	// The spec can't be null since an instant fire ability must exist
	// within the local one.
	check(Spec);
	InstantFireAbilityHandle = Spec->Handle;
	InstantFireAbility =
		CastChecked<UPMTFWInstantFireAbility>(Spec->GetPrimaryInstance());
	FScopedServerAbilityRPCBatcher RPCBatcher(OwnerASC, InstantFireAbilityHandle);
	const bool bActivated = OwnerASC->TryActivateAbility(InstantFireAbilityHandle, true);
	if (!bActivated)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
		return;
	}
	// TODO(Brian): Handle semi-automatic fire weapons.
	InitWaitInputReleaseTask();

	// In case the task finished because it tested that it was released already.
	if (!WaitInputReleaseTask)
	{
		// Ability end is handled in "OnInputReleased"
		return;
	}

	InitRateOfFireDelay(GetCurrentEquippedWeaponFireRate(CurrentActorInfo->AvatarActor.Get()));
}

void UPMTFWLocalFireAbility::ApplyCost(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo) const
{
	// We don't apply cost here since the instant ability that is predicted
	// will fire.
	UGameplayAbility::ApplyCost(Handle, ActorInfo, ActivationInfo);
}

void UPMTFWLocalFireAbility::OnRateOfFireDelayFinished()
{
	Super::OnRateOfFireDelayFinished();
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo))
	{
		InstantFireAbility->ExternalEndAbility();
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
		return;
	}
	InstantFireAbility->FireBullet();
	InitRateOfFireDelay(GetCurrentEquippedWeaponFireRate(CurrentActorInfo->AvatarActor.Get()));
}

void UPMTFWLocalFireAbility::OnInputReleased(float TimeHeld)
{
	Super::OnInputReleased(TimeHeld);
	InstantFireAbility->ExternalEndAbility();
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
