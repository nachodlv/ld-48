// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilities/PMTFWBaseAbility.h"

void UPMTFWBaseAbility::OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnRemoveAbility(ActorInfo, Spec);
	AbilityRemovedDelegate.ExecuteIfBound(ActorInfo->AbilitySystemComponent.Get(), Spec.Handle);
}

FPMTGUAbilityRemovedDelegate& UPMTFWBaseAbility::GetAbilityRemovedDelegate()
{
	return AbilityRemovedDelegate;
}
