// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilities/PMTFWBaseFiregunAbility.h"

// PMTFW Includes
#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregunData.h"
#include "Globals/PMTFWGlobals.h"
#include "Interfaces/PMTFWFiregunProvider.h"

// UE Includes
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_WaitInputPress.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"

bool UPMTFWBaseFiregunAbility::CheckCooldown(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	return Super::CheckCooldown(Handle, ActorInfo, OptionalRelevantTags) && CanFire(ActorInfo);
}

void UPMTFWBaseFiregunAbility::ApplyCooldown(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo) const
{
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(ActorInfo->AvatarActor.Get());
	check(FiregunProvider);
	FindOrAddWeaponHandle(FiregunProvider->GetCurrentEquippedFiregun().Handle).LastTimeFire = GetWorld()->GetTimeSeconds();
}

bool UPMTFWBaseFiregunAbility::CheckCost(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	return Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags) && HasBullets(ActorInfo);
}

void UPMTFWBaseFiregunAbility::ApplyCost(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo) const
{
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(ActorInfo->AvatarActor.Get());
	if (!FiregunProvider)
	{
		UE_LOG(PMTFWFiregunAbilityLog, Error, TEXT("'%s': Actor '%s' does not inherit from IPMTFWFiregunProvider or is not valid."),
			ANSI_TO_TCHAR(__FUNCTION__),
			ActorInfo->AvatarActor.Get() ?
			*ActorInfo->AvatarActor.Get()->GetName() : TEXT("NullActor"));
		return;
	}
	FPMTFWFiregunInstanceDataItem& MutableInstance = FiregunProvider->GetMutableCurrentEquippedFiregun();
	// TODO(Brian): Pass proper amount of bullets shot.
	MutableInstance.HandleBulletFired(AmmoType);
	FiregunProvider->NotifyBulletCountChanged(MutableInstance);
}

void UPMTFWBaseFiregunAbility::InitRateOfFireDelay(float TimeToWait)
{
	WaitDelayRateOfFireTask = UAbilityTask_WaitDelay::WaitDelay(this, TimeToWait);
	WaitDelayRateOfFireTask->OnFinish.AddDynamic(this, &UPMTFWBaseFiregunAbility::OnRateOfFireDelayFinished);
	WaitDelayRateOfFireTask->ReadyForActivation();
}

void UPMTFWBaseFiregunAbility::ClearRateOfFireDelay()
{
	if (WaitDelayRateOfFireTask)
	{
		WaitDelayRateOfFireTask->OnFinish.RemoveDynamic(this, &UPMTFWBaseFiregunAbility::OnRateOfFireDelayFinished);
		WaitDelayRateOfFireTask->EndTask();
		WaitDelayRateOfFireTask = nullptr;
	}
}

void UPMTFWBaseFiregunAbility::InitWaitInputReleaseTask()
{
	WaitInputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this, true);
	WaitInputReleaseTask->OnRelease.AddDynamic(this, &UPMTFWBaseFiregunAbility::OnInputReleased);
	WaitInputReleaseTask->ReadyForActivation();
}

void UPMTFWBaseFiregunAbility::ClearWaitInputReleaseTask()
{
	if (WaitInputReleaseTask)
	{
		WaitInputReleaseTask->OnRelease.RemoveDynamic(this, &UPMTFWBaseFiregunAbility::OnInputReleased);
		WaitInputReleaseTask->EndTask();
		WaitInputReleaseTask = nullptr;
	}
}

void UPMTFWBaseFiregunAbility::InitWaitInputPressedTask()
{
	WaitInputPressTask = UAbilityTask_WaitInputPress::WaitInputPress(this, false);
	WaitInputPressTask->OnPress.AddDynamic(this, &UPMTFWBaseFiregunAbility::OnInputPressed);
	WaitInputPressTask->ReadyForActivation();
}

void UPMTFWBaseFiregunAbility::ClearWaitInputPressedTask()
{
	if (WaitInputPressTask)
	{
		WaitInputPressTask->OnPress.RemoveDynamic(this, &UPMTFWBaseFiregunAbility::OnInputPressed);
		WaitInputPressTask->EndTask();
		WaitInputPressTask = nullptr;
	}
}

void UPMTFWBaseFiregunAbility::OnRateOfFireDelayFinished()
{
	ClearRateOfFireDelay();
}

void UPMTFWBaseFiregunAbility::OnInputReleased(float TimeHeld)
{
	ClearWaitInputReleaseTask();
}

void UPMTFWBaseFiregunAbility::OnInputPressed(float TimeWaited)
{
	ClearWaitInputPressedTask();
}

bool UPMTFWBaseFiregunAbility::CanFire(const FGameplayAbilityActorInfo* ActorInfo) const
{
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(ActorInfo->AvatarActor.Get());
	if (!FiregunProvider)
	{
		UE_LOG(PMTFWFiregunAbilityLog, Error, TEXT("'%s': Actor '%s' does not inherit from IPMTFWFiregunProvider or is not valid."),
			ANSI_TO_TCHAR(__FUNCTION__),
			ActorInfo->AvatarActor.Get() ?
			*ActorInfo->AvatarActor.Get()->GetName() : TEXT("NullActor"));
		return false;
	}
	const float CurrentWorldTimeSeconds = GetWorld()->GetTimeSeconds();
	constexpr float SmallOffset = 0.015;
	return CurrentWorldTimeSeconds + SmallOffset >= (GetLastTimeFire(*FiregunProvider) + FiregunProvider->GetMutableCurrentFiregunProxy()->GetFiregunFireRate());
}

bool UPMTFWBaseFiregunAbility::HasBullets(const FGameplayAbilityActorInfo* ActorInfo) const
{
	IPMTFWFiregunProvider* FiregunProvider = Cast<IPMTFWFiregunProvider>(ActorInfo->AvatarActor.Get());
	if (!FiregunProvider)
	{
		UE_LOG(PMTFWFiregunAbilityLog, Error, TEXT("'%s': Actor '%s' does not inherit from IPMTFWFiregunProvider or is not valid."),
			ANSI_TO_TCHAR(__FUNCTION__),
			ActorInfo->AvatarActor.Get() ?
			*ActorInfo->AvatarActor.Get()->GetName() : TEXT("NullActor"));
		return false;
	}
	return FiregunProvider->GetCurrentEquippedFiregun().HasBulletsInMagazine(AmmoType);
}

float UPMTFWBaseFiregunAbility::GetLastTimeFire(const IPMTFWFiregunProvider& Provider) const
{
	const FPMTFWFiregunInstanceDataItem& CurrentEquippedFiregun = Provider.GetCurrentEquippedFiregun();
	return FindOrAddWeaponHandle(CurrentEquippedFiregun.Handle).LastTimeFire;
}

void UPMTFWBaseFiregunAbility::ClearOldFiregunHandles()
{
	constexpr float GoodEnoughTimeToClear = 10.0f;
	const float WorldTimeSeconds = GetWorld()->GetTimeSeconds();
	int32 Index = 0;
	while (Index < SharedWeaponHandles.Num())
	{
		if (SharedWeaponHandles[Index].LastTimeFire < WorldTimeSeconds + GoodEnoughTimeToClear)
		{
			if (Index != SharedWeaponHandles.Num() - 1)
			{
				SharedWeaponHandles.SwapMemory(Index, SharedWeaponHandles.Num() - 1);
			}
			SharedWeaponHandles.Pop(false);
		}
		else
		{
			++Index;
		}
	}
}

FPMTFWLastTimeFirePerHandle& UPMTFWBaseFiregunAbility::FindOrAddWeaponHandle(FPMTFWFiregunInstanceHandle InHandle) const
{
	for (FPMTFWLastTimeFirePerHandle& Handle : SharedWeaponHandles)
	{
		if (Handle.FiregunHandle == InHandle)
		{
			return Handle;
		}
	}
	return SharedWeaponHandles.Add_GetRef(FPMTFWLastTimeFirePerHandle(InHandle));
}
