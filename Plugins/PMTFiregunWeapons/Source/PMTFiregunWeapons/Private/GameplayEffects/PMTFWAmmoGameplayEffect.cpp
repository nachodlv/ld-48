// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayEffects/PMTFWAmmoGameplayEffect.h"

UPMTFWAmmoGameplayEffect::UPMTFWAmmoGameplayEffect()
{
	DurationPolicy = EGameplayEffectDurationType::Infinite;
}

#if WITH_EDITOR
bool UPMTFWAmmoGameplayEffect::CanEditChange(const FProperty* InProperty) const
{
	const bool bParent = Super::CanEditChange(InProperty);
	if (bParent && InProperty)
	{
		if (InProperty->GetNameCPP() == GET_MEMBER_NAME_STRING_CHECKED(UGameplayEffect, DurationPolicy))
		{
			return false;
		}
	}
	return bParent;
}
#endif // WITH_EDITOR

