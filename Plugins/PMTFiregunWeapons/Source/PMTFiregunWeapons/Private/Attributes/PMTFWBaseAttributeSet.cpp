// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Attributes/PMTFWBaseAttributeSet.h"
#include "GameplayEffectExtension.h"

UPMTFWBaseAttributeSet::UPMTFWBaseAttributeSet()
{
	InitDamage(0.0f);
	InitReloadSpeed(0.0f);
	InitSpread(0.0f);
	InitMaxSpread(0.0f);
	InitSpreadIncrementPerPellet(0.0f);
	InitFireRate(0.0f);
	InitBulletSpeed(0.0f);
	InitBulletRange(0.0f);
	InitCriticalRate(0.0f);
	InitCriticalDamage(0.0f);
	InitWeight(0.0f);
	InitSoundNoise(0.0f);
	InitMagazineSize(0.0f);
	InitPelletsPerBullet(0.0f);
	InitBulletsPerShot(0.0f);
	InitRecoil(0.0f);
}

void UPMTFWBaseAttributeSet::ResetAttributes()
{
	SetDamage(0.0f);
	SetReloadSpeed(0.0f);
	SetSpread(0.0f);
	SetMaxSpread(0.0f);
	SetSpreadIncrementPerPellet(0.0f);
	SetFireRate(0.0f);
	SetBulletSpeed(0.0f);
	SetBulletRange(0.0f);
	SetCriticalRate(0.0f);
	SetCriticalDamage(0.0f);
	SetWeight(0.0f);
	SetSoundNoise(0.0f);
	SetMagazineSize(0.0f);
	SetPelletsPerBullet(0.0f);
	SetBulletsPerShot(0.0f);
	SetPenetration(0.0f);
	SetRecoil(0.0f);
}

void UPMTFWBaseAttributeSet::PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);
	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;
	if (ModifiedAttribute == GetFireRateAttribute())
	{
		SetFireRate(FMath::Clamp(GetFireRate(), 0.01f, TNumericLimits<float>::Max()));
	}
}

bool UPMTFWBaseAttributeSet::PreGameplayEffectExecute(struct FGameplayEffectModCallbackData& Data)
{
	const bool bResult = Super::PreGameplayEffectExecute(Data);
	if (!bResult)
	{
		return false;
	}
	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;
	if (ModifiedAttribute == GetFireRateAttribute())
	{
		SetFireRate(FMath::Clamp(GetFireRate(), 0.01f, TNumericLimits<float>::Max()));
	}
	return true;
}
