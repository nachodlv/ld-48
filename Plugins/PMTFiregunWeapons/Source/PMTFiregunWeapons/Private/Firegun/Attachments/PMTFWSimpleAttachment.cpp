// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Firegun/Attachments/PMTFWSimpleAttachment.h"

const FGameplayTag& UPMTFWSimpleAttachment::GetAttachmentType() const
{
	return AttachmentType;
}

TSoftClassPtr<UGameplayEffect> UPMTFWSimpleAttachment::GetAttributesGameplayEffect() const
{
	return AttributesGameplayEffect;
}

TSoftObjectPtr<UPMTFWAbilitySetDataAsset> UPMTFWSimpleAttachment::GetAbilitySet() const
{
	return FiregunAbilitySet;
}

TArray<TSoftClassPtr<UPMTFWFiregunEquipGE>> UPMTFWSimpleAttachment::GetAdditionalGEs() const
{
	return AttachmentGameplayEffects;
}
