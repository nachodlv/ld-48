// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Firegun/Attachments/PMTFWAttachmentTypes.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunData.h"

void FPMTFWFiregunAttachmentDataItem::PreReplicatedRemove(const FPMTFWFiregunAttachmentArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunAttachmentInstanceRemovedDelegate().Broadcast(*this);
	}
}

void FPMTFWFiregunAttachmentDataItem::PostReplicatedAdd(const FPMTFWFiregunAttachmentArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunAttachmentInstanceAddedDelegate().Broadcast(*this);
	}
}

void FPMTFWFiregunAttachmentDataItem::PostReplicatedChange(const FPMTFWFiregunAttachmentArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunAttachmentInstanceChangedDelegate().Broadcast(*this);
	}
}
