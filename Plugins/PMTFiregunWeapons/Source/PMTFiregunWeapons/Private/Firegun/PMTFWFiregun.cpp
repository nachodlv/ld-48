// Fill out your copyright notice in the Description page of Project Settings.


#include "Firegun/PMTFWFiregun.h"

// UE Includes
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"

DEFINE_LOG_CATEGORY(APMTFWFiregunLog);

// ~ Begin static methods and members.

FName APMTFWFiregun::GetBoxCollisionComponentName()
{
	return FName(TEXT("FiregunBoxCollision"));
}

FName APMTFWFiregun::GetCapsuleCollisionComponentName()
{
	return FName(TEXT("FiregunCapsuleCollision"));
}

FName APMTFWFiregun::GetSkeletalMeshComponentName()
{
	return FName(TEXT("FiregunSkeletalMesh"));
}

FName APMTFWFiregun::GetStaticMeshComponentName()
{
	return FName(TEXT("FiregunStaticMesh"));
}

// ~ End static methods and members.

APMTFWFiregun::APMTFWFiregun(const FObjectInitializer& InObjectInitializer) : Super(InObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);
	NetDormancy = ENetDormancy::DORM_DormantAll;

	// Do not create components for base class.
	if (GetClass() == APMTFWFiregun::StaticClass())
	{
		return;
	}

	BoxCollision = CreateOptionalDefaultSubobject<UBoxComponent>(GetBoxCollisionComponentName());
	if (BoxCollision)
	{
		CachedCollisionComponent = BoxCollision;
	}

	CapsuleCollision = CreateOptionalDefaultSubobject<UCapsuleComponent>(GetCapsuleCollisionComponentName());
	if (CapsuleCollision)
	{
		CachedCollisionComponent = CapsuleCollision;
	}

	RootComponent = CachedCollisionComponent;

	FiregunSkeletalMesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(GetSkeletalMeshComponentName());
	if (FiregunSkeletalMesh)
	{
		CachedMeshComponent = FiregunSkeletalMesh;
	}

	FiregunStaticMesh = CreateOptionalDefaultSubobject<UStaticMeshComponent>(GetStaticMeshComponentName());
	if (FiregunStaticMesh)
	{
		CachedMeshComponent = FiregunStaticMesh;
	}

	UE_CLOG(FiregunSkeletalMesh == nullptr && FiregunStaticMesh == nullptr, APMTFWFiregunLog,
		Fatal, TEXT("You need to specify at least one and not more than one type of physical representation for '%s'"), *GetName());
	UE_CLOG(FiregunSkeletalMesh != nullptr && FiregunStaticMesh != nullptr, APMTFWFiregunLog,
		Fatal, TEXT("You can only specify one type of physical representation for '%s'"), *GetName());

	if (CachedMeshComponent && RootComponent)
	{
		CachedMeshComponent->SetupAttachment(RootComponent);
	}
}

void APMTFWFiregun::SetFiregunSkeletalMesh(USkeletalMesh* InSkeletalMesh)
{
	if (GetFiregunSkeletalMeshComponent())
	{
		GetFiregunSkeletalMeshComponent()->SetSkeletalMesh(InSkeletalMesh);
	}
}

void APMTFWFiregun::SetFiregunStaticMesh(UStaticMesh* InStaticMesh)
{
	if (GetFiregunStaticMeshComponent())
	{
		GetFiregunStaticMeshComponent()->SetStaticMesh(InStaticMesh);
	}
}

FTransform APMTFWFiregun::RetrieveBulletTransform(FName StandardBulletSocketName) const
{
	// TODO(Brian): Check for cannon.
	check(FiregunSkeletalMesh);
	return FiregunSkeletalMesh->GetSocketTransform(StandardBulletSocketName);
}

UBoxComponent* APMTFWFiregun::GetFiregunBoxComponent() const
{
	return BoxCollision;
}

UCapsuleComponent* APMTFWFiregun::GetFiregunCapsuleComponent() const
{
	return CapsuleCollision;
}

UShapeComponent* APMTFWFiregun::GetFiregunCollisionComponent() const
{
	return CachedCollisionComponent;
}

USkeletalMeshComponent* APMTFWFiregun::GetFiregunSkeletalMeshComponent() const
{
	return FiregunSkeletalMesh;
}

UStaticMeshComponent* APMTFWFiregun::GetFiregunStaticMeshComponent() const
{
	return FiregunStaticMesh;
}

UMeshComponent* APMTFWFiregun::GetFiregunMeshComponent() const
{
	check(CachedMeshComponent);
	return CachedMeshComponent;
}

void APMTFWFiregun::BeginPlay()
{
	Super::BeginPlay();
}
