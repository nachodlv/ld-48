// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Firegun/Ammo/PMTFWAmmunition.h"

TSoftClassPtr<UGameplayEffect> UPMTFWAmmunition::GetHitGameplayEffect() const
{
	return HitGameplayEffect;
}

TSoftClassPtr<UPMTFWAmmoGameplayEffect> UPMTFWAmmunition::GetAttributesGameplayEffect() const
{
	return AttributesGameplayEffect;
}

TSoftObjectPtr<UTexture2D> UPMTFWAmmunition::GetIcon() const
{
	return Icon;
}

#if WITH_EDITOR

void UPMTFWAmmunition::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.GetPropertyName() == GET_MEMBER_NAME_CHECKED(UPMTFWAmmunition, AmmoTypeTag))
	{
		AmmoTypeName = AmmoTypeTag.GetTagName();
	}
}

#endif // WITH_EDITOR
