// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Firegun/PMTFWActiveFiregunsProxy.h"

// PMTFW Includes
#include "Attributes/PMTFWBaseAttributeSet.h"
#include "Firegun/Attachments/PMTFWSimpleAttachment.h"
#include "GameplayEffects/PMTFWFiregunEquipGE.h"
#include "Interfaces/PMTFWFiregunProvider.h"

// PMTGU Includes
#include "ActorComponents/PMTGUAbilityRestorerComp.h"
#include "ActorComponents/PMTGUChainedAbilityRemovalComp.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "GameFramework/PlayerController.h"

APMTFWActiveFiregunsProxy::APMTFWActiveFiregunsProxy()
{
	PrimaryActorTick.bCanEverTick = false;
	AbilitySystemComponentClass = UAbilitySystemComponent::StaticClass();
	FiregunAttributeSetClass = UPMTFWBaseAttributeSet::StaticClass();

	SetReplicates(true);
	bAlwaysRelevant = true;

	NetDormancy = DORM_DormantAll;
}

TArray<APMTFWActiveFiregunsProxy*> APMTFWActiveFiregunsProxy::CreateFiregunProxiesForPlayer(
	AController* OwnerController, TSubclassOf<APMTFWActiveFiregunsProxy> ProxyClass, int32 ActiveCount)
{
	check(OwnerController);
	check(ActiveCount > 0);
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = OwnerController;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	TArray<APMTFWActiveFiregunsProxy*> Proxies;
	Proxies.Reserve(ActiveCount);
	for (int32 i = 0; i < ActiveCount; ++i)
	{
		APMTFWActiveFiregunsProxy* Proxy = OwnerController->GetWorld()->SpawnActor<APMTFWActiveFiregunsProxy>(ProxyClass, SpawnParameters);
		check(Proxy);
		Proxy->InitializeActiveInstance();
		Proxies.Add(Proxy);
	}
	
	return Proxies;
}

FActiveGameplayEffectHandle APMTFWActiveFiregunsProxy::ApplyGameplayEffect(const FGameplayEffectSpec& InGameplayEffectSpec, FPMTFWFiregunInstanceHandle InFiregunHandle)
{
	if (InFiregunHandle == InstanceHandle)
	{
		FActiveGameplayEffectHandle ActiveHandle =
			InstanceASC->ApplyGameplayEffectSpecToSelf(InGameplayEffectSpec);
		if (ActiveHandle.IsValid())
		{
			ActiveGameplayEffects.Add(ActiveHandle);
		}
		return ActiveHandle;
	}
	return FActiveGameplayEffectHandle();
}

bool APMTFWActiveFiregunsProxy::RemoveGameplayEffect(FActiveGameplayEffectHandle GameplayEffectHandle, FPMTFWFiregunInstanceHandle InFiregunHandle)
{
	if (InFiregunHandle == InstanceHandle)
	{
		for (int32 Index = 0; Index < ActiveGameplayEffects.Num(); ++Index)
		{
			if (ActiveGameplayEffects[Index] == GameplayEffectHandle)
			{
				ActiveGameplayEffects.RemoveAtSwap(Index);
			}
		}
		return InstanceASC->RemoveActiveGameplayEffect(GameplayEffectHandle, 1);
	}
	return false;
}

void APMTFWActiveFiregunsProxy::ChangeOrUpdateFiregunInstance(UAbilitySystemComponent* OwnerASC,
	const FPMTFWFiregunInstanceDataItem& InFiregunInstance, IPMTFWFiregunTrackerInterface* FiregunTracker, FGameplayEffectContextHandle InContextHandle)
{
	check(FiregunTracker);
	FiregunTracker->GetFiregunInstanceRemovedDelegate().RemoveAll(this);
	InstanceRemovedDelegateHandle = FiregunTracker->GetFiregunInstanceRemovedDelegate().AddUObject(this, &APMTFWActiveFiregunsProxy::OnInstanceRemoved);
	// TODO(Brian): Investigate if we could verify which gameplay effects should be applied
	// if we are just updating an existing handle.
	for (FActiveGameplayEffectHandle& SpecHandle : ActiveGameplayEffects)
	{
		InstanceASC->RemoveActiveGameplayEffect(SpecHandle);
	}
	ActiveGameplayEffects.Reset();
	for (UAttributeSet* ASCAttributeSet : InstanceASC->SpawnedAttributes)
	{
		// This is required in case instant gameplayeffects that changed attributes were applied.
		UPMTFWBaseAttributeSet* BaseAttributeSet = Cast<UPMTFWBaseAttributeSet>(ASCAttributeSet);
		if (BaseAttributeSet)
		{
			BaseAttributeSet->ResetAttributes();
		}
	}

	const bool bUpdatingSameInstance = InstanceHandle == InFiregunInstance.Handle;
	InstanceHandle = InFiregunInstance.Handle;

	const UPMTFWFiregunData* const FiregunCDO = InFiregunInstance.FiregunDataClass.GetDefaultObject();
	// ensureAlwaysMsgf(FiregunCDO->GetAttributesGameplayEffect().IsValid(), TEXT("'%s': You should try to load the firegun class '%s' to avoid a synchronous load for better performance."), ANSI_TO_TCHAR(__FUNCTION__), *FiregunCDO->GetName());

	InContextHandle = InstanceASC->MakeEffectContext();
	InContextHandle.AddSourceObject(this);

	if (!bUpdatingSameInstance)
	{
		const UGameplayEffect* const BaseAttributesGameplayEffect = FiregunCDO->GetAttributesGameplayEffect().LoadSynchronous()->GetDefaultObject<UGameplayEffect>();
		ApplyGameplayEffect(FGameplayEffectSpec(BaseAttributesGameplayEffect, InContextHandle), InstanceHandle);
	}

	for (const FPMTFWFiregunAttachmentDataItem& Attachment : InFiregunInstance.FiregunAttachments.Attachments)
	{
		const UPMTFWSimpleAttachment* const AttachmentCDO = Attachment.AttachmentClass.GetDefaultObject();
		// ensureAlwaysMsgf(AttachmentCDO->GetAttributesGameplayEffect().IsValid(), TEXT("'%s': You should try to load the attachment class '%s' to avoid a synchronous load for better performance."), ANSI_TO_TCHAR(__FUNCTION__), *AttachmentCDO->GetName());
		const UGameplayEffect* const AttachmentAttributesGameplayEffect = AttachmentCDO->GetAttributesGameplayEffect().LoadSynchronous()->GetDefaultObject<UGameplayEffect>();
		ApplyGameplayEffect(FGameplayEffectSpec(AttachmentAttributesGameplayEffect, InContextHandle), InstanceHandle);
	}

	TArray<TSubclassOf<UGameplayEffect>> GatheredGEs = GatherFiregunGameplayEffects(OwnerASC, InFiregunInstance, InContextHandle);
	for (TSubclassOf<UGameplayEffect> SingleGE : GatheredGEs)
	{
		ApplyGameplayEffect(FGameplayEffectSpec(SingleGE.GetDefaultObject(), InContextHandle), InstanceHandle);
	}

	InstanceChangedDelegate.Broadcast(InFiregunInstance);
}

TArray<FActiveGameplayEffectHandle> APMTFWActiveFiregunsProxy::EquipCurrentFiregunInstance(UAbilitySystemComponent* OwnerASC,
	UPMTGUAbilityRestorerComp* OwnerAbilityRestorer,
	UPMTGUChainedAbilityRemovalComp* OwnerChainAbilityRemovalComp,
	TScriptInterface<IPMTFWFiregunTrackerInterface> FiregunTracker,
	FPredictionKey PredictionKey,
	FGameplayEffectContextHandle InContextHandle,
	bool bStoreAbilities)
{
	TArray<FActiveGameplayEffectHandle> EffectsAppliedToOwner;
	if (!InstanceHandle.IsValid())
	{
		return EffectsAppliedToOwner;
	}
	check(OwnerAbilityRestorer);
	const FPMTFWFiregunInstanceDataItem& Instance = FiregunTracker->GetInstanceDataItem(InstanceHandle);
	if (Instance.Handle != InstanceHandle)
	{
		// TODO(Brian): Change log category.
		UE_LOG(LogTemp, Error, TEXT("'%s': Can't equip firegun since the handle that this proxy has is different than the given one."), ANSI_TO_TCHAR(__FUNCTION__));
		return EffectsAppliedToOwner;
	}

	FGameplayAbilitySpec PrimaryAbilitySpec;
	FGameplayAbilitySpec SecondaryAbilitySpec;
	FGameplayAbilitySpec SpecialAbilitySpec;

	TArray<FPMTGUChainAbility> ChainedAbilitySpecs;

	// Look for attachments that could possible override an existing firegun's ability.
	for (const FPMTFWFiregunAttachmentDataItem& Attachment : Instance.FiregunAttachments.Attachments)
	{
		const UPMTFWSimpleAttachment* const AttachmentCDO = Attachment.AttachmentClass.GetDefaultObject();
		if (AttachmentCDO->GetAbilitySet().ToSoftObjectPath().IsValid())
		{
			//ensureAlwaysMsgf(AttachmentCDO->GetAbilitySet().IsValid(), TEXT("'%s': You should load this ability set before trying to equip the firegun. Will use a sync load now."), ANSI_TO_TCHAR(__FUNCTION__));
			const UPMTFWAbilitySetDataAsset* AbilitySet = AttachmentCDO->GetAbilitySet().LoadSynchronous();
			if (AbilitySet->PrimaryAbilitySpec)
			{
				ensureAlwaysMsgf(!PrimaryAbilitySpec.Ability, TEXT("There was an attachment that already overrides the primary ability. Ignoring."));
				if (!PrimaryAbilitySpec.Ability)
				{
					UPMTFWFireAbilitySpecDataAsset* PrimaryAbilityFromSet = AbilitySet->PrimaryAbilitySpec;
					PrimaryAbilitySpec = PrimaryAbilityFromSet->CreateAbilitySpec();
					ChainedAbilitySpecs.Add(FPMTGUChainAbility(PrimaryAbilitySpec.Handle, PrimaryAbilityFromSet->CreateChainedAbilitySpecs()));
				}
			}

			if (AbilitySet->SecondaryAbilitySpec)
			{
				ensureAlwaysMsgf(!SecondaryAbilitySpec.Ability, TEXT("There was an attachment that already overrides the secondary ability. Ignoring."));
				if (!SecondaryAbilitySpec.Ability)
				{
					UPMTFWFireAbilitySpecDataAsset* SecondaryAbilityFromSet = AbilitySet->SecondaryAbilitySpec;
					SecondaryAbilitySpec = SecondaryAbilityFromSet->CreateAbilitySpec();
					ChainedAbilitySpecs.Add(FPMTGUChainAbility(SecondaryAbilitySpec.Handle, SecondaryAbilityFromSet->CreateChainedAbilitySpecs()));
				}
			}

			if (AbilitySet->SpecialAbilitySpec)
			{
				ensureAlwaysMsgf(!SpecialAbilitySpec.Ability, TEXT("There was an attachment that already overrides the special ability. Ignoring."));
				if (!SpecialAbilitySpec.Ability)
				{
					UPMTFWFireAbilitySpecDataAsset* SpecialAbilityFromSet = AbilitySet->SpecialAbilitySpec;
					SpecialAbilitySpec = SpecialAbilityFromSet->CreateAbilitySpec();
					ChainedAbilitySpecs.Add(FPMTGUChainAbility(SpecialAbilitySpec.Handle, SpecialAbilityFromSet->CreateChainedAbilitySpecs()));
				}
			}
		}

		// Apply any effect of the attachments to the player who is using this firegun.
		for (TSoftClassPtr<UPMTFWFiregunEquipGE> SoftGE : AttachmentCDO->GetAdditionalGEs())
		{
			check(!SoftGE.IsNull());
			//ensureAlwaysMsgf(SoftGE.IsValid(), TEXT("'%s': You should load this gameplay effect before trying to equip the firegun. Will use a sync load now."), ANSI_TO_TCHAR(__FUNCTION__));
			const UPMTFWFiregunEquipGE* GameplayEffectCDO = SoftGE.LoadSynchronous()->GetDefaultObject<UPMTFWFiregunEquipGE>();
			FGameplayEffectSpec GameplayEffectSpec(GameplayEffectCDO, InContextHandle);
			if (GameplayEffectCDO->ShouldApplyToOwner())
			{
				FActiveGameplayEffectHandle EffectHandle = OwnerASC->ApplyGameplayEffectSpecToSelf(GameplayEffectSpec, PredictionKey);
				if (EffectHandle.IsValid())
				{
					EffectsAppliedToOwner.Add(EffectHandle);
				}
			}
			else
			{
				FActiveGameplayEffectHandle EffectHandle = InstanceASC->ApplyGameplayEffectSpecToSelf(GameplayEffectSpec, PredictionKey);
				if (EffectHandle.IsValid())
				{
					ActiveEquippedGameplayEffects.Add(EffectHandle);
				}
			}
		}
	}

	const UPMTFWFiregunData* const FiregunCDO = Instance.FiregunDataClass.GetDefaultObject();
	//ensureAlwaysMsgf(FiregunCDO->GetAbilitySet().IsValid(), TEXT("'%s': You should load this ability set before trying to equip the firegun. Will use a sync load now."), ANSI_TO_TCHAR(__FUNCTION__));
	UPMTFWAbilitySetDataAsset* FiregunAbilitySpec = FiregunCDO->GetAbilitySet().LoadSynchronous();

	TArray<FGameplayAbilitySpec> AbilitiesToGive;
	if (!PrimaryAbilitySpec.Ability)
	{
		check(FiregunAbilitySpec->PrimaryAbilitySpec);
		PrimaryAbilitySpec = FiregunAbilitySpec->PrimaryAbilitySpec->CreateAbilitySpec();
		AbilitiesToGive.Add(PrimaryAbilitySpec);
		if (FiregunAbilitySpec->PrimaryAbilitySpec->HasChainedAbilities())
		{
			ChainedAbilitySpecs.Add(FPMTGUChainAbility(PrimaryAbilitySpec.Handle, FiregunAbilitySpec->PrimaryAbilitySpec->CreateChainedAbilitySpecs()));
		}
	}
	if (!SecondaryAbilitySpec.Ability && FiregunAbilitySpec->SecondaryAbilitySpec)
	{
		SecondaryAbilitySpec = FiregunAbilitySpec->SecondaryAbilitySpec->CreateAbilitySpec();
		AbilitiesToGive.Add(SecondaryAbilitySpec);
		if (FiregunAbilitySpec->SecondaryAbilitySpec->HasChainedAbilities())
		{
			ChainedAbilitySpecs.Add(FPMTGUChainAbility(SecondaryAbilitySpec.Handle, FiregunAbilitySpec->SecondaryAbilitySpec->CreateChainedAbilitySpecs()));
		}
	}
	if (!SpecialAbilitySpec.Ability && FiregunAbilitySpec->SpecialAbilitySpec)
	{
		SpecialAbilitySpec = FiregunAbilitySpec->SpecialAbilitySpec->CreateAbilitySpec();
		AbilitiesToGive.Add(SpecialAbilitySpec);
		ChainedAbilitySpecs.Add(FPMTGUChainAbility(SpecialAbilitySpec.Handle, FiregunAbilitySpec->SpecialAbilitySpec->CreateChainedAbilitySpecs()));
	}
	if (FiregunAbilitySpec->ReloadAbilitySpec)
	{
		AbilitiesToGive.Add(FiregunAbilitySpec->ReloadAbilitySpec->CreateAbilitySpec());
	}
	OwnerAbilityRestorer->ReplaceAbilities(AbilitiesToGive, bStoreAbilities);
	if (OwnerChainAbilityRemovalComp && ChainedAbilitySpecs.Num() > 0)
	{
		OwnerChainAbilityRemovalComp->AddChainedAbilities(OwnerASC, ChainedAbilitySpecs);
	}
	return EffectsAppliedToOwner;
}

void APMTFWActiveFiregunsProxy::UnEquipCurrentFiregunInstance(UAbilitySystemComponent* OwnerASC,
	UPMTGUAbilityRestorerComp* OwnerAbilityRestorer,
	const TArray<FActiveGameplayEffectHandle>& EffectsAppliedToOwner)
{
	check(OwnerASC && OwnerAbilityRestorer);
	for (FActiveGameplayEffectHandle& EffectHandle : ActiveEquippedGameplayEffects)
	{
		InstanceASC->RemoveActiveGameplayEffect(EffectHandle);
	}
	ActiveEquippedGameplayEffects.Reset();

	for (const FActiveGameplayEffectHandle& EffectHandle : EffectsAppliedToOwner)
	{
		OwnerASC->RemoveActiveGameplayEffect(EffectHandle);
	}
	OwnerAbilityRestorer->RestoreAbilities();
}

bool APMTFWActiveFiregunsProxy::DoesProxyContainFiregunInstance() const
{
	return InstanceHandle.IsValid();
}

bool APMTFWActiveFiregunsProxy::DoesProxyContainFiregunInstanceHandle(FPMTFWFiregunInstanceHandle FiregunHandle) const
{
	return FiregunHandle.IsValid() && InstanceHandle == FiregunHandle;
}

float APMTFWActiveFiregunsProxy::GetFiregunDamage() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetDamage();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunReloadSpeed() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetReloadSpeed();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunFireRate() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetFireRate();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunBulletSpeed() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetBulletSpeed();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunMaxRange() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetBulletRange();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunCriticalRate() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetCriticalRate();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunCriticalDamage() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetCriticalDamage();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunWeight() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetWeight();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunSoundNoise() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetSoundNoise();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunMagazineSize() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetMagazineSize();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunBulletsPerShot() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetBulletsPerShot();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunBaseSpread() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetSpread();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunMaxSpread() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetMaxSpread();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunSpreadIncrementPerPellet() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetSpreadIncrementPerPellet();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunPelletsPerShot() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetPelletsPerBullet();
	}
	return 0.0f;
}

float APMTFWActiveFiregunsProxy::GetFiregunPenetration() const
{
	if (InstanceAttributeSet)
	{
		return InstanceAttributeSet->GetPenetration();
	}
	return 0.0f;
}

UAbilitySystemComponent* APMTFWActiveFiregunsProxy::GetAbilitySystemComponent() const
{
	return InstanceASC;
}

TArray<TSubclassOf<UGameplayEffect>> APMTFWActiveFiregunsProxy::GatherFiregunGameplayEffects(
	UAbilitySystemComponent* OwnerASC, const FPMTFWFiregunInstanceDataItem& InstanceToUse, FGameplayEffectContextHandle InContextHandle) const
{
	return TArray<TSubclassOf<UGameplayEffect>>();
}

void APMTFWActiveFiregunsProxy::OnInstanceRemoved(const FPMTFWFiregunInstanceDataItem& InstanceRemoved)
{
	if (InstanceRemoved.Handle.IsValid() && InstanceRemoved.Handle == InstanceHandle)
	{
		InstanceRemovedDelegate.Broadcast(InstanceRemoved);
		for (FActiveGameplayEffectHandle& SpecHandle : ActiveGameplayEffects)
		{
			InstanceASC->RemoveActiveGameplayEffect(SpecHandle);
		}
		ActiveGameplayEffects.Reset();
		for (UAttributeSet* ASCAttributeSet : InstanceASC->SpawnedAttributes)
		{
			// This is required in case instant gameplayeffects that changed attributes were applied.
			UPMTFWBaseAttributeSet* BaseAttributeSet = Cast<UPMTFWBaseAttributeSet>(ASCAttributeSet);
			if (BaseAttributeSet)
			{
				BaseAttributeSet->ResetAttributes();
			}
		}
		InstanceHandle.Reset();
	}
}

void APMTFWActiveFiregunsProxy::InitializeActiveInstance()
{
	InstanceASC = NewObject<UAbilitySystemComponent>(this, AbilitySystemComponentClass);

	// We don't want replication because we can't deal with an owner having a bunch of
	// ASCs.
	InstanceASC->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);
	InstanceASC->SetIsReplicated(false);

	InstanceASC->RegisterComponent();

	InstanceAttributeSet = NewObject<UPMTFWBaseAttributeSet>(this, FiregunAttributeSetClass);
	InstanceASC->AddAttributeSetSubobject(InstanceAttributeSet);

	InstanceASC->InitAbilityActorInfo(this, this);
}
