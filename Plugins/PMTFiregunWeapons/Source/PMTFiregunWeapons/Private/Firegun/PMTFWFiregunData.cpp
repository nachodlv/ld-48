// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Firegun/PMTFWFiregunData.h"

// PMTFW Includes
#include "Globals/PMTFWGlobals.h"
#include "Firegun/PMTFWFiregun.h"

uint16 FPMTFWFiregunInstanceDataItem::HandleBulletFired(const FGameplayTag& InAmmoTag, uint16 BulletsShot)
{
	for (FPMTFWCurrentAmmoForBulletType& CurrentAmmo : CurrentBulletsInMagazine)
	{
		if (CurrentAmmo.AmmoType == InAmmoTag)
		{
			CurrentAmmo.BulletsInMagazine -= BulletsShot;
			check(CurrentAmmo.BulletsInMagazine >= 0);
			return CurrentAmmo.BulletsInMagazine;
		}
	}
	// This means we fired a bullet type we currently don't have.
	check(false);
	return 0;
}

bool FPMTFWFiregunInstanceDataItem::HasBulletsInMagazine(const FGameplayTag& InAmmoTag) const
{
	for (const FPMTFWCurrentAmmoForBulletType& CurrentAmmo : CurrentBulletsInMagazine)
	{
		if (CurrentAmmo.AmmoType == InAmmoTag)
		{
			return CurrentAmmo.BulletsInMagazine > 0;
		}
	}
	return false;
}

void FPMTFWFiregunInstanceDataItem::ModifyBulletCount(const FGameplayTag& InAmmoTag, uint16 NewValue)
{
	for (FPMTFWCurrentAmmoForBulletType& CurrentAmmo : CurrentBulletsInMagazine)
	{
		if (CurrentAmmo.AmmoType == InAmmoTag)
		{
			CurrentAmmo.BulletsInMagazine = NewValue;
			break;
		}
	}
}

uint16 FPMTFWFiregunInstanceDataItem::GetCurrentBulletsForAmmoType(const FGameplayTag& InAmmoTag) const
{
	for (const FPMTFWCurrentAmmoForBulletType& CurrentAmmo : CurrentBulletsInMagazine)
	{
		if (CurrentAmmo.AmmoType == InAmmoTag)
		{
			return CurrentAmmo.BulletsInMagazine;
		}
	}
	return 0;
}

void FPMTFWFiregunInstanceDataItem::PreReplicatedRemove(const FPMTFWFiregunInstanceArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunInstanceRemovedDelegate().Broadcast(*this);
	}
}

void FPMTFWFiregunInstanceDataItem::PostReplicatedAdd(const FPMTFWFiregunInstanceArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunInstanceAddedDelegate().Broadcast(*this);
	}
}

void FPMTFWFiregunInstanceDataItem::PostReplicatedChange(const FPMTFWFiregunInstanceArray& InArraySerializer)
{
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->GetFiregunInstanceChangedDelegate().Broadcast(*this);
	}
}

void UPMTFWFiregunData::ApplyDataToFiregun(APMTFWFiregun* FiregunToApply)
{
	check(FiregunToApply);

	FiregunToApply->SetFiregunSkeletalMesh(FiregunSkeletalMesh);
	FiregunToApply->SetFiregunStaticMesh(FiregunStaticMesh);
}

const FText& UPMTFWFiregunData::GetFiregunName() const
{
	return FiregunName;
}

FTransform UPMTFWFiregunData::GetShotInitialTransform(const APMTFWFiregun* Firegun) const
{
	check(Firegun);
	if (Firegun->GetFiregunSkeletalMeshComponent())
	{
		return Firegun->RetrieveBulletTransform(FireSocketName);
	}
	else if (Firegun->GetFiregunStaticMeshComponent())
	{
		FTransform FiregunTransform = Firegun->GetActorTransform();
		const FVector Offset = FireOffset * FiregunTransform.GetScale3D();
		FiregunTransform.AddToTranslation(Offset);
		return FiregunTransform;
	}
	ensureMsgf(false, TEXT("Firegun '%s' does not have a skeletal mesh or a static mesh."), *Firegun->GetName());
	return FTransform();
}

TSoftClassPtr<UGameplayEffect> UPMTFWFiregunData::GetAttributesGameplayEffect() const
{
	return AttributesGameplayEffect;
}

TSoftObjectPtr<UPMTFWAbilitySetDataAsset> UPMTFWFiregunData::GetAbilitySet() const
{
	return FiregunAbilitySet;
}

USkeletalMesh* UPMTFWFiregunData::GetSkeletalMesh() const
{
	return FiregunSkeletalMesh;
}

FGameplayTag UPMTFWFiregunData::GetFiregunAmmoType() const
{
	return AmmoType;
}
