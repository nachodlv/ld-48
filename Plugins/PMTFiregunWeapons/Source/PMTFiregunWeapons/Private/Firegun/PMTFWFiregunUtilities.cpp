// Fill out your copyright notice in the Description page of Project Settings.


#include "Firegun/PMTFWFiregunUtilities.h"

FGameplayAbilitySpec UPMTFWFireAbilitySpecDataAsset::CreateAbilitySpec() const
{
	return FGameplayAbilitySpec(Ability, LevelScalableFloat.GetValue(), InputID);
}

TArray<FGameplayAbilitySpec> UPMTFWFireAbilitySpecDataAsset::CreateChainedAbilitySpecs() const
{
	TArray<FGameplayAbilitySpec> Specs;
	Specs.Reserve(ChainedAbilities.Num());
	for (TSubclassOf<UGameplayAbility> SingleAbility : ChainedAbilities)
	{
		Specs.Emplace(SingleAbility, LevelScalableFloat.GetValue(), INDEX_NONE);
	}
	return Specs;
}
