// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionLibraries/PMTFWFiregunFunctionLibrary.h"

// PMTFW Includes
#include "Firegun/PMTFWActiveFiregunsProxy.h"
#include "Firegun/PMTFWFiregun.h"
#include "Firegun/PMTFWFiregunData.h"
#include "Firegun/PMTFWFiregunUtilities.h"
#include "Globals/PMTFWGlobals.h"

// UE Includes
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"

namespace
{

	bool ClipCameraRayToAbilityRange(const FVector& CameraLocation, const FVector& CameraDirection,
		const FPMTFWFiregunTraceParameters& TraceParameters, FVector& ClippedPosition)
	{
		FVector CameraToCenter = TraceParameters.StartTraceLocation - CameraLocation;
		float DotToCenter = FVector::DotProduct(CameraToCenter, CameraDirection);
		if (DotToCenter >= 0.0f)	//If this fails, we're pointed away from the center, but we might be inside the sphere and able to find a good exit point.
		{
			float DistanceSquared = CameraToCenter.SizeSquared() - (DotToCenter * DotToCenter);
			float RadiusSquared = (TraceParameters.MaxRange * TraceParameters.MaxRange);
			if (DistanceSquared <= RadiusSquared)
			{
				float DistanceFromCamera = FMath::Sqrt(RadiusSquared - DistanceSquared);
				float DistanceAlongRay = DotToCenter + DistanceFromCamera;						//Subtracting instead of adding will get the other intersection point
				ClippedPosition = CameraLocation + (DistanceAlongRay * CameraDirection);		//Cam aim point clipped to range sphere
				return true;
			}
		}
		return false;
	}

	TArray<FHitResult> MakeLineTrace(const UWorld* World, const FVector& Start, const FVector& End, const FCollisionQueryParams& Params, const FPMTFWFiregunTraceParameters& TraceParameters)
	{
		check(World);
		TArray<FHitResult> HitResults;
		World->LineTraceMultiByProfile(HitResults, Start, End, TraceParameters.TraceProfile.Name, Params);
		return HitResults;
	}

	FVector AimWithPlayerController(const AActor* InSourceActor, const FVector& ViewStart, const FRotator& ViewRot,
		const FCollisionQueryParams& QueryParams, const FPMTFWFiregunTraceParameters& TraceParameters, FRandomStream& InRandomStream, float InSpread)
	{
		const FVector& TraceStart = TraceParameters.StartTraceLocation;
		const FVector ViewDir = ViewRot.Vector();
		FVector ViewEnd = ViewStart + (ViewDir * TraceParameters.MaxRange);
		ClipCameraRayToAbilityRange(ViewStart, ViewDir, TraceParameters, ViewEnd /* Out parameter */);
		TArray<FHitResult> HitResults = MakeLineTrace(InSourceActor->GetWorld(), ViewStart, ViewEnd, QueryParams, TraceParameters);

		const bool bUseTraceResult = HitResults.Num() > 0 && (FVector::DistSquared(TraceStart, HitResults[0].Location) <= (TraceParameters.MaxRange * TraceParameters.MaxRange));
		const FVector AdjustedEnd = (bUseTraceResult) ? HitResults[0].Location : ViewEnd;
		FVector AdjustedAimDir = (AdjustedEnd - TraceStart).GetSafeNormal();
		if (AdjustedAimDir.IsZero())
		{
			AdjustedAimDir = ViewDir;
		}

		if (bUseTraceResult)
		{
			FVector OriginalAimDir = (ViewEnd - TraceParameters.StartTraceLocation).GetSafeNormal();
			if (!OriginalAimDir.IsZero())
			{
				// Convert to angles and use original pitch
				const FRotator OriginalAimRot = OriginalAimDir.Rotation();

				FRotator AdjustedAimRot = AdjustedAimDir.Rotation();
				AdjustedAimRot.Pitch = OriginalAimRot.Pitch;
				AdjustedAimDir = AdjustedAimRot.Vector();
			}
		}

		const float ConeHalfAngle = FMath::DegreesToRadians(InSpread * 0.5f);
		const FVector ShootDir = InRandomStream.VRandCone(AdjustedAimDir, ConeHalfAngle, ConeHalfAngle);
		return TraceStart + (ShootDir * TraceParameters.MaxRange);
	}

	FVector AimWithPlayerController(const AActor* InSourceActor, const APlayerController* ActorController,
		const FCollisionQueryParams& QueryParams, const FPMTFWFiregunTraceParameters& TraceParameters, FRandomStream& InRandomStream, float InSpread)
	{
		const FVector& TraceStart = TraceParameters.StartTraceLocation;
		// These values will be used for the AI.
		FVector ViewStart = TraceStart;
		FRotator ViewRot = TraceParameters.StartTraceRotation;

		if (ActorController)
		{
			ActorController->GetPlayerViewPoint(ViewStart, ViewRot);
		}

		return AimWithPlayerController(InSourceActor, ViewStart, ViewRot, QueryParams, TraceParameters, InRandomStream, InSpread);
	}
}

FPMTFWFiregunTraceResults UPMTFWFiregunFunctionLibrary::DoTrace(AActor* ActorRequestingTrace,
	APlayerController* ActorController, const FPMTFWFiregunTraceParameters& TraceParameters)
{
	TArray<FHitResult> Hits;

	FCollisionQueryParams Params(SCENE_QUERY_STAT(UPMTFWFiregunFunctionLibrary_DoTrace), TraceParameters.bComplexTrace);
	Params.bReturnPhysicalMaterial = true;
	Params.AddIgnoredActors(TraceParameters.ActorsToIgnore);
	Params.bIgnoreBlocks = TraceParameters.bIgnoreBlockingHits;
	float CurrentSpread = TraceParameters.BaseSpread;
	const int32 InitialSeed = FMath::Rand();
	FRandomStream RandomStream(InitialSeed);
	for (int32 CurrentPellet = 0; CurrentPellet < TraceParameters.PelletsAmount; ++CurrentPellet)
	{
		const int32 OldHitCount = Hits.Num();

		const FVector TraceEnd = AimWithPlayerController(ActorRequestingTrace, ActorController, Params, TraceParameters, RandomStream, CurrentSpread);
		Hits.Append(MakeLineTrace(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, TraceEnd, Params, TraceParameters));

		const int32 ActualHitCount = Hits.Num();
		// We didn't hit anything, therefore we are going to add an empty hit result.
		// We need at least an "empty" hit result so we can properly show any particle effect.
		if (OldHitCount == ActualHitCount)
		{
			FHitResult HitResult;
			HitResult.TraceStart = TraceParameters.StartTraceLocation;
			HitResult.TraceEnd = TraceEnd;
			HitResult.Location = TraceEnd;
			HitResult.ImpactPoint = TraceEnd;
			HitResult.bBlockingHit = false;
			Hits.Emplace(HitResult);
		}
		CurrentSpread = FMath::Clamp(CurrentSpread + TraceParameters.SpreadIncrementPerPellet, TraceParameters.BaseSpread, TraceParameters.MaxSpread);
	}
#if ENABLE_DRAW_DEBUG
	if (TraceParameters.bDebug)
	{
		UE_VLOG_LOCATION(ActorRequestingTrace, PMTFWFiregunTraceLog, Verbose, TraceParameters.StartTraceLocation, 10.0f, FColor::Green, TEXT("Trace initial location requested from '%s'"), *ActorRequestingTrace->GetName());
		// Draw an sphere to show the initial shot location.
		DrawDebugSphere(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, 10.0f, 12, FColor::Green, false, 2.0f, 0, 1.0f);
		for (const FHitResult& SingleHit : Hits)
		{
			DrawDebugLine(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, SingleHit.TraceEnd, FColor::Red, false, 2.0f, 0, 1.0f);
		}
	}
#endif
	FPMTFWFiregunTraceResults Results;
	Results.Hits = MoveTemp(Hits);
	Results.SeedUsed = InitialSeed;
	return Results;
}

FPMTFWFiregunTraceResults UPMTFWFiregunFunctionLibrary::SimulateTrace(AActor* ActorRequestingTrace,
	const FPMTFWBulletHitTargetData& TargetData, const FPMTFWFiregunTraceParameters& TraceParameters)
{
	TArray<FHitResult> Hits;

	FCollisionQueryParams Params(SCENE_QUERY_STAT(UPMTFWFiregunFunctionLibrary_DoTrace), TraceParameters.bComplexTrace);
	Params.bReturnPhysicalMaterial = true;
	Params.AddIgnoredActors(TraceParameters.ActorsToIgnore);
	Params.bIgnoreBlocks = TraceParameters.bIgnoreBlockingHits;
	float CurrentSpread = TraceParameters.BaseSpread;
	const int32 InitialSeed = TargetData.Seed;
	FRandomStream RandomStream(InitialSeed);
	for (int32 CurrentPellet = 0; CurrentPellet < TraceParameters.PelletsAmount; ++CurrentPellet)
	{
		const int32 OldHitCount = Hits.Num();

		const FVector TraceEnd = AimWithPlayerController(ActorRequestingTrace, TargetData.ViewStart, TargetData.ViewRotation, Params, TraceParameters, RandomStream, CurrentSpread);
		Hits.Append(MakeLineTrace(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, TraceEnd, Params, TraceParameters));

		const int32 ActualHitCount = Hits.Num();
		// We didn't hit anything, therefore we are going to add an empty hit result.
		// We need at least an "empty" hit result so we can properly show any particle effect.
		if (OldHitCount == ActualHitCount)
		{
			FHitResult HitResult;
			HitResult.TraceStart = TraceParameters.StartTraceLocation;
			HitResult.TraceEnd = TraceEnd;
			HitResult.Location = TraceEnd;
			HitResult.ImpactPoint = TraceEnd;
			HitResult.bBlockingHit = false;
			Hits.Emplace(HitResult);
		}
		CurrentSpread = FMath::Clamp(CurrentSpread + TraceParameters.SpreadIncrementPerPellet, TraceParameters.BaseSpread, TraceParameters.MaxSpread);
	}
#if ENABLE_DRAW_DEBUG
	if (TraceParameters.bDebug)
	{
		UE_VLOG_LOCATION(ActorRequestingTrace, PMTFWFiregunTraceLog, Verbose, TraceParameters.StartTraceLocation, 25.0f, FColor::Green, TEXT("Trace initial location requested from '%s'"), *ActorRequestingTrace->GetName());
		// Draw an sphere to show the initial shot location.
		DrawDebugSphere(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, 25.0f, 12, FColor::Green, false, 2.0f, 0, 1.0f);
		for (const FHitResult& SingleHit : Hits)
		{
			DrawDebugLine(ActorRequestingTrace->GetWorld(), TraceParameters.StartTraceLocation, SingleHit.TraceEnd, FColor::Green, false, 2.0f, 0, 1.0f);
		}
	}
#endif
	FPMTFWFiregunTraceResults Results;
	Results.Hits = MoveTemp(Hits);
	Results.SeedUsed = InitialSeed;
	return Results;
}

FPMTFWFiregunTraceParameters UPMTFWFiregunFunctionLibrary::GetTraceParameters(const FPMTFWFiregunInstanceDataItem& FiregunInstance,
	const APMTFWActiveFiregunsProxy* ActiveProxy, const APMTFWFiregun* Firegun)
{
	check(ActiveProxy);
	const UPMTFWFiregunData* const FiregunDataCDO = FiregunInstance.FiregunDataClass.GetDefaultObject();
	const FTransform ShotTransform = FiregunDataCDO->GetShotInitialTransform(Firegun);

	FPMTFWFiregunTraceParameters TraceParameters;
	TraceParameters.StartTraceLocation = ShotTransform.GetLocation();
	TraceParameters.StartTraceRotation = ShotTransform.GetRotation().Rotator();
	TraceParameters.MaxRange = ActiveProxy->GetFiregunMaxRange();
	TraceParameters.BaseSpread = ActiveProxy->GetFiregunBaseSpread();
	TraceParameters.MaxSpread = ActiveProxy->GetFiregunMaxSpread();
	TraceParameters.SpreadIncrementPerPellet = ActiveProxy->GetFiregunSpreadIncrementPerPellet();
	TraceParameters.PelletsAmount = ActiveProxy->GetFiregunPelletsPerShot();
	TraceParameters.Penetration = ActiveProxy->GetFiregunPenetration();

	return TraceParameters;
}

FPMTFWBulletHitTargetData UPMTFWFiregunFunctionLibrary::GenerateHitTargetData(const FPMTFWFiregunTraceResults& InTraceResults)
{
	FPMTFWBulletHitTargetData TargetData;
	TargetData.ViewStart = InTraceResults.ViewStart;
	TargetData.ViewRotation = InTraceResults.ViewRot;
	TargetData.HitResults = InTraceResults.Hits;
	TargetData.Seed = InTraceResults.SeedUsed;
	TargetData.bNeedRegeneration = false;
	return TargetData;
}

FPMTFWCurrentAmmoForBulletType UPMTFWFiregunFunctionLibrary::CreateAmmoForFiregun(FGameplayTag InTag, int32 InCount)
{
	return FPMTFWCurrentAmmoForBulletType(InTag, static_cast<uint16>(InCount));
}

TArray<FHitResult> UPMTFWFiregunFunctionLibrary::GetHitResultsFromAbilityTargetData(const FGameplayAbilityTargetDataHandle& Data)
{
	const FPMTFWBulletHitTargetData CopyBulletTargetData = *(static_cast<const FPMTFWBulletHitTargetData*>(Data.Get(0)));
	return CopyBulletTargetData.HitResults;
}

