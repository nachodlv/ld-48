// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Globals/PMTFWGlobals.h"

// PMTFW Includes
#include "Firegun/Ammo/PMTFWAmmunition.h"
#include "Firegun/PMTFWFiregunData.h"

// UE Includes
#include "AssetRegistryModule.h"
#include "Engine/ObjectLibrary.h"
#include "GameplayEffect.h"
#include "GameplayTagsManager.h"
#include "IAssetRegistry.h"
#include "Modules/ModuleManager.h"
#include "UObject/SoftObjectPtr.h"

DEFINE_LOG_CATEGORY(PMTFWFiregunAbilityLog);
DEFINE_LOG_CATEGORY(PMTFWFiregunWeaponLog);
DEFINE_LOG_CATEGORY(PMTFWFiregunTraceLog);

namespace
{
	void LogAmmoTagIntegrity(const TArray<FAssetData>& AmmoAssetData)
	{
#if WITH_EDITOR
		// Verify that each ammo has a unique tag.
		TMap<FName, FString> AmmoTags;
		for (const FAssetData& AssetData : AmmoAssetData)
		{
			FName Tag;
			if (AssetData.GetTagValue<FName>(GET_MEMBER_NAME_CHECKED(UPMTFWAmmunition, AmmoTypeName), Tag))
			{
				if (!AmmoTags.Contains(Tag))
				{
					FString FullName;
					AssetData.GetFullName(FullName);
					AmmoTags.Add(Tag, FullName);
				}
				else
				{
					FString FullName;
					AssetData.GetFullName(FullName);
					auto ClassName = AmmoTags[Tag];
					ensureAlwaysMsgf(false, TEXT("'%s': Class '%s' shares the same tag as '%s' which it shouldn't. Fix that otherwise you'll have missing ammo types in your game."),
						ANSI_TO_TCHAR(__FUNCTION__), *ClassName, *FullName);
				}
			}
		}
#endif // WITH_EDITOR
	}
}

UPMTFWGlobals::UPMTFWGlobals(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PMTFWGlobalsClassName = FSoftClassPath(TEXT("/Script/PMTFiregunWeapons.PMTFWGlobals"));
}

void UPMTFWGlobals::InitFiregunWeaponGlobalData()
{
	UPMTFWGlobals::Get().InitGlobalData();
}

UPMTFWGlobals* UPMTFWGlobals::GetFiregunGlobals()
{
	return &UPMTFWGlobals::Get();
}

void UPMTFWGlobals::AddAmmoClass(FGameplayTag InTag, TSubclassOf<UPMTFWAmmunition> InAmmoClass)
{
	LoadedAmmunition.Add(InTag, InAmmoClass);
}

void UPMTFWGlobals::InitGlobalData()
{
	if (bInitialized)
	{
		UE_LOG(PMTFWFiregunWeaponLog, Error, TEXT("'%s': Global data already loaded. Please make sure to not do it more than once."), ANSI_TO_TCHAR(__FUNCTION__));
		return;
	}

#if WITH_EDITOR
	ReloadAmmoAssetData();
#else
	LoadAmmunitionClasses();
	bInitialized = true;
#endif // WITH_EDITOR

	UGameplayTagsManager& TagsManager = UGameplayTagsManager::Get();
	//TagsManager.AddNativeGameplayTag(FName(TEXT("LD48.Ammo")));
	//TagsManager.AddNativeGameplayTag(FName(TEXT("LD48.Ammo.AK47")));
	//TagsManager.AddNativeGameplayTag(FName(TEXT("LD48.Ammo.9mm")));
}

#if WITH_EDITOR

void UPMTFWGlobals::ReloadAmmoAssetData()
{
	AmmunitionObjectLibrary = nullptr;
	LoadAmmunitionClasses();
}

#endif // WITH_EDITOR

void UPMTFWGlobals::LoadFiregunAsync(const TArray<TSubclassOf<UPMTFWFiregunData>>& InFiregunClasses, FStreamableDelegate CompletionDelegate)
{
	UAssetManager& AssetManager = UAssetManager::Get();
	TArray<FSoftObjectPath> ObjectsToLoad;
	for (TSubclassOf<UPMTFWFiregunData> FiregunClass : InFiregunClasses)
	{
		const UPMTFWFiregunData* FiregunCDO = FiregunClass.GetDefaultObject();

		// TODO(Brian): Add remaining assets we care to load.
		ObjectsToLoad.Add(FiregunCDO->GetAttributesGameplayEffect().ToSoftObjectPath());
	}
	// Ignore handles, we don't want to store these references.
	if (!bLoadFiregunSync)
	{
		AssetManager.LoadAssetList(ObjectsToLoad, CompletionDelegate);
	}
	else
	{
		LoadAssetsSync(ObjectsToLoad, CompletionDelegate);
	}
}

void UPMTFWGlobals::LoadFiregunAsync(const TArray<TSoftClassPtr<UPMTFWFiregunData>>& InFiregunSoftClasses, FStreamableDelegate CompletionDelegate)
{
	TArray<FSoftObjectPath> SoftObjectPtrs;
	SoftObjectPtrs.Reserve(InFiregunSoftClasses.Num());
	for (TSoftClassPtr<UPMTFWFiregunData> SoftClassPtr : InFiregunSoftClasses)
	{
		SoftObjectPtrs.Add(SoftClassPtr.ToSoftObjectPath());
	}

	UAssetManager& AssetManager = UAssetManager::Get();
	// Ignore handles, we don't want to store these references.
	auto StremeableDel = FStreamableDelegate::CreateWeakLambda(this, [=]()
		{
			TArray<TSubclassOf<UPMTFWFiregunData>> Classes;
			for (TSoftClassPtr<UPMTFWFiregunData> SoftClass : InFiregunSoftClasses)
			{
				check(SoftClass.IsValid());
				Classes.Add(SoftClass.Get());
			}
			LoadFiregunAsync(Classes, CompletionDelegate);
		});
	if (!bLoadFiregunSync)
	{
		AssetManager.LoadAssetList(SoftObjectPtrs, StremeableDel);
	}
	else
	{
		LoadAssetsSync(SoftObjectPtrs, StremeableDel);
	}
}

void UPMTFWGlobals::LoadFiregunAmmunitions(const FGameplayTagContainer& AmmunitionTagContainer,
	FStreamableDelegate CompletionDelegate, PMTFWDataLoadBundle LoadBundles)
{
	TArray<FSoftObjectPath> ObjectsToLoad;
	for (const FGameplayTag& Tag : AmmunitionTagContainer)
	{
		for (FAssetData& AmmoAssetData : AmmunitionAssetData)
		{
			FName OutTag;
			AmmoAssetData.GetTagValue<FName>(GET_MEMBER_NAME_CHECKED(UPMTFWAmmunition, AmmoTypeName), OutTag);
			if (OutTag == Tag.GetTagName())
			{
				ObjectsToLoad.Add(AmmoAssetData.ToSoftObjectPath());
			}
		}
	}
	check(ObjectsToLoad.Num() > 0);
	FStreamableDelegate IntermediateDelegate = FStreamableDelegate::CreateWeakLambda(this, [=]()
		{
			TArray<FSoftObjectPath> OtherClassesToLoad;
			for (FSoftObjectPath ObjectLoaded : ObjectsToLoad)
			{
				UPMTFWAmmunition* AmmunitionCDO = CastChecked<UPMTFWAmmunition>(ObjectLoaded.ResolveObject());
				if (LoadBundles & PMTFWDataLoadBundle::PMTFWBundleGameplay)
				{
					OtherClassesToLoad.Add(AmmunitionCDO->GetHitGameplayEffect().ToSoftObjectPath());
					OtherClassesToLoad.Add(AmmunitionCDO->GetAttributesGameplayEffect().ToSoftObjectPath());
				}
				if (LoadBundles & PMTFWDataLoadBundle::PMTFWBundleUI)
				{
					OtherClassesToLoad.Add(AmmunitionCDO->GetIcon().ToSoftObjectPath());
				}
			}
			LoadAssetsAsync(OtherClassesToLoad, CompletionDelegate);
		}
	);
	LoadAssetsAsync(ObjectsToLoad, IntermediateDelegate);
}

UPMTFWAmmunition* UPMTFWGlobals::GetAmmunition(const FGameplayTag& InAmmoTag)
{
	return LoadedAmmunition[InAmmoTag].GetDefaultObject();
	/*if (AmmunitionAssetData.Num() == 0)
	{
		LoadAmmunitionClasses();
	}
	for (const FAssetData& AmmoAssetData : AmmunitionAssetData)
	{
		FName OutTag;
		AmmoAssetData.GetTagValue<FName>(GET_MEMBER_NAME_CHECKED(UPMTFWAmmunition, AmmoTypeName), OutTag);
		if (OutTag == InAmmoTag.GetTagName())
		{
			UObject* LoadedObject = AmmoAssetData.ToSoftObjectPath().TryLoad();
			UBlueprint* BlueprintObject = Cast<UBlueprint>(LoadedObject);
			if (BlueprintObject)
			{
				return Cast<UPMTFWAmmunition>(BlueprintObject->GeneratedClass->GetDefaultObject());
			}
			return Cast<UPMTFWAmmunition>(LoadedObject->GetClass()->GetDefaultObject());
		}
	}
	return nullptr;*/
}

bool UPMTFWGlobals::IsDataRandomizationForAttachmentsEnabled() const
{
	return bEnableDataRandomizationForAttachments;
}

void UPMTFWGlobals::LoadAmmunitionClasses()
{
	if (!AmmunitionObjectLibrary)
	{
		AmmunitionObjectLibrary = UObjectLibrary::CreateLibrary(UPMTFWAmmunition::StaticClass(), true, GIsEditor && !IsRunningCommandlet());
	}
	TArray<FString> Paths;
	Paths.Add(TEXT("/Game/Ammo"));
	const int32 AmmunitionTypesCount = AmmunitionObjectLibrary->LoadBlueprintAssetDataFromPaths(Paths, true);
	AmmunitionObjectLibrary->GetAssetDataList(AmmunitionAssetData);
	UE_LOG(PMTFWFiregunWeaponLog, Warning, TEXT("'%s': Loaded '%d' ammunition types "), ANSI_TO_TCHAR(__FUNCTION__), AmmunitionTypesCount);

#if WITH_EDITOR
	LogAmmoTagIntegrity(AmmunitionAssetData);
#endif // WITH_EDITOR
}

void UPMTFWGlobals::LoadAssetsSync(const TArray<FSoftObjectPath>& SoftObjects, FStreamableDelegate DelegateToCall)
{
	FStreamableManager& StreameableManager = UAssetManager::GetStreamableManager();
	StreameableManager.RequestSyncLoad(SoftObjects, false, FString(TEXT("Loading Sync")));
	FStreamableHandle::ExecuteDelegate(DelegateToCall);
}

void UPMTFWGlobals::LoadAssetsAsync(const TArray<FSoftObjectPath>& SoftObjects, FStreamableDelegate DelegateToCall)
{
	UAssetManager& AssetManager = UAssetManager::Get();
	AssetManager.LoadAssetList(SoftObjects, DelegateToCall);
}
