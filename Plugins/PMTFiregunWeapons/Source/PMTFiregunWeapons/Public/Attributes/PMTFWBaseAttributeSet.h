// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "PMTFWBaseAttributeSet.generated.h"

// Uses macros from AttributeSet.h
#define PMTFW_ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * Base attribute set for firegun weapons, attachment and ammo.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWBaseAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

	/** Set default values. */
	UPMTFWBaseAttributeSet();

	void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;

	bool PreGameplayEffectExecute(struct FGameplayEffectModCallbackData& Data) override;

	/** Reset the value of all the attributes to 0.0 so we can reapply gameplay effects that modified these attributes.
	* This should be called after all active gameplay effects were removed. */
	virtual void ResetAttributes();

	/** This measures how much damage can be applied to a target. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Damage;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, Damage);
	/** How much time does the firegun need to reload a full magazine. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData ReloadSpeed;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, ReloadSpeed);
	/** How much does a bullet can get be out of the cone range while firing. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Spread;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, Spread);
	/** Maximum spread permitted. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxSpread;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, MaxSpread);
	/** How much the pellet spreads continously. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData SpreadIncrementPerPellet;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, SpreadIncrementPerPellet);
	/** How quick does the gun fire a bullet. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData FireRate;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, FireRate);
	/** How quick does the bullet travel (only applies to weapons that shoot projectiles, not traces). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData BulletSpeed;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, BulletSpeed);
	/** Range of the bullet (for traces mostly). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData BulletRange;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, BulletRange);
	/** Chances of doing a critical shot. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData CriticalRate;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, CriticalRate);
	/** How much extra damage does a critical hit. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData CriticalDamage;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, CriticalDamage);
	/** Weight of the weapon. Can have different meanings based on the game (from reducing walk speed to inventory limit) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Weight;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, Weight);
	/** How many units can a pellet penetrate. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Penetration;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, Penetration);
	/** Noise that the firegun does while shooting. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData SoundNoise;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, SoundNoise);
	/** Size of the current magazine (not the max amount of ammo). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MagazineSize;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, MagazineSize);
	/** How many pellets a single bullet produces (a shotgun bullet normally fires more than 6 'pellets') */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData PelletsPerBullet;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, PelletsPerBullet);
	/** How many bullets we fire (think like a double-cannon shotgun). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData BulletsPerShot;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, BulletsPerShot);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Recoil;
	PMTFW_ATTRIBUTE_ACCESSORS(UPMTFWBaseAttributeSet, Recoil);
};
