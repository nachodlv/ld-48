// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunData.h"

#include "PMTFWFiregunTrackerComp.generated.h"

class UPMTFWFiregunTrackerComp;

DECLARE_DELEGATE_TwoParams(FPMTFWFiregunInstanceUpdateDelegate, UPMTFWFiregunTrackerComp*, const FPMTFWFiregunInstanceArray&);

/** Determines how the replication of the component will work. */
UENUM(BlueprintType)
enum class EPMTFWFiregunTrackReplicationMode : uint8
{
	NoReplication = 0, // No data will be replicated.
	All = 1, // Replicated to all.
	OwnerOnly = 2 // Replicated to the owner of the component only.
};

/** Basic component that handles the replication of firegun data. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTFIREGUNWEAPONS_API UPMTFWFiregunTrackerComp : public UActorComponent, public IPMTFWFiregunTrackerInterface
{
	GENERATED_BODY()

public:	

	UPMTFWFiregunTrackerComp();

	// ~ Begin IPMTFWFiregunTrackerInterface

	/** Create a firegun instance with the given parameters and return the handle. */
	FPMTFWFiregunInstanceHandle AddFiregunInstance(TUniquePtr<FPMTFWFiregunInstanceCreationParams> CreationParameters) override;
	/** Add the given existent firegun instance. */
	FPMTFWFiregunInstanceHandle AddExistingFiregunInstance(const FPMTFWFiregunInstanceDataItem& FiregunInstanceToAdd) override;
	/** Remove an existing firegun instance. */
	void RemoveFiregunInstance(FPMTFWFiregunInstanceHandle InInstanceHandle) override;
	/** Verifies if a given handle is valid (i.e. an instance with that value exists). */
	bool IsHandleValid(FPMTFWFiregunInstanceHandle InInstanceHandle) const override;
	/** Get an instance data item. Will crash if the item does not exists. */
	const FPMTFWFiregunInstanceDataItem& GetInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle) const override;
	/** Get a mutable instance data item. Will crash if the item does not exists. */
	FPMTFWFiregunInstanceDataItem& GetMutableInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle) override;
	/** Add the given attachment to the given handle if possible.
	* @returns Valid handle if attachment was added, invalid if not. */
	FPMTFWAttachmentInstanceHandle AddAttachmentToInstance(FPMTFWFiregunInstanceHandle InInstanceHandle,
		TSubclassOf<UPMTFWSimpleAttachment> InAttachmentClass, int32 InUniqueSeed,
		FGameplayAbilityTargetDataHandle AttachmentCustomData = FGameplayAbilityTargetDataHandle()) override;
	/** Removes the given attachment from the given handle if possible.
	* @returns true if attachment was removed, false otherwise. */
	bool RemoveAttachmentFromInstance(FPMTFWFiregunInstanceHandle InInstanceHandle, FPMTFWFiregunInstanceHandle InAttachmentHandle) override;
	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	FPMTFWFiregunInstanceAddedDelegate& GetFiregunInstanceAddedDelegate() override;
	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	FPMTFWFiregunInstanceRemovedDelegate& GetFiregunInstanceRemovedDelegate() override;
	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	FPMTFWFiregunInstanceChangedDelegate& GetFiregunInstanceChangedDelegate() override;

	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	FPMTFWFiregunAttachmentInstanceAddedDelegate& GetFiregunAttachmentInstanceAddedDelegate() override;
	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	FPMTFWFiregunAttachmentInstanceRemovedDelegate& GetFiregunAttachmentInstanceRemovedDelegate() override;
	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	FPMTFWFiregunAttachmentInstanceChangedDelegate& GetFiregunAttachmentInstanceChangedDelegate() override;

	TArray<FPMTFWFiregunInstanceHandle> GetAllFiregunHandles() const;

	// ~ End IPMTFWFiregunTrackerInterface

	/** Delegate fired when there's an update on the instances. */
	FPMTFWFiregunInstanceUpdateDelegate FiregunInstanceUpdateDelegate;

protected:

	/** Register to logging if applies. */
	void BeginPlay() override;
	/** Unregister from logging if applies. */
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	/** Set as owner of the replication array. */
	void InitializeComponent() override;
	/** Set the array of fireguns to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	virtual void OnRep_FiregunInstances();

	/** Verify if we can add the attachment to the given instance by checking that the instance does not have
	* an attachment of the same type already attached.
	* Override if you want to add specific behavior rather than just checking if the type is already used. */
	virtual bool CanAddAttachment(const FPMTFWFiregunInstanceDataItem& InInstance, const UPMTFWSimpleAttachment* InAttachment,
		FGameplayAbilityTargetDataHandle AttachmentCustomData = FGameplayAbilityTargetDataHandle());

#if WITH_EDITOR

	/** Enable or disable logging. */
	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif // WITH_EDITOR

	/** Determines how are we going to replicate the firegun instances. */
	UPROPERTY(EditDefaultsOnly, Category = "TrackerComp|Networking|Replication")
	EPMTFWFiregunTrackReplicationMode TrackReplicationMode = EPMTFWFiregunTrackReplicationMode::All;

#if WITH_EDITORONLY_DATA
	UPROPERTY(EditAnywhere, Category = "Debug")
	bool bDebugDataInScreen = false;
	UPROPERTY(EditAnywhere, Category = "Debug", meta = (EditCondition = "bDebugDataInScreen"))
	bool bDebugReplication = false;
	UPROPERTY(EditAnywhere, Category = "Debug", meta = (EditCondition = "bDebugDataInScreen"))
	bool bDebugStatic = false;
	UPROPERTY(EditAnywhere, Category = "Debug", meta = (EditCondition = "bDebugDataInScreen"))
	bool bDebugFiregunInstances = false;
#endif // WITH_EDITORONLY_DATA

private:

	/** Instances of fireguns to be replicated. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_FiregunInstances, meta = (PMTCDebugCategory = "Replication", PMTCDebugToggleBy = "bDebugFiregunInstances"))
	FPMTFWFiregunInstanceArray FiregunInstances;

	/** Delegate fired when a firegun instance is replicated. */
	FPMTFWFiregunInstanceAddedDelegate FiregunInstanceAddedDelegate;
	/** Delegate fired when a firegun instance is about to be removed. */
	FPMTFWFiregunInstanceRemovedDelegate FiregunInstanceRemovedDelegate;
	/** Delegate fired when a firegun instance is changed. */
	FPMTFWFiregunInstanceChangedDelegate FiregunInstanceChangedDelegate;
	/** Delegate fired when an attachment is added to a firegun instance. */
	FPMTFWFiregunAttachmentInstanceAddedDelegate FiregunAttachmentInstanceAddedDelegate;
	/** Delegate fired when an attachment is about to be removed to a firegun instance. */
	FPMTFWFiregunAttachmentInstanceRemovedDelegate FiregunAttachmentInstanceRemovedDelegate;
	/** Delegate fired when an attachment is changed to a firegun instance. */
	FPMTFWFiregunAttachmentInstanceChangedDelegate FiregunAttachmentInstanceChangedDelegate;
		
};
