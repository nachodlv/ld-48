// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWFiregunEquipComponent.generated.h"

class IPMTFWFiregunTrackerInterface;
struct FPMTFWFiregunInstanceDataItem;

/** Component that manages which firegun is currently equipped for use. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTFIREGUNWEAPONS_API UPMTFWFiregunEquipComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPMTFWFiregunEquipComponent();

	/** Equip the given firegun handle. */
	UFUNCTION(BlueprintCallable)
	void EquipFiregun(FPMTFWFiregunInstanceHandle InHandle);
	/** Verifies if there's any firegun equipped. */
	UFUNCTION(BlueprintPure)
	bool HasAnyFiregunEquipped() const;
	/** Retrieve current firegun's handle. */
	UFUNCTION(BlueprintPure)
	FPMTFWFiregunInstanceHandle GetCurrentFiregunHandle() const;

protected:

	/** Check if the owner or any of its component inherits from "IPMTFWFiregunTracker" and cache it. */
	void InitializeComponent() override;
	/** Unbind delegates. */
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:

	void OnFiregunInstanceRemoved(const FPMTFWFiregunInstanceDataItem& FiregunInstance);

	/** Tracker that takes care of all the fireguns that can be equipped. */
	UPROPERTY(Transient)
	TScriptInterface<IPMTFWFiregunTrackerInterface> Tracker = nullptr;

	/** Handle of the firegun equipped. */
	FPMTFWFiregunInstanceHandle EquippedFiregunHandle;
	/** Handle of the delegate when a firegun instance is deleted. */
	FDelegateHandle InstanceRemovedDelegateHandle;

		
};
