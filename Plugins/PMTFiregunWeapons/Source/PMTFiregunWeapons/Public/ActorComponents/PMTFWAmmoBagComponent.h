// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// PMTC Includes
#include "Utilities/PMTCCommonTypes.h"

#include "PMTFWAmmoBagComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPMTFWAmmoBagBulletCountChanged, const FPMTCTagAttribute&, TagAttribute);

/** Component that holds the amount of bullets we currently have for different types of ammo. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTFIREGUNWEAPONS_API UPMTFWAmmoBagComponent : public UActorComponent, public IPMTCTagAttributeHandlerInterface
{
	GENERATED_BODY()

public:	

	UPMTFWAmmoBagComponent();

	// ~ Begin IPMTCTagAttributeHandlerInterface
	void NotifyTagAttributeChanged(const FPMTCTagAttribute& InTagAttribute) override;
	// ~ End IPMTCTagAttributeHandlerInterface

	/** Add the given array of bullet types. If any of them exists, they will be replaced. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void AddAmmoAttributes(const TArray<FPMTCTagAttribute>& AmmoTypes);
	/** Increment (or decrement if the value is negative) the amount of bullets of given type (if we have it). */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool IncrementBulletCount(FGameplayTag AmmoTag, int32 InValue);
	/** Set the initial count for a given ammo type. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool SetBulletCountInitialValue(FGameplayTag AmmoTag, int32 InValue);
	/** Add modifier to the given ammo.
	* if "InValue" is 0.0f, then the value from the modifier will be used instead.
	* @returns true if the modifier was applied, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool AddModifierToAmmo(FGameplayTag AmmoTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue = 0.0f);
	/** Remove modifier from the given ammo with the given stack amount.
	* If StacksToRemove is -1, all stacks are removed.
	* @returns true if the modifier was removed, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool RemoveModifierFromAmmo(FGameplayTag AmmoTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove = -1);
	/** Retrieve the total bullets we have left in the bag. */
	UFUNCTION(BlueprintPure)
	float GetCountForBulletType(FGameplayTag AmmoTag);


	/** Callback called when the amount of bullets of a specific type changed. */
	UPROPERTY(BlueprintAssignable)
	FPMTFWAmmoBagBulletCountChanged BulletCountChanged;

protected:

	/** Set owner on tag attribute array. */
	void InitializeComponent() override;
	/** Set properties to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	virtual void OnRep_BulletsPerAmmoType();

	/** Track the amount of bullets we have per ammo type. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_BulletsPerAmmoType)
	FPMTCTagAttributeArray BulletsPerAmmoType;
};
