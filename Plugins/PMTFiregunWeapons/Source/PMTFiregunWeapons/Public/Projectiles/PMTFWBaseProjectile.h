// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PMTFWBaseProjectile.generated.h"

/** Base class for firegun projectiles. */
UCLASS()
class PMTFIREGUNWEAPONS_API APMTFWBaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	APMTFWBaseProjectile();

};
