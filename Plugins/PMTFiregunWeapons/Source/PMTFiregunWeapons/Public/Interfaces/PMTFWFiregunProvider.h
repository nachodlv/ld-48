// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PMTFWFiregunProvider.generated.h"

class APMTFWActiveFiregunsProxy;
class APMTFWFiregun;
struct FPMTFWFiregunInstanceDataItem;

UINTERFACE(MinimalAPI)
class UPMTFWFiregunProvider : public UInterface
{
	GENERATED_BODY()
};

/**
 * Simple interface for objects that provide the current firegun being used.
 */
class PMTFIREGUNWEAPONS_API IPMTFWFiregunProvider
{
	GENERATED_BODY()

public:


	virtual void NotifyBulletCountChanged(const FPMTFWFiregunInstanceDataItem& ItemThatChanged) = 0;
	/** Check if there's any firegun equipped. */
	virtual bool HasAnyFiregunEquipped() const = 0;
	/** Get a mutable reference to the current equipped firegun. */
	virtual FPMTFWFiregunInstanceDataItem& GetMutableCurrentEquippedFiregun() = 0;
	/** Get a const reference to the current equipped firegun. */
	virtual const FPMTFWFiregunInstanceDataItem& GetCurrentEquippedFiregun() const = 0;
	/** Get the current physical representation in the world of the firegun. */
	virtual APMTFWFiregun* GetMutableCurrentPhysicalFiregun() const = 0;
	/** Get the proxy associated with the current equipped firegun. */
	virtual APMTFWActiveFiregunsProxy* GetMutableCurrentFiregunProxy() const = 0;
};
