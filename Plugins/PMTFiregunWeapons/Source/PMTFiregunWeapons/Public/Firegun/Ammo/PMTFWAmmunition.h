// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "GameplayTagContainer.h"

// PMTFW Includes
#include "GameplayEffects/PMTFWAmmoGameplayEffect.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWAmmunition.generated.h"

class UGameplayEffect;

/**
 * Base class for ammunition. You can use this to have different ammo types
 * for the same weapon, like standard ammo or explosive rounds.
 */
UCLASS(BlueprintType, Blueprintable)
class PMTFIREGUNWEAPONS_API UPMTFWAmmunition : public UObject
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	TSoftClassPtr<UGameplayEffect> GetHitGameplayEffect() const;
	UFUNCTION(BlueprintPure)
	TSoftClassPtr<UPMTFWAmmoGameplayEffect> GetAttributesGameplayEffect() const;
	UFUNCTION(BlueprintPure)
	TSoftObjectPtr<UTexture2D> GetIcon() const;

	/** Type of this ammunition. Public for "GET_MEMBER_NAME_CHECKED" calls works. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag AmmoTypeTag;
	/** Mirrors AmmoTypeTag in order to be asset registry searchable */
	UPROPERTY(VisibleDefaultsOnly, AssetRegistrySearchable)
	FName AmmoTypeName;

protected:

#if WITH_EDITOR

	/** Make sure that the ammo type name is the same as the tag. */
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;

#endif // WITH_EDITOR

	/** Name of the ammunition. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText AmmoName;
	/** Which fireguns this ammunition can be used. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer AvailableForFireguns;
	/** Attributes that this bullet will apply to the firegun. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AssetBundles = "Gameplay"))
	TSoftClassPtr<UPMTFWAmmoGameplayEffect> AttributesGameplayEffect;
	/** Image used for UI purposes. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AssetBundles = "UI"))
	TSoftObjectPtr<UTexture2D> Icon;
	/** Gameplay effect used to cause a hit to actors with ASC. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AssetBundles = "Gameplay"))
	TSoftClassPtr<UGameplayEffect> HitGameplayEffect;
	
};
