// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameplayAbilitySpec.h"
#include "GameplayTagContainer.h"

// PMTFW Includes
#include "Firegun/Attachments/PMTFWAttachmentTypes.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWSimpleAttachment.generated.h"

class UGameplayEffect;
class USkeletalMesh;
class UStaticMesh;

class UPMTFWFiregunEquipGE;

/**
 * Class that represents an attachment for a firegun.
 */
UCLASS(BlueprintType, Blueprintable)
class PMTFIREGUNWEAPONS_API UPMTFWSimpleAttachment : public UObject
{
	GENERATED_BODY()

public:

	/** Retrieve attachment's type. */
	const FGameplayTag& GetAttachmentType() const;
	/** Retrieve attachment's attribute gameplay effect. */
	UFUNCTION(BlueprintPure)
	TSoftClassPtr<UGameplayEffect> GetAttributesGameplayEffect() const;
	/** Retrieve firegun ability's set. */
	UFUNCTION(BlueprintPure)
	TSoftObjectPtr<UPMTFWAbilitySetDataAsset> GetAbilitySet() const;
	/** Retrieve attachment's additional GEs */
	UFUNCTION(BlueprintPure)
	TArray<TSoftClassPtr<UPMTFWFiregunEquipGE>> GetAdditionalGEs() const;

protected:

	/** Name of the attachment. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta Data")
	FText AttachmentName;
	/** Type of the attachment. For example, "Canon", "Magazine" and so on. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta Data")
	FGameplayTag AttachmentType;
	/** Rarity of the attachment. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta Data")
	FGameplayTag RarityLevel;
	/** Determines which weapon types can use this attachment. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta Data")
	FGameplayTagContainer AvailableForFireguns;
	/** Static representation of the attachment. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visualization")
	UStaticMesh* StaticMesh = nullptr;
	/** Skeletal representation of the attachment. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visualization")
	USkeletalMesh* SkeletalMesh = nullptr;
	/** Where does the attachment will be applied (applies only if the firegun is a skeletal mesh) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visualization")
	FName AttachmentSocketName;
	/** Attributes that this attachment will apply to the firegun. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities", meta = (AssetBundles = "Gameplay"))
	TSoftClassPtr<UGameplayEffect> AttributesGameplayEffect;
	/** Gameplay effects that will be applied when attached to a firegun when it's currently active. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities", meta = (AssetBundles = "Gameplay"))
	TArray<TSoftClassPtr<UPMTFWFiregunEquipGE>> AttachmentGameplayEffects;
	/** Ability specs that override the ones in the firegun. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities", meta = (AssetBundles = "Gameplay"))
	TSoftObjectPtr<UPMTFWAbilitySetDataAsset> FiregunAbilitySet;
	
};
