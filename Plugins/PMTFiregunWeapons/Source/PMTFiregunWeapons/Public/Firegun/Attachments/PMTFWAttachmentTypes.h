// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Abilities/GameplayAbilityTargetTypes.h"


#include "PMTFWAttachmentTypes.generated.h"

/** Lightweight structure that uniquely identifies an attachment instance. */
USTRUCT(BlueprintType)
struct FPMTFWAttachmentInstanceHandle
{
	GENERATED_BODY()

public:

	FPMTFWAttachmentInstanceHandle() {}
	FPMTFWAttachmentInstanceHandle(int32 InHandle) : Handle(InHandle) {}
	FPMTFWAttachmentInstanceHandle(const FPMTFWAttachmentInstanceHandle& InHandle) :
		Handle(InHandle.Handle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	UPROPERTY(Transient)
	int32 Handle = INDEX_NONE;

	friend bool operator==(FPMTFWAttachmentInstanceHandle Lhs, FPMTFWAttachmentInstanceHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FPMTFWAttachmentInstanceHandle Lhs, FPMTFWAttachmentInstanceHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	operator bool() const
	{
		return IsValid();
	}

	static FPMTFWAttachmentInstanceHandle GenerateNewHandle()
	{
		static int32 AttachmentHandleNumber = 0;
		++AttachmentHandleNumber;
		return FPMTFWAttachmentInstanceHandle(AttachmentHandleNumber);
	}
};

class IPMTFWFiregunTrackerInterface;
class UPMTFWSimpleAttachment;
struct FPMTFWFiregunAttachmentArray;

/** Mutable data that can be replicated which represents the state of a firegun attachment. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunAttachmentDataItem : public FFastArraySerializerItem
{
	GENERATED_BODY()

public:


	/** Notify that we are about to be removed.  */
	void PreReplicatedRemove(const FPMTFWFiregunAttachmentArray& InArraySerializer);
	/** Notify that we got added. */
	void PostReplicatedAdd(const FPMTFWFiregunAttachmentArray& InArraySerializer);
	/** Notify that we got changed. */
	void PostReplicatedChange(const FPMTFWFiregunAttachmentArray& InArraySerializer);

	/** Class of the firegun used. */
	UPROPERTY(Transient)
	TSubclassOf<UPMTFWSimpleAttachment> AttachmentClass;
	/** Handle of the attachment. */
	UPROPERTY(Transient)
	FPMTFWAttachmentInstanceHandle AttachmentHandle;
	/** Seed used to randomize attachment characteristics if desired. */
	UPROPERTY(Transient)
	int32 UniqueSeed = 0;
	/** Custom data that can be passed for any attachment instance using target data. */
	UPROPERTY(Transient)
	FGameplayAbilityTargetDataHandle AttachmentCustomData;
};

/** Special array for attachment replication. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunAttachmentArray : public FFastArraySerializer
{
	GENERATED_BODY()

	/** Array of firegun attachments. */
	UPROPERTY()
	TArray<FPMTFWFiregunAttachmentDataItem> Attachments;
	/** Owner of the array. Useful to notify about replication changes. */
	UPROPERTY(Transient, NotReplicated)
	TScriptInterface<IPMTFWFiregunTrackerInterface> Owner = nullptr;

	FPMTFWFiregunAttachmentDataItem& AddItem(const FPMTFWFiregunAttachmentDataItem& Instance, bool bMarkItemDirty = true)
	{
		FPMTFWFiregunAttachmentDataItem& RefItem = Attachments.Add_GetRef(Instance);
		if (bMarkItemDirty)
		{
			MarkItemDirty(RefItem);
		}
		// Server does not call replication methods so make sure to do it.
		RefItem.PostReplicatedAdd(*this);
		return RefItem;
	}

	/** Remove a given item from the array.
	* @returns True if an item was removed, false otherwise. */
	bool RemoveItem(FPMTFWAttachmentInstanceHandle InHandle, bool bMarkArrayDirty = true)
	{
		for (int32 Index = 0; Index < Attachments.Num(); ++Index)
		{
			if (Attachments[Index].AttachmentHandle == InHandle)
			{
				Attachments[Index].PreReplicatedRemove(*this);
				Attachments.RemoveAt(Index, 1);
				if (bMarkArrayDirty)
				{
					MarkArrayDirty();
				}
				return true;
			}
		}
		return false;
	}

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FPMTFWFiregunAttachmentDataItem, FPMTFWFiregunAttachmentArray>(Attachments, DeltaParms, *this);
	}
};

/** Specified to allow fast TArray replication */
template<>
struct TStructOpsTypeTraits<FPMTFWFiregunAttachmentArray> : public TStructOpsTypeTraitsBase2<FPMTFWFiregunAttachmentArray>
{
	enum
	{
		WithNetDeltaSerializer = true,
	};
};

/**
 * 
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWAttachmentTypes : public UObject
{
	GENERATED_BODY()
	
};
