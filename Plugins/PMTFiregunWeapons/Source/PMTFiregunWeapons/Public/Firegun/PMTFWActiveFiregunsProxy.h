// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"
#include "GameplayPrediction.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunData.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWActiveFiregunsProxy.generated.h"

class AController;

class UGameplayEffect;

class IPMTFWFiregunTrackerInterface;
class UPMTFWBaseAttributeSet;
class UPMTGUAbilityRestorerComp;
class UPMTGUChainedAbilityRemovalComp;

struct FPMTFWFiregunInstanceDataItem;

/** Actor that acts as a proxy between the player that has an ASC and the active fireguns.
* The idea of this actor is to keep the functionality of GAS for firegun attributes
* WITHOUT replication. We force NO replication because otherwise the controller will
* have to manage all the ASC and this could come with more overhead. Instead,
* there are a set of functions that will help us in the prediction of GE's to apply to fireguns
* without having to manage many ASCs.
* The advantage of this approach is having the great power of GAS with modifying attributes
* without having to implement the same system again.
* So this DOES NOT support:
* - Abilities
* - Cues
* But this DOES support:
* + GE to modify attributes.
* + React to added tags to the owner of these fireguns to apply specific behavior. */
UCLASS()
class PMTFIREGUNWEAPONS_API APMTFWActiveFiregunsProxy : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:	

	APMTFWActiveFiregunsProxy();

	/** Create proxies that will be owned by the given player. */
	UFUNCTION(BlueprintCallable)
	static TArray<APMTFWActiveFiregunsProxy*> CreateFiregunProxiesForPlayer(AController* OwnerController,
		TSubclassOf<APMTFWActiveFiregunsProxy> ProxyClass, int32 ActiveCount);

	/** Apply the given gameplay effect if the firegun handle is valid and equal to the same firegun we have. */
	FActiveGameplayEffectHandle ApplyGameplayEffect(const FGameplayEffectSpec& InGameplayEffectSpec, FPMTFWFiregunInstanceHandle InFiregunHandle);
	/** Removes an applied gameplay effect. */
	bool RemoveGameplayEffect(FActiveGameplayEffectHandle GameplayEffectHandle, FPMTFWFiregunInstanceHandle InFiregunHandle);
	/** Change current instance or, in case it's the same, updates the attributes in case an attachment or ammo type changed. */
	void ChangeOrUpdateFiregunInstance(UAbilitySystemComponent* OwnerASC,
		const FPMTFWFiregunInstanceDataItem& InFiregunInstance,
		IPMTFWFiregunTrackerInterface* FiregunProvider,
		FGameplayEffectContextHandle InContextHandle = FGameplayEffectContextHandle());

	/** Equip the current firegun instance if possible (handle is valid) and return
	* any gameplay effect that was applied to the given 'OwnerASC'. */
	TArray<FActiveGameplayEffectHandle> EquipCurrentFiregunInstance(UAbilitySystemComponent* OwnerASC,
		UPMTGUAbilityRestorerComp* OwnerAbilityRestorer,
		UPMTGUChainedAbilityRemovalComp* OwnerChainAbilityRemovalComp,
		TScriptInterface<IPMTFWFiregunTrackerInterface> FiregunTracker,
		FPredictionKey PredictionKey = FPredictionKey(),
		FGameplayEffectContextHandle InContextHandle = FGameplayEffectContextHandle(),
		bool bStoreAbilities = false);

	/** Unequips the current firegun, making sure to restore the abilities. */
	UFUNCTION(BlueprintCallable)
	void UnEquipCurrentFiregunInstance(UAbilitySystemComponent* OwnerASC,
		UPMTGUAbilityRestorerComp* OwnerAbilityRestorer,
		const TArray<FActiveGameplayEffectHandle>& EffectsAppliedToOwner);

	/** Check if this proxy has an active firegun instance. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy")
	bool DoesProxyContainFiregunInstance() const;

	/** Check if this proxy has an active firegun instance by the given handle. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy")
	bool DoesProxyContainFiregunInstanceHandle(FPMTFWFiregunInstanceHandle FiregunHandle) const;

	/** Retrieve firegun's damage. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunDamage() const;
	/** Retrieve firegun's reload speed. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunReloadSpeed() const;
	/** Retrieve instance's base spread. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunBaseSpread() const;
	/** Retrieve instance's max spread. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunMaxSpread() const;
	/** Retrieve instance's spread increment per pellet. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunSpreadIncrementPerPellet() const;
	/** Retrieve firegun's fire rate. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunFireRate() const;
	/** Retrieve instance's bullet speed. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunBulletSpeed() const;
	/** Retrieve instance's max range. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunMaxRange() const;
	/** Retrieve instance's critical rate. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunCriticalRate() const;
	/** Retrieve instance's critical damage. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunCriticalDamage() const;
	/** Retrieve instance's weight. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunWeight() const;
	/** Retrieve instance's penetration. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunPenetration() const;
	/** Retrieve instance's penetration. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunSoundNoise() const;
	/** Retrieve instance's magazine size. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunMagazineSize() const;
	/** Retrieve instance's pellets per shot. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunPelletsPerShot() const;
	/** Retrieve instance's bullets per shot. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|ActiveFiregunProxy|Attributes")
	float GetFiregunBulletsPerShot() const;

	/** Retrieve's proxy ASC. */
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	/** Delegate fired when the instance is about to be removed */
	FPMTFWFiregunInstanceRemovedDelegate InstanceRemovedDelegate;
	/** Delegate fired when the instance we had was changed. */
	FPMTFWFiregunInstanceChangedDelegate InstanceChangedDelegate;

protected:

	/** Gather all the gameplay effects that should be applied to the firegun when "ChangeOrUpdate" is called.
	* @param OwnerASC - ASC where we get the tags we want in case we need to apply something specific.
	* @param InstanceToUse - Firegun instance that will be used. */
	virtual TArray<TSubclassOf<UGameplayEffect>> GatherFiregunGameplayEffects(UAbilitySystemComponent* OwnerASC,
		const FPMTFWFiregunInstanceDataItem& InstanceToUse, FGameplayEffectContextHandle InContextHandle) const;

	/** Callback called when an instance is removed. */
	UFUNCTION()
	virtual void OnInstanceRemoved(const FPMTFWFiregunInstanceDataItem& InstanceRemoved);

	/** Class used to create the ASC that will be bound to a firegun instance. */
	UPROPERTY(EditDefaultsOnly, Category = "ASC")
	TSubclassOf<UAbilitySystemComponent> AbilitySystemComponentClass;
	/** Class used for the attribute sets. */
	UPROPERTY(EditDefaultsOnly, Category = "ASC")
	TSubclassOf<UPMTFWBaseAttributeSet> FiregunAttributeSetClass;

private:

	/** Create ASC, attribute set and so on. */
	void InitializeActiveInstance();

	/** Handle of the firegun this proxy is using. */
	UPROPERTY(Transient, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FPMTFWFiregunInstanceHandle InstanceHandle;
	/** ASC that will be used to handle tags and GE's for a gun. */
	UPROPERTY(Transient, BlueprintReadOnly, VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"))
	UAbilitySystemComponent* InstanceASC = nullptr;
	/** Attribute set bound to the ASC. */
	UPROPERTY(Transient)
	UPMTFWBaseAttributeSet* InstanceAttributeSet = nullptr;

	/** All gameplay effects that were activated for this instance. */
	TArray<FActiveGameplayEffectHandle> ActiveGameplayEffects;
	/** Gameplay effects that were applied when the firegun was actually being used. */
	TArray<FActiveGameplayEffectHandle> ActiveEquippedGameplayEffects;

	/** Handle to the instance removed delegate. */
	FDelegateHandle InstanceRemovedDelegateHandle;

};
