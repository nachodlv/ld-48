// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Abilities/GameplayAbilityTargetTypes.h"
#include "GameplayAbilitySpec.h"
#include "GameplayTagContainer.h"
#include "UObject/Interface.h"
#include "UObject/NoExportTypes.h"

// PMTFW Includes
#include "Firegun/Attachments/PMTFWAttachmentTypes.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWFiregunData.generated.h"

class APMTFWFiregun;
class UPMTFWFiregunData;

class UStaticMesh;
class USkeletalMesh;

struct FPMTFWFiregunInstanceArray;

/** Mutable data that can be replicated which represents the state of a firegun. */
USTRUCT(BlueprintType)
struct PMTFIREGUNWEAPONS_API FPMTFWFiregunInstanceDataItem : public FFastArraySerializerItem
{
	GENERATED_BODY()

public:

	/** Remove proper amount of bullets from magazine and return the remaining amount of bullets. */
	uint16 HandleBulletFired(const FGameplayTag& InAmmoTag, uint16 BulletsShot = 1);
	/** Verify if there is at least one bullet in the magazine */
	bool HasBulletsInMagazine(const FGameplayTag& InAmmoTag) const;
	uint16 GetCurrentBulletsForAmmoType(const FGameplayTag& InAmmoTag) const;
	void ModifyBulletCount(const FGameplayTag& InAmmoTag, uint16 NewValue);

	// ~ Begin FFastArraySerializerItem

	/** Notify that we are about to be removed.  */
	void PreReplicatedRemove(const FPMTFWFiregunInstanceArray& InArraySerializer);
	/** Notify that we got added. */
	void PostReplicatedAdd(const FPMTFWFiregunInstanceArray& InArraySerializer);
	/** Notify that we got changed. */
	void PostReplicatedChange(const FPMTFWFiregunInstanceArray& InArraySerializer);

	// ~ End FFastArraySerializerItem

	/** Attachments used by this firegun. */
	UPROPERTY(Transient)
	FPMTFWFiregunAttachmentArray FiregunAttachments;
	/** Class of the firegun used. */
	UPROPERTY(Transient)
	TSubclassOf<UPMTFWFiregunData> FiregunDataClass;
	/** Unique identifier for this item. */
	UPROPERTY(Transient)
	FPMTFWFiregunInstanceHandle Handle;
	/** Current bullets that this weapon has in each magazine. */
	UPROPERTY(Transient)
	TArray<FPMTFWCurrentAmmoForBulletType> CurrentBulletsInMagazine;
	/** Custom data that can be passed around between firegun instances using target data. */
	UPROPERTY(Transient)
	FGameplayAbilityTargetDataHandle CustomFiregunData;
};

/** Special array for firegun instance replication.
* We use this instead of a plain array since little data will change
* between weapons, therefore we do not want to replicate all the
* array of objects that a player could have. */
USTRUCT(BlueprintType)
struct PMTFIREGUNWEAPONS_API FPMTFWFiregunInstanceArray : public FFastArraySerializer
{
	GENERATED_BODY()

	/** Array of firegun instances. */
	UPROPERTY()
	TArray<FPMTFWFiregunInstanceDataItem> Fireguns;
	/** Owner of the array. Useful to notify about replication changes. */
	UPROPERTY(Transient, NotReplicated)
	TScriptInterface<IPMTFWFiregunTrackerInterface> Owner = nullptr;

	/** Add an item to the array and call the replication method for the server. */
	FPMTFWFiregunInstanceDataItem& AddItem(const FPMTFWFiregunInstanceDataItem& Instance, bool bMarkItemDirty = true)
	{
		FPMTFWFiregunInstanceDataItem& RefItem = Fireguns.Add_GetRef(Instance);
		if (bMarkItemDirty)
		{
			MarkItemDirty(RefItem);
		}
		// Server does not call replication methods so make sure to do it.
		RefItem.PostReplicatedAdd(*this);
		return RefItem;
	}

	/** Remove a given item from the array.
	* @returns True if an item was removed, false otherwise. */
	bool RemoveItem(FPMTFWFiregunInstanceHandle InHandle, bool bMarkArrayDirty = true)
	{
		for (int32 Index = 0; Index < Fireguns.Num(); ++Index)
		{
			if (Fireguns[Index].Handle == InHandle)
			{
				// Server does not call replication methods so make sure to do it.
				Fireguns[Index].PreReplicatedRemove(*this);
				Fireguns.RemoveAt(Index, 1);
				if (bMarkArrayDirty)
				{
					MarkArrayDirty();
				}
				return true;
			}
		}
		return false;
	}

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FPMTFWFiregunInstanceDataItem, FPMTFWFiregunInstanceArray>(Fireguns, DeltaParms, *this);
	}
};

/** Specified to allow fast TArray replication */
template<>
struct TStructOpsTypeTraits<FPMTFWFiregunInstanceArray> : public TStructOpsTypeTraitsBase2<FPMTFWFiregunInstanceArray>
{
	enum
	{
		WithNetDeltaSerializer = true,
	};
};

UINTERFACE(MinimalAPI)
class UPMTFWFiregunTrackerInterface : public UInterface
{
	GENERATED_BODY()
};

DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunInstanceAddedDelegate, const FPMTFWFiregunInstanceDataItem&);
DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunInstanceRemovedDelegate, const FPMTFWFiregunInstanceDataItem&);
DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunInstanceChangedDelegate, const FPMTFWFiregunInstanceDataItem&);

DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunAttachmentInstanceAddedDelegate, const FPMTFWFiregunAttachmentDataItem&);
DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunAttachmentInstanceRemovedDelegate, const FPMTFWFiregunAttachmentDataItem&);
DECLARE_MULTICAST_DELEGATE_OneParam(FPMTFWFiregunAttachmentInstanceChangedDelegate, const FPMTFWFiregunAttachmentDataItem&);

/** Parameters used to create a firegun instance. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunInstanceCreationParams
{
	GENERATED_BODY()

public:

	/** Base class for the instance. */
	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<UPMTFWFiregunData> FiregunDataClass;
	/** A unique seed to randomize parameters. */
	UPROPERTY(BlueprintReadWrite)
	int32 UniqueSeed = 0;
	/** Bullets stored in this instance. It could happen that it's
	* a dropped weapon, therefore we want to persist the amount of bullets in the magazine. */
	UPROPERTY(BlueprintReadWrite)
	TArray<FPMTFWCurrentAmmoForBulletType> CurrentBulletsInMagazine;
	/** Custom data that can be passed around between firegun instances using target data. */
	UPROPERTY(BlueprintReadWrite)
	FGameplayAbilityTargetDataHandle CustomFiregunData;
	
};

/**
 * Determines that a class tracks the real data of a firegun.
 */
class PMTFIREGUNWEAPONS_API IPMTFWFiregunTrackerInterface
{
	GENERATED_BODY()

public:

	/** Add a firegun instance. */
	virtual FPMTFWFiregunInstanceHandle AddFiregunInstance(TUniquePtr<FPMTFWFiregunInstanceCreationParams> CreationParameters) = 0;

	/** Add an existent firegun instance. This could be used if we deal with "dropped" weapons.
	* Instead of creating a new instance, we just add the existent one and let replication handle the rest. */
	virtual FPMTFWFiregunInstanceHandle AddExistingFiregunInstance(const FPMTFWFiregunInstanceDataItem& FiregunInstanceToAdd) = 0;

	/** Remove an existing firegun instance. */
	virtual void RemoveFiregunInstance(FPMTFWFiregunInstanceHandle InInstanceHandle) = 0;

	/** Verifies if a given handle is valid (i.e. an instance with that value exists). */
	virtual bool IsHandleValid(FPMTFWFiregunInstanceHandle InInstanceHandle) const = 0;

	/** Retrieve a const instance data item. */
	virtual const FPMTFWFiregunInstanceDataItem& GetInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle) const = 0;

	/** Retrieve a mutable instance data item. This shouldn't be used by anyone, but exists to avoid people doing const_cast. */
	virtual FPMTFWFiregunInstanceDataItem& GetMutableInstanceDataItem(FPMTFWFiregunInstanceHandle InInstanceHandle) = 0;

	/** Add the given attachment to the given handle if possible.
	* @returns Valid handle if attachment was added, invalid if not. */
	virtual FPMTFWAttachmentInstanceHandle AddAttachmentToInstance(FPMTFWFiregunInstanceHandle InInstanceHandle, TSubclassOf<UPMTFWSimpleAttachment> InAttachmentClass, int32 InUniqueSeed,
		FGameplayAbilityTargetDataHandle AttachmentCustomData = FGameplayAbilityTargetDataHandle()) = 0;

	/** Removes the given attachment from the given handle if possible.
	* @returns true if attachment was removed, false otherwise. */
	virtual bool RemoveAttachmentFromInstance(FPMTFWFiregunInstanceHandle InInstanceHandle, FPMTFWFiregunInstanceHandle InAttachmentHandle) = 0;

	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	virtual FPMTFWFiregunInstanceAddedDelegate& GetFiregunInstanceAddedDelegate() = 0;
	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	virtual FPMTFWFiregunInstanceRemovedDelegate& GetFiregunInstanceRemovedDelegate() = 0;
	/** Get firegun instance delegate that should be fired when the replication of an instance occurs. */
	virtual FPMTFWFiregunInstanceChangedDelegate& GetFiregunInstanceChangedDelegate() = 0;

	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	virtual FPMTFWFiregunAttachmentInstanceAddedDelegate& GetFiregunAttachmentInstanceAddedDelegate() = 0;
	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	virtual FPMTFWFiregunAttachmentInstanceRemovedDelegate& GetFiregunAttachmentInstanceRemovedDelegate() = 0;
	/** Get firegun instance delegate that should be fired when the replication of an attachment occurs. */
	virtual FPMTFWFiregunAttachmentInstanceChangedDelegate& GetFiregunAttachmentInstanceChangedDelegate() = 0;
};

/**
 * Base class that specifies INMUTABLE data for a weapon.
 * The idea is to share common data between instances and any specialization
 * be handled in another struct.
 */
UCLASS(BlueprintType, Blueprintable)
class PMTFIREGUNWEAPONS_API UPMTFWFiregunData : public UObject
{
	GENERATED_BODY()

public:

	/** Applies this data to the given firegun actor. */
	UFUNCTION(BlueprintCallable, Category = "PMTFW|FiregunData")
	void ApplyDataToFiregun(APMTFWFiregun* FiregunToApply);
	/** Get firegun's name. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	const FText& GetFiregunName() const;
	/** Retrieve the initial transform from where bullets will be fired from.
	* A physical representation in the world must exist. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	FTransform GetShotInitialTransform(const APMTFWFiregun* Firegun) const;
	/** Retrieve the attribute's soft pointer. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	TSoftClassPtr<UGameplayEffect> GetAttributesGameplayEffect() const;
	/** Retrieve firegun's ability set. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	TSoftObjectPtr<UPMTFWAbilitySetDataAsset> GetAbilitySet() const;
	/** Retrieve firegun's skeletal mesh. */
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	USkeletalMesh* GetSkeletalMesh() const;
	UFUNCTION(BlueprintPure, Category = "PMTFW|FiregunData|Getters")
	FGameplayTag GetFiregunAmmoType() const;
	UFUNCTION(BlueprintPure)
	FGameplayTag GetFiregunWeaponType() const { return WeaponType; }

protected:

	/** Name of the firegun. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Attributes")
	FText FiregunName;
	/** Attributes that this firegun has. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities", meta = (AssetBundles = "Gameplay"))
	TSoftClassPtr<UGameplayEffect> AttributesGameplayEffect;
	/** Static mesh that could be used for the weapon. Can be null since normally they will use skeletal meshes. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Attributes")
	UStaticMesh* FiregunStaticMesh;
	/** Skeletal mesh that could be used for the weapon. Normally it will have something. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Attributes")
	USkeletalMesh* FiregunSkeletalMesh;
	/** In case we use a skeletal mesh, where do we start firing the projectile. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Attributes")
	FName FireSocketName;
	/** In case we use a static mesh, where do we start firing the projectile. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Attributes")
	FVector FireOffset = FVector::ZeroVector;
	/** Box extent used if the weapon has a box collision. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Collisions")
	FVector BoxExtent;
	/** Capsule radius used if the weapon has a capsule collision. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Collisions")
	float CapsuleRadius = 0.0;
	/** Capsule half height used if the weapon has a capsule collision. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Collisions")
	float CapsuleHalfHeight = 0.0;
	/** Type of the ammo to use. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Characteristics")
	FGameplayTag AmmoType;
	/** Type of the weapon. */
	UPROPERTY(EditDefaultsOnly, Category = "Firegun Characteristics")
	FGameplayTag WeaponType;
	/** Rarity of the firegun. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firegun Characteristics")
	FGameplayTag RarityLevel;
	/** Ability spec for the firegun. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firegun Characteristics", meta = (AssetBundles = "Gameplay"))
	TSoftObjectPtr<UPMTFWAbilitySetDataAsset> FiregunAbilitySet;
};
