// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "Abilities/GameplayAbilityTargetTypes.h"
#include "GameplayAbilitySpec.h"
#include "Engine/CollisionProfile.h"
#include "Engine/DataAsset.h"

#include "PMTFWFiregunUtilities.generated.h"

/** Determine how the firegun will shoot. */
UENUM(BlueprintType)
enum class EPMTFWProjectileType : uint8
{
	Projectile = 0, // Custom behavior (like a rocket). This is left to the projectile.
	Trace = 1 // Where we aim is where it shoots instantly.
};

/** Lightweight structure that uniquely identifies a firegun instance. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunInstanceHandle
{
	GENERATED_BODY()

public:

	FPMTFWFiregunInstanceHandle() {}
	FPMTFWFiregunInstanceHandle(int32 InHandle) : Handle(InHandle) {}
	FPMTFWFiregunInstanceHandle(const FPMTFWFiregunInstanceHandle& InHandle) :
		Handle(InHandle.Handle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	void Reset() { Handle = INDEX_NONE; }

	UPROPERTY()
	int32 Handle = INDEX_NONE;

	friend bool operator==(FPMTFWFiregunInstanceHandle Lhs, FPMTFWFiregunInstanceHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FPMTFWFiregunInstanceHandle Lhs, FPMTFWFiregunInstanceHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	operator bool() const
	{
		return IsValid();
	}

	static FPMTFWFiregunInstanceHandle GenerateNewHandle()
	{
		static int32 NewHandle = 0;
		++NewHandle;
		return FPMTFWFiregunInstanceHandle(NewHandle);
	}
};

/** Parameters used to make a trace to shoot a bullet. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunTraceParameters
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> ActorsToIgnore;
	/** Profile used to make the traces. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCollisionProfileName TraceProfile;
	/** Start location of the trace. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector StartTraceLocation;
	/** Start rotation of the trace. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator StartTraceRotation;
	/** Max range of the trace, does not consider the penetration. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxRange = 0.0f;
	/** How much length does the line trace penetrates when it hits a target. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Penetration = 0.0f;
	/** Spread used to fire pellets. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseSpread = 0.0f;
	/** Spread increment to fire pellets. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpreadIncrementPerPellet = 0.0f;
	/** Max spread permitted. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpread = 0.0f;
	/** How many traces we are going to do. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 PelletsAmount = 1;
	/** Whenever the trace is complex or not. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bComplexTrace = false;
	/** Whenever we ignore "Block" collision. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIgnoreBlockingHits = false;
	/** Whenever we show any debugging info. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDebug = true;
};

/** Minimal data that represents the hit of a bullet shot.
* The idea is to reproduce the same hits in the server.
* Multiple hits by multiple pellets are resumed by just using 12 bytes. */
USTRUCT(BlueprintType)
struct FPMTFWBulletHitTargetData : public FGameplayAbilityTargetData
{
	GENERATED_BODY()

public:

	FPMTFWBulletHitTargetData() {}

	/** View from player's controller. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector ViewStart = FVector::ZeroVector;
	/** Rotation from player's controller. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator ViewRotation = FRotator::ZeroRotator;
	/** Seed used to make the traces. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Seed = 0;
	/** Cached hit results for both client and server. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, NotReplicated)
	TArray<FHitResult> HitResults;
	/** Boolean that determines locally if we need to regenerate the data. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, NotReplicated)
	bool bNeedRegeneration = true;

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		ViewStart.NetSerialize(Ar, Map, bOutSuccess);
		ViewRotation.NetSerialize(Ar, Map, bOutSuccess);
		Ar << Seed;
		return true;
	}

	virtual UScriptStruct* GetScriptStruct() const override
	{
		return FPMTFWBulletHitTargetData::StaticStruct();
	}
};

template<>
struct TStructOpsTypeTraits<FPMTFWBulletHitTargetData> : public TStructOpsTypeTraitsBase2<FPMTFWBulletHitTargetData>
{
	enum
	{
		WithNetSerializer = true,
		WithCopy = true	
	};
};

/** Results produced by fireguns when doing line traces. */
USTRUCT(BlueprintType)
struct FPMTFWFiregunTraceResults
{
	GENERATED_BODY()

public:

	/** All hits produced by a single shot. */
	UPROPERTY(BlueprintReadOnly)
	TArray<FHitResult> Hits;
	/** Initial view start used to make the traces. */
	UPROPERTY(BlueprintReadOnly)
	FVector ViewStart;
	/** Initial view rotation to make the traces. */
	UPROPERTY(BlueprintReadOnly)
	FRotator ViewRot;
	/** Final spread. Useful for consecutive shots like an automatic rifle. */
	UPROPERTY(BlueprintReadOnly)
	float FinalSpread = 0.0f;
	/** Seed used for spread and other things. */
	UPROPERTY(BlueprintReadOnly)
	int32 SeedUsed = 0;
};

/** Single ability spec used to define an ability for a firegun. */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWFireAbilitySpecDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	/** Generate an ability spec from the data that the data asset has. */
	FGameplayAbilitySpec CreateAbilitySpec() const;

	/** Create ability spec for each of the chained abilities. */
	TArray<FGameplayAbilitySpec> CreateChainedAbilitySpecs() const;

	bool HasChainedAbilities() const { return ChainedAbilities.Num() > 0; }

	/** What ability to grant */
	UPROPERTY(EditDefaultsOnly, Category = "Ability Definition")
	TSubclassOf<UGameplayAbility> Ability;

	/** What level to grant this ability at */
	UPROPERTY(EditDefaultsOnly, Category = "Ability Definition", DisplayName = "Level")
	FScalableFloat LevelScalableFloat;

	/** Input ID to bind this ability to */
	UPROPERTY(EditDefaultsOnly, Category = "Ability Definition")
	int32 InputID;

	/** What will remove this ability later */
	UPROPERTY(EditDefaultsOnly, Category = "Ability Definition")
	EGameplayEffectGrantedAbilityRemovePolicy RemovalPolicy;

	/** Abilities that can be triggered from the main ability but they are not bound
	* to any input id. */
	UPROPERTY(EditDefaultsOnly, Category = "Ability Definition")
	TArray<TSubclassOf<UGameplayAbility>> ChainedAbilities;
};

/** Set of abilities used by a firegun (primary, secondary and special). */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWAbilitySetDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	/** Spec that describes the primary fire ability. */
	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	UPMTFWFireAbilitySpecDataAsset* PrimaryAbilitySpec;
	/** Spec that describes the secondary fire ability. */
	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	UPMTFWFireAbilitySpecDataAsset* SecondaryAbilitySpec;
	/** Spec that describes the special fire ability. */
	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	UPMTFWFireAbilitySpecDataAsset* SpecialAbilitySpec;
	/** Spec that describes the reload ability */
	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	UPMTFWFireAbilitySpecDataAsset* ReloadAbilitySpec;
	/** Any other abilities that are used by the firegun. */
	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	TArray<UPMTFWFireAbilitySpecDataAsset*> MiscAbilitySpec;
	
};

/** Track the amount of bullets that an instance has with a type of ammo. */
USTRUCT(BlueprintType)
struct FPMTFWCurrentAmmoForBulletType
{
	GENERATED_BODY()

public:

	FPMTFWCurrentAmmoForBulletType() {}

	FPMTFWCurrentAmmoForBulletType(FGameplayTag InTag, uint16 InAmmo) : AmmoType(InTag), BulletsInMagazine(InAmmo) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag AmmoType;
	UPROPERTY()
	uint16 BulletsInMagazine = 0;

	friend bool operator==(const FPMTFWCurrentAmmoForBulletType& Lhs, const FPMTFWCurrentAmmoForBulletType& Rhs)
	{
		return Lhs.AmmoType == Rhs.AmmoType;
	}

	friend bool operator!=(const FPMTFWCurrentAmmoForBulletType& Lhs, const FPMTFWCurrentAmmoForBulletType& Rhs)
	{
		return !(Lhs == Rhs);
	}
};

/**
 * 
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWFiregunUtilities : public UObject
{
	GENERATED_BODY()
	
};
