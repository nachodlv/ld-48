// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PMTFWFiregun.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(APMTFWFiregunLog, Log, All);

class UBoxComponent;
class UCapsuleComponent;
class UMeshComponent;
class UShapeComponent;
class USkeletalMesh;
class USkeletalMeshComponent;
class UStaticMesh;
class UStaticMeshComponent;

/** Physical representation of a firegun. Mainly to be dropped or attached to a character.
* It does not contain state. */
UCLASS(BlueprintType, Abstract)
class PMTFIREGUNWEAPONS_API APMTFWFiregun : public AActor
{
	GENERATED_BODY()
	
public:

	// ~ Begin static methods and members.

	/** Get the name of the box collision component. */
	static FName GetBoxCollisionComponentName();
	/** Get the name of the capsule collision component. */
	static FName GetCapsuleCollisionComponentName();
	/** Get the name of the skeletal mesh component. Useful to create a skeletal mesh
	* rather than a static mesh for the firegun. */
	static FName GetSkeletalMeshComponentName();
	/** Get the name of the static mesh component. Useful to create a skeletal mesh
	* rather than a static mesh for the firegun. */
	static FName GetStaticMeshComponentName();

	// ~ End static methods and members.

	/** Create the component used for the skeletal or static mesh. */
	APMTFWFiregun(const FObjectInitializer& InObjectInitializer);

	/** Set the skeletal mesh this firegun will use. */
	UFUNCTION(BlueprintCallable, Category = "PMTFWFiregun|Firegun|Visualization")
	void SetFiregunSkeletalMesh(USkeletalMesh* InSkeletalMesh);
	/** Set the static mesh this firegun will use. */
	UFUNCTION(BlueprintCallable, Category = "PMTFWFiregun|Firegun|Visualization")
	void SetFiregunStaticMesh(UStaticMesh* InStaticMesh);
	/** Retrieve the transform where the bullet should fire.
	* @param StandardBulletSocketName - Socket from the default CDO where should we get
	* the bullet start location. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	FTransform RetrieveBulletTransform(FName StandardBulletSocketName) const;

	// ~ Begin getters.

	/** Get firegun's box collision component. Can be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	UBoxComponent* GetFiregunBoxComponent() const;
	/** Get firegun's capsule collision component. Can be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	UCapsuleComponent* GetFiregunCapsuleComponent() const;
	/** Get firegun's collision component. This shouldn't be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	UShapeComponent* GetFiregunCollisionComponent() const;
	/** Get firegun's skeletal mesh component. Can be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	USkeletalMeshComponent* GetFiregunSkeletalMeshComponent() const;
	/** Get firegun's skeletal mesh component. Can be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	UStaticMeshComponent* GetFiregunStaticMeshComponent() const;
	/** Get firegun's mesh component. This won't be null. */
	UFUNCTION(BlueprintPure, Category = "PMTFWFiregun|Firegun|Getters")
	UMeshComponent* GetFiregunMeshComponent() const;

	// ~ End Getters

protected:

	void BeginPlay() override;

private:

	/** Box Collision component of the firegun. Can be null. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Collisions")
	UBoxComponent* BoxCollision = nullptr;
	/** Capsule Collision component of the firegun. Can be null. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Collisions")
	UCapsuleComponent* CapsuleCollision = nullptr;
	/** Skeletal mesh if applies. Can be null since an static
	* mesh could have been used instead. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Mesh")
	USkeletalMeshComponent* FiregunSkeletalMesh = nullptr;
	/** Static representation of the gun. Can be null if a skeletal mesh was used instead (more common to be null). */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Mesh")
	UStaticMeshComponent* FiregunStaticMesh = nullptr;

	// ~ Begin cached pointers.

	/** Cached collision component that is used to avoid casting for the collision component. Won't be null. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Collisions")
	UShapeComponent* CachedCollisionComponent = nullptr;
	/** Cached pointer to either the skeletal mesh component or the static mesh.
	* Useful to retrieve common data shared between components without checking for nullness. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "PMTFW|Firegun|Components|Mesh")
	UMeshComponent* CachedMeshComponent = nullptr;

	// ~ End cached pointers.

};
