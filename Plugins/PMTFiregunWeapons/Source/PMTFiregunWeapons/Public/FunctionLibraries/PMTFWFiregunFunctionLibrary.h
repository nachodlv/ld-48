// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWFiregunFunctionLibrary.generated.h"

class APMTFWActiveFiregunsProxy;
class APMTFWFiregun;
struct FPMTFWFiregunInstanceDataItem;


/**
 * Utility functions for anything related to fireguns (traces and such).
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWFiregunFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Make a trace and return all the hits we did along the way. */
	UFUNCTION(BlueprintPure)
	static FPMTFWFiregunTraceResults DoTrace(AActor* ActorRequestingTrace,
		APlayerController* ActorController, const FPMTFWFiregunTraceParameters& TraceParameters);
	/** Simulate a trace with the given target data. */
	UFUNCTION(BlueprintPure)
	static FPMTFWFiregunTraceResults SimulateTrace(AActor* ActorRequestingTrace,
		const FPMTFWBulletHitTargetData& TargetData, const FPMTFWFiregunTraceParameters& TraceParameters);
	/** Get the trace parameters from the given instance and physical representation. */
	UFUNCTION(BlueprintPure)
	static FPMTFWFiregunTraceParameters GetTraceParameters(const FPMTFWFiregunInstanceDataItem& FiregunInstance,
		const APMTFWActiveFiregunsProxy* ActiveProxy, const APMTFWFiregun* Firegun);
	/** Generate target data from the given trace results. */
	UFUNCTION(BlueprintPure)
	static FPMTFWBulletHitTargetData GenerateHitTargetData(const FPMTFWFiregunTraceResults& InTraceResults);
	UFUNCTION(BlueprintPure)
	static FPMTFWCurrentAmmoForBulletType CreateAmmoForFiregun(FGameplayTag InTag, int32 InCount);
	UFUNCTION(BlueprintPure)
	static TArray<FHitResult> GetHitResultsFromAbilityTargetData(const FGameplayAbilityTargetDataHandle& Data);
};
