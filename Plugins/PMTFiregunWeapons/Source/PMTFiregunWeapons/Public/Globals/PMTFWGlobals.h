// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"

// UE Includes
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"
#include "GameplayTagContainer.h"

// PMTFW Includes
#include "PMTFiregunWeapons.h"

#include "PMTFWGlobals.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(PMTFWFiregunAbilityLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(PMTFWFiregunWeaponLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(PMTFWFiregunTraceLog, Log, All);

class UPMTFWAmmunition;
class UPMTFWFiregunData;

class UObjectLibrary;

/** Enum that represents a bundle name to load data asynchronously. */
enum PMTFWDataLoadBundle
{
	PMTFWBundleNone = 0x0,
	PMTFWBundleGameplay = 0x1,
	PMTFWBundleUI = 0x2,

	PMTFWBundleAll = PMTFWBundleGameplay | PMTFWBundleUI
};

DECLARE_DELEGATE_OneParam(FPMTFWFiregunsLoadedDelegate, const TArray<const UPMTFWFiregunData*>&);

/** Holds global data for the firegun system. Can be configured per project via config file */
UCLASS(config = Game)
class PMTFIREGUNWEAPONS_API UPMTFWGlobals : public UObject
{
	GENERATED_BODY()

public:

	UPMTFWGlobals(const FObjectInitializer& ObjectInitializer);

	/** Initialize firegun weapon global data from blueprints. */
	UFUNCTION(BlueprintCallable, Category = "PMTFiregunWeapons|Globals")
	static void InitFiregunWeaponGlobalData();
	/** Retrieve the singleton globals data. */
	UFUNCTION(BlueprintPure, Category = "PMTFiregunWeapons|Globals")
	static UPMTFWGlobals* GetFiregunGlobals();

	UFUNCTION(BlueprintCallable)
	void AddAmmoClass(FGameplayTag InTag, TSubclassOf<UPMTFWAmmunition> InAmmoClass);

	/** Gets the single instance of the globals object, will create it as necessary */
	static UPMTFWGlobals& Get()
	{
		return *IPMTFiregunWeaponsModule::Get().GetFiregunWeaponsGlobals();
	}

	/** Should be called once as part of project setup to load global data.
	* Loads ammunition types. */
	virtual void InitGlobalData();

#if WITH_EDITOR

	/** Reloads the ammo asset data (called when an ammo type is destroyed or created). */
	void ReloadAmmoAssetData();

#endif // WITH_EDITOR

	void LoadFiregunAsync(const TArray<TSubclassOf<UPMTFWFiregunData>>& InFiregunClasses, FStreamableDelegate CompletionDelegate);
	void LoadFiregunAsync(const TArray<TSoftClassPtr<UPMTFWFiregunData>>& InFiregunSoftClasses, FStreamableDelegate CompletionDelegate);

	void LoadFiregunAmmunitions(const FGameplayTagContainer& AmmunitionTagContainer, FStreamableDelegate CompletionDelegate, PMTFWDataLoadBundle LoadBundles = PMTFWDataLoadBundle::PMTFWBundleAll);
	/** Get a loaded ammunition by tag. */
	UPMTFWAmmunition* GetAmmunition(const FGameplayTag& InAmmoTag);

	/** Check if we randomize data in the attachments. */
	bool IsDataRandomizationForAttachmentsEnabled() const;

	/** The class to instantiate as the globals object. Defaults to this class but can be overridden */
	UPROPERTY(Config)
	FSoftClassPath PMTFWGlobalsClassName;

	/** Paths where we should look for ammunition classes to load them up. */
	UPROPERTY(Config)
	TArray<FString> AmmunitionClassPaths;

protected:

	/** Load all ammunition classes that the project has. */
	virtual void LoadAmmunitionClasses();

	/** Whenever attachments can have a seed that randomizes the attributes. */
	UPROPERTY(Config)
	bool bEnableDataRandomizationForAttachments = false;
	/** Whenever we load fireguns asynchronously. */
	UPROPERTY(Config)
	bool bLoadFiregunSync = false;

private:

	UPROPERTY(Transient)
	TMap<FGameplayTag, TSubclassOf<UPMTFWAmmunition>> LoadedAmmunition;

	void LoadAssetsSync(const TArray<FSoftObjectPath>& SoftObjects, FStreamableDelegate DelegateToCall);
	void LoadAssetsAsync(const TArray<FSoftObjectPath>& SoftObjects, FStreamableDelegate DelegateToCall);

	/** Library that contains all the ammunition type objects in the project. */
	UPROPERTY(Transient)
	UObjectLibrary* AmmunitionObjectLibrary = nullptr;
	/** Ammunition asset data loaded */
	TArray<FAssetData> AmmunitionAssetData;

	/** Whenever we initialized the global data already. */
	bool bInitialized = false;
};
