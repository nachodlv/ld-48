// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "PMTFWFiregunEquipGE.generated.h"

/**
 * Gameplay effect for fireguns that should be applied only
 * when the firegun is the currently one being active.
 * Can be either applied to the owner of the firegun or the firegun's ASC.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWFiregunEquipGE : public UGameplayEffect
{
	GENERATED_BODY()

public:

	/** Verify if this GE should be applied to the firegun's owner. */
	bool ShouldApplyToOwner() const { return bApplyToFiregunOwner; }

protected:

	/** Whenever this gameplay effect should be applied to the firegun's owner.
	* In case it's false, then this will be applied to the firegun instead. */
	UPROPERTY(EditDefaultsOnly)
	bool bApplyToFiregunOwner = false;
	
};
