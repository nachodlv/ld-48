// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "PMTFWAmmoGameplayEffect.generated.h"

/**
 * Basic gameplay effect for ammunition. This effect is always infinite because
 * we handle automatically the application and removal of this.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWAmmoGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()

public:

	UPMTFWAmmoGameplayEffect();

protected:

#if WITH_EDITOR

	bool CanEditChange(const FProperty* InProperty) const override;

#endif // WITH_EDITOR
	
};
