// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"

// PMTGU Includes
#include "Interfaces/PMTGUAbilityNotifierInterface.h"

#include "PMTFWBaseAbility.generated.h"

/**
 * 
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWBaseAbility : public UGameplayAbility, public IPMTGUAbilityNotifierInterface
{
	GENERATED_BODY()

public:

	/** Fire delegate to notify anyone that we got removed. */
	void OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	FPMTGUAbilityRemovedDelegate& GetAbilityRemovedDelegate() override;

protected:

	FPMTGUAbilityRemovedDelegate AbilityRemovedDelegate;
	
};
