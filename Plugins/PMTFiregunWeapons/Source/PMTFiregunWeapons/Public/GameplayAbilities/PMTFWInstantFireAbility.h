// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTFWBaseFiregunAbility.h"
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWInstantFireAbility.generated.h"

class APMTFWActiveFiregunsProxy;
class APMTFWFiregun;
class UPMTGUWaitTargetData;

struct FPMTFWFiregunInstanceDataItem;

/**
 * Local predicted ability that shoots a bullet.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWInstantFireAbility : public UPMTFWBaseFiregunAbility
{
	GENERATED_BODY()

public:

	UPMTFWInstantFireAbility();

	/** Start the bullet procedure. */
	void FireBullet();

	/** Expected to be called only by the Local version of the fire ability.*/
	void ExternalEndAbility();

protected:

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* OwnerInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

	UFUNCTION()
	void OnValidTargetData(const FGameplayAbilityTargetDataHandle& Data);

	/** Generate trace parameters to shoot a bullet. */
	FPMTFWFiregunTraceParameters GenerateTraceParameters(const FPMTFWFiregunInstanceDataItem& Instance,
		const APMTFWActiveFiregunsProxy* ActiveProxy, APMTFWFiregun* Firegun);

	/** Whenever this ability fires a projectile (fire & forget) or is a trace (results are obtained in the same frame) */
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior")
	EPMTFWProjectileType ProjectileType = EPMTFWProjectileType::Trace;
	/** Profile used for traces. */
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior")
	FCollisionProfileName ProfileName;
	/** Whenever we make complex traces. */
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior|Trace")
	bool bComplexTrace = false;
	/** Whenever we ignore blocking hits. */
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior|Trace")
	bool bIgnoreBlockingHits = false;
	/** Whenever we debug the line traces. */
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior|Trace")
	bool bDebug = false;
	/** Task that waits for client data in server. */
	UPROPERTY(Transient)
	UPMTGUWaitTargetData* WaitTargetDataTask = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior|Trace")
	FGameplayTag ImpactCue;
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Behavior|Trace")
	FGameplayTag MuzzleCue;
};
