// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTFWBaseAbility.h"

// PMTFW Includes
#include "Firegun/PMTFWFiregunUtilities.h"

#include "PMTFWBaseFiregunAbility.generated.h"

class IPMTFWFiregunProvider;
class UPMTFWInstantFireAbility;

class UAbilityTask_WaitDelay;
class UAbilityTask_WaitInputRelease;
class UAbilityTask_WaitInputPress;

struct FPMTFWLastTimeFirePerHandle
{
	FPMTFWLastTimeFirePerHandle() {}
	FPMTFWLastTimeFirePerHandle(FPMTFWFiregunInstanceHandle InHandle) : FiregunHandle(InHandle), LastTimeFire(0.0f) {}

	FPMTFWFiregunInstanceHandle FiregunHandle;
	float LastTimeFire = 0.0;
};

/**
 * Base ability for anything related to firing bullets, either projectiles or traces.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWBaseFiregunAbility : public UPMTFWBaseAbility
{
	GENERATED_BODY()

public:

	bool CheckCooldown(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	/** Applies CooldownGameplayEffect to the target */
	void ApplyCooldown(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo) const override;

	/** Checks cost. returns true if we can pay for the ability. False if not */
	bool CheckCost(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	/** Applies the ability's cost to the target */
	void ApplyCost(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo) const override;

protected:

	/** Wait a given time to fire another bullet. */
	void InitRateOfFireDelay(float TimeToWait);
	/** Clear rate of fire delay task. */
	void ClearRateOfFireDelay();
	/** Initialize a wait input release task. */
	void InitWaitInputReleaseTask();
	/** Clear wait input release delay task. */
	void ClearWaitInputReleaseTask();
	/** Initialize a wait input press task. */
	void InitWaitInputPressedTask();
	/** Clear wait input press delay task. */
	void ClearWaitInputPressedTask();

	/** Make sure to nullout the task created. */
	UFUNCTION()
	virtual void OnRateOfFireDelayFinished();
	/** Make sure to nullout the task created. */
	UFUNCTION()
	virtual void OnInputReleased(float TimeHeld);
	/** Make sure to nullout the task created. */
	UFUNCTION()
	virtual void OnInputPressed(float TimeWaited);

	/** Task created to wait for the firegun's fire rate. */
	UPROPERTY(Transient)
	UAbilityTask_WaitDelay* WaitDelayRateOfFireTask = nullptr;
	/** Task created to stop firing for non semi-automatic weapons. */
	UPROPERTY(Transient)
	UAbilityTask_WaitInputRelease* WaitInputReleaseTask = nullptr;
	/** Task created to keep firing for semi-automatic weapons. */
	UPROPERTY(Transient)
	UAbilityTask_WaitInputPress* WaitInputPressTask = nullptr;
	/** Type of ammo this ability uses. */
	UPROPERTY(EditDefaultsOnly)
	FGameplayTag AmmoType;

private:

	/** Verify if enough time since last time has passed to fire a bullet. */
	bool CanFire(const FGameplayAbilityActorInfo* ActorInfo) const;
	/** Verify if we have any bullet to fire. */
	bool HasBullets(const FGameplayAbilityActorInfo* ActorInfo) const;
	/** Retrieve the last time a firegun shoot. */
	float GetLastTimeFire(const IPMTFWFiregunProvider& Provider) const;

	/** Clear the handles that were unused for a while. */
	void ClearOldFiregunHandles();
	/** Find the associated last time fire with the given handle. If not found, then it adds it and returns it.  */
	FPMTFWLastTimeFirePerHandle& FindOrAddWeaponHandle(FPMTFWFiregunInstanceHandle InHandle) const;

	/** Last time fired per weapon. Useful to keep track of shared abilities with different gun instances. */
	mutable TArray<FPMTFWLastTimeFirePerHandle> SharedWeaponHandles;
	
};
