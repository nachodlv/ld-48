// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTFWBaseFiregunAbility.h"

#include "PMTFWLocalFireAbility.generated.h"

class UPMTFWInstantFireAbility;

/**
 * Ability used to fire any firegun.
 */
UCLASS()
class PMTFIREGUNWEAPONS_API UPMTFWLocalFireAbility : public UPMTFWBaseFiregunAbility
{
	GENERATED_BODY()

public:

	UPMTFWLocalFireAbility();

protected:

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;


	/** Skip bullet cost. */
	void ApplyCost(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo) const override;

	/** Fire another bullet from Instant Ability. */
	void OnRateOfFireDelayFinished() override;
	/** Kill instant ability and this ability too. */
	void OnInputReleased(float TimeHeld) override;

	/** Ability that will be used to fire from weapon. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UPMTFWInstantFireAbility> InstantFireAbilityClass;

private:

	/** Cached pointer to the instant fire ability. */
	UPROPERTY(Transient)
	UPMTFWInstantFireAbility* InstantFireAbility = nullptr;
	/** Ability handle used for shooting a single bullet from the
	* weapon's magazine. */
	FGameplayAbilitySpecHandle InstantFireAbilityHandle;
	
};
