// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionLibraries/PMTCAttributeTagFuncLibrary.h"

bool UPMTCAttributeTagFuncLibrary::SetTagAttributeCurrentValueInArray(FPMTCTagAttributeArray& TagAttributeArray,
	FGameplayTag AttributeTag, float InValue, float MinClamp)
{
	for (FPMTCTagAttribute& TagAttribute : TagAttributeArray.Attributes)
	{
		if (TagAttribute.Tag == AttributeTag)
		{
			const bool bModified = UPMTCAttributeTagFuncLibrary::SetTagAttributeCurrentValue(TagAttribute, InValue, MinClamp);
			if (bModified)
			{
				TagAttributeArray.MarkItemDirty(TagAttribute);
			}
			return bModified;
		}
	}
	return false;
}

bool UPMTCAttributeTagFuncLibrary::SetTagAttributeCurrentValue(FPMTCTagAttribute& TagAttribute, float InValue, float MinClamp)
{
	return TagAttribute.SetCurrentValue(InValue, MinClamp);
}

bool UPMTCAttributeTagFuncLibrary::SetTagAttributeInitialValueInArray(FPMTCTagAttributeArray& TagAttributeArray, FGameplayTag AttributeTag, float InValue)
{
	for (FPMTCTagAttribute& TagAttribute : TagAttributeArray.Attributes)
	{
		if (TagAttribute.Tag == AttributeTag)
		{
			const bool bModified = UPMTCAttributeTagFuncLibrary::SetTagAttributeInitialValue(TagAttribute, InValue);
			if (bModified)
			{
				TagAttributeArray.MarkItemDirty(TagAttribute);
			}
			return bModified;
		}
	}
	return false;
}

bool UPMTCAttributeTagFuncLibrary::SetTagAttributeInitialValue(FPMTCTagAttribute& TagAttribute, float InValue)
{
	return TagAttribute.SetBaseValueAndRecalculate(InValue);
}

bool UPMTCAttributeTagFuncLibrary::AddModifierToTagAttribute(FPMTCTagAttribute& TagAttribute,
	TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue)
{
	check(InModifierClass);
	const UPMTCTagAttributeModifier* const ModifierCDO = InModifierClass.GetDefaultObject();
	for (FPMTCSimpleAttributeModifier& Modifier : TagAttribute.Modifiers)
	{
		if (Modifier.ModifierClass == InModifierClass)
		{
			if (Modifier.CurrentStacks == ModifierCDO->GetStackLimit())
			{
				return false;
			}
			else
			{
				Modifier.CurrentStacks += 1;
				return true;
			}
		}
	}
	FPMTCSimpleAttributeModifier Modifier;
	Modifier.CurrentStacks = 1;
	Modifier.ModifierClass = InModifierClass;
	Modifier.Value = InValue == 0.0f ? ModifierCDO->GetModifierValue() : InValue;
	TagAttribute.Modifiers.Emplace(Modifier);
	return true;
}

bool UPMTCAttributeTagFuncLibrary::AddModifierToTagAttributeInArray(FPMTCTagAttributeArray& TagAttributeArray,
	FGameplayTag AttributeTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue)
{
	check(InModifierClass);
	for (FPMTCTagAttribute& TagAttribute : TagAttributeArray.Attributes)
	{
		if (TagAttribute.Tag == AttributeTag)
		{
			const bool bModified = AddModifierToTagAttribute(TagAttribute, InModifierClass, InValue);
			if (bModified)
			{
				TagAttributeArray.MarkItemDirty(TagAttribute);
			}
			return bModified;
		}
	}
	return false;
}

bool UPMTCAttributeTagFuncLibrary::RemoveModifierFromTagAttribute(FPMTCTagAttribute& TagAttribute,
	TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove)
{
	constexpr int32 AllStacks = -1;
	int32 Index = 0;
	const UPMTCTagAttributeModifier* const ModifierCDO = InModifierClass.GetDefaultObject();
	for (FPMTCSimpleAttributeModifier& Modifier : TagAttribute.Modifiers)
	{
		if (Modifier.ModifierClass == InModifierClass)
		{
			if (StacksToRemove == AllStacks)
			{
				TagAttribute.Modifiers.RemoveAtSwap(Index);
				return true;
			}
			else
			{
				Modifier.CurrentStacks -= StacksToRemove;
				const bool bRemoved = Modifier.CurrentStacks < 1;
				if (bRemoved)
				{
					TagAttribute.Modifiers.RemoveAtSwap(Index);
				}
				return bRemoved;
			}
		}
		++Index;
	}
	return false;
}

bool UPMTCAttributeTagFuncLibrary::RemoveModifierFromTagAttributeInArray(
	FPMTCTagAttributeArray& TagAttributeArray,
	FGameplayTag AttributeTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove)
{
	check(InModifierClass);
	for (FPMTCTagAttribute& TagAttribute : TagAttributeArray.Attributes)
	{
		if (TagAttribute.Tag == AttributeTag)
		{
			const bool bModified = RemoveModifierFromTagAttribute(TagAttribute, InModifierClass);
			if (bModified)
			{
				TagAttributeArray.MarkItemDirty(TagAttribute);
			}
			return bModified;
		}
	}
	return false;
}
