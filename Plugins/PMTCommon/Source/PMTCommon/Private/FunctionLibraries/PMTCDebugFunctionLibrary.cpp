// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionLibraries/PMTCDebugFunctionLibrary.h"

// UE Includes
#include "DrawDebugHelpers.h"
#include "Engine/CollisionProfile.h"
#include "GameplayTagContainer.h"
#include "Kismet/KismetSystemLibrary.h"

namespace PMTCommonHiddenClasses
{
	namespace
	{
		constexpr const char* const PrefixString = "+";
		constexpr const char* const ExpandedString = "-";
		/** Space used per text (tested in editor, there's no 'good way' to find a value for this.) */
		constexpr float ZLocationPerText = -10.0f;

		FString GetTabString(int32 TabSize)
		{
			FString Spaces;
			Spaces.Reserve(TabSize);
			for (int32 i = 0; i < TabSize; ++i)
			{
				Spaces += TEXT(" ");
			}
			return Spaces;
		}

		FString GetTextToDisplay(int32 TabSize, const FString& PlainText, bool bExpanded = false)
		{
			const FString Prefix = !bExpanded ? FString(ANSI_TO_TCHAR(PrefixString)) : FString(ANSI_TO_TCHAR(ExpandedString));
			const FString TabString = GetTabString(TabSize);
			return TabString + Prefix + PlainText;
		}

		void ToggleBoolean(UObject* Object, FName PropertyName)
		{
			FBoolProperty* DebugActiveProperty = CastField<FBoolProperty>(Object->GetClass()->FindPropertyByName(PropertyName));
			if (DebugActiveProperty)
			{
				const bool bPropertyValue = DebugActiveProperty->GetPropertyValue_InContainer(Object, 0);
				DebugActiveProperty->SetPropertyValue_InContainer(Object, !bPropertyValue);
			}
		}

		FName GetCategoryPropertyName(FName CategoryName)
		{
			const FString CategoryAsString = CategoryName.ToString();
			return FName(FString(TEXT("bDebug")) + CategoryAsString);
		}

		bool GetDebugCategoryValue(UObject* Object, FName CategoryName)
		{
			const FName PropertyName = GetCategoryPropertyName(CategoryName);
			FBoolProperty* DebugActiveProperty = CastField<FBoolProperty>(Object->GetClass()->FindPropertyByName(PropertyName));
			if (DebugActiveProperty)
			{
				return DebugActiveProperty->GetPropertyValue_InContainer(Object, 0);
			}
			return false;
		}

		FLinearColor GetStaticDebuggingColor()
		{
			return FLinearColor::Green;
		}

		FLinearColor GetActiveDebuggingColor()
		{
			return FLinearColor::Red;
		}

		const FName NAME_DebugCategoryMetaTag(TEXT("PMTCDebugCategory"));
		const FName NAME_DebugDebugToggleByMetaTag(TEXT("PMTCDebugToggleBy"));
	}

	FString GetBoolAsString(bool Value)
	{
		return Value ? FString(TEXT("true")) : FString(TEXT("false"));
	}

	FString GetCollisionProfileName(FCollisionProfileName CollisionProfile)
	{
		return CollisionProfile.Name.IsNone() ? FString(TEXT("No collision profile specified")) : CollisionProfile.Name.ToString();
	}

	FVector DebugProperty(FProperty* Property, UObject* InWorldContext, void* Container, const FVector& TextLocation,
		TMap<FString, bool>& ActiveCategories,
		TMap<FString, bool>& ToggledBooleans,
		class AActor* TestBaseActor = nullptr,
		float Duration = 0.f, int32 TabSize = 0, bool bExpanded = false,
		FLinearColor TextColor = FLinearColor::White, int32 ArrayIndex = 0);

	FVector DebugStructProperty(FStructProperty* StructProperty, UObject* InWorldContext, void* Container, const FVector& TextLocation,
		TMap<FString, bool>& ActiveCategories,
		TMap<FString, bool>& ToggledBooleans,
		class AActor* TestBaseActor = nullptr,
		float Duration = 0.f, int32 TabSize = 0, bool bExpanded = false,
		FLinearColor TextColor = FLinearColor::White,
		int32 InArrayIndex = 0)
	{
		const FString PropertyName = StructProperty->GetNameCPP() + ": ";
		FVector NewLocation = TextLocation;
		if (StructProperty->Struct->IsChildOf(FGameplayTagContainer::StaticStruct()))
		{
			FGameplayTagContainer* TagContainerDataPtr = StructProperty->ContainerPtrToValuePtr<FGameplayTagContainer>(Container, InArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugTagContainer(InWorldContext, TextLocation, PropertyName, *TagContainerDataPtr, TestBaseActor, Duration, TextColor);
		}
		else if (StructProperty->Struct->IsChildOf(FGameplayAttributeData::StaticStruct()))
		{
			FGameplayAttributeData* AttributeDataPtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(Container, InArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugGameplayAttribute(InWorldContext, TextLocation, PropertyName, *AttributeDataPtr, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (StructProperty->Struct->IsChildOf(FGameplayTag::StaticStruct()))
		{
			FGameplayTag* TagDataPtr = StructProperty->ContainerPtrToValuePtr<FGameplayTag>(Container, InArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + TagDataPtr->GetTagName().ToString(), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (StructProperty->Struct->IsChildOf(FCollisionProfileName::StaticStruct()))
		{
			FCollisionProfileName* ProfileDataPtr = StructProperty->ContainerPtrToValuePtr<FCollisionProfileName>(Container, InArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + GetCollisionProfileName(*ProfileDataPtr), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else
		{
			NewLocation = UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, TEXT("Struct ") + PropertyName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			void* StructContainer = StructProperty->ContainerPtrToValuePtr<void>(Container, InArrayIndex);
			for (TFieldIterator<FProperty> It(StructProperty->Struct); It; ++It)
			{
				NewLocation = DebugProperty(*It, InWorldContext, StructContainer, NewLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize + 4, true, TextColor, InArrayIndex);
			}
		}
		return NewLocation;
	}

	FVector DebugProperty(FProperty* Property, UObject* InWorldContext, void* Container, const FVector& TextLocation,
		TMap<FString, bool>& ActiveCategories,
		TMap<FString, bool>& ToggledBooleans,
		class AActor* TestBaseActor,
		float Duration, int32 TabSize, bool bExpanded,
		FLinearColor TextColor, int32 ArrayIndex)
	{
		if (!Property)
		{
			return TextLocation;
		}
		const FString PropertyName = Property->GetNameCPP() + ": ";
		FVector NewLocation = TextLocation;
		if (FBoolProperty* BoolProperty = CastField<FBoolProperty>(Property))
		{
			const bool Value = BoolProperty->GetPropertyValue_InContainer(Container, ArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + GetBoolAsString(Value), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FIntProperty* IntProperty = CastField<FIntProperty>(Property))
		{
			const int32 Value = IntProperty->GetPropertyValue_InContainer(Container, ArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString::FromInt(Value), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FFloatProperty* FloatProperty = CastField<FFloatProperty>(Property))
		{
			const float Value = FloatProperty->GetPropertyValue_InContainer(Container, ArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString::SanitizeFloat(Value, 5), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FObjectProperty* ObjectProperty = CastField<FObjectProperty>(Property))
		{
			const UObject* Value = ObjectProperty->GetPropertyValue_InContainer(Container, ArrayIndex);
			const FString ObjectName = Value ? *Value->GetName() : FString("Null");
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + ObjectName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FEnumProperty* EnumProperty = CastField<FEnumProperty>(Property))
		{
			const FString DisplayValue = EnumProperty->GetEnum()->GetNameStringByValue(EnumProperty->GetUnderlyingProperty()->GetSignedIntPropertyValue(EnumProperty->ContainerPtrToValuePtr<void>(Container, ArrayIndex)));
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + DisplayValue, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FByteProperty* ByteProperty = CastField<FByteProperty>(Property))
		{
			const uint8 Value = ByteProperty->GetPropertyValue_InContainer(Container, ArrayIndex);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString::FromInt(Value), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FNameProperty* NameProperty = CastField<FNameProperty>(Property))
		{
			const FName Value = NameProperty->GetPropertyValue(Container);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + Value.ToString(), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FStrProperty* StringProperty = CastField<FStrProperty>(Property))
		{
			const FString Value = StringProperty->GetPropertyValue(Container);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + Value, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FTextProperty* TextProperty = CastField<FTextProperty>(Property))
		{
			const FText Value = TextProperty->GetPropertyValue(Container);
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + Value.ToString(), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FStructProperty* StructProperty = CastField<FStructProperty>(Property))
		{
			return DebugStructProperty(StructProperty, InWorldContext, Container, TextLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize, bExpanded, TextColor, ArrayIndex);
		}
		else if (FInterfaceProperty* InterfaceProperty = CastField<FInterfaceProperty>(Property))
		{
			const UObject* Value = InterfaceProperty->GetPropertyValue(Container).GetObject();
			const FString ObjectName = Value ? *Value->GetName() : FString("Null");
			return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + ObjectName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
		}
		else if (FArrayProperty* ArrayProperty = CastField<FArrayProperty>(Property))
		{
			FScriptArrayHelper_InContainer ArrayHelper(ArrayProperty, Container);
			const int32 ArrayNum = ArrayHelper.Num();
			if (ArrayNum == 0)
			{
				return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString(TEXT("Empty Array")), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			}
			else
			{
				for (int32 ItArrayIndex = 0; ItArrayIndex < ArrayNum; ++ItArrayIndex)
				{
					NewLocation = DebugProperty(ArrayProperty->Inner, InWorldContext, ArrayHelper.GetRawPtr(ItArrayIndex), NewLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize + 2, true, FLinearColor::White, ItArrayIndex);
				}
			}
		}
		else if (FMapProperty* MapProperty = CastField<FMapProperty>(Property))
		{
			FScriptMapHelper_InContainer MapHelper(MapProperty, Container, ArrayIndex);
			const int32 MapHelperCount = MapHelper.Num();
			if (MapHelperCount == 0)
			{
				return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString(TEXT("Empty TMap")), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			}
			NewLocation = UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, FString(TEXT("TMap Extension of ")) + PropertyName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			for (int32 ItMapCount = 0; ItMapCount < MapHelperCount; ++ItMapCount)
			{
				FProperty* KeyProperty = MapHelper.GetKeyProperty();
				NewLocation = DebugProperty(KeyProperty, InWorldContext, MapHelper.GetKeyPtr(ItMapCount), NewLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize + 2, true, TextColor, ArrayIndex);
				FProperty* ValueProperty = MapHelper.GetValueProperty();
				NewLocation = DebugProperty(ValueProperty, InWorldContext, MapHelper.GetValuePtr(ItMapCount), NewLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize + 2, true, TextColor, ArrayIndex);
			}
		}
		else if (FSetProperty* SetProperty = CastField<FSetProperty>(Property))
		{
			FScriptSetHelper_InContainer SetHelper(SetProperty, Container, ArrayIndex);
			const int32 SetHelperCount = SetHelper.Num();
			if (SetHelperCount == 0)
			{
				return UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, PropertyName + FString(TEXT("Empty TSet")), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			}
			NewLocation = UPMTCDebugFunctionLibrary::DrawDebugString(InWorldContext, TextLocation, FString(TEXT("TSet Extension of ")) + PropertyName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
			for (int32 ItSetCount = 0; ItSetCount < SetHelperCount; ++ItSetCount)
			{
				FProperty* ElementProperty = SetHelper.GetElementProperty();
				NewLocation = DebugProperty(ElementProperty, InWorldContext, SetHelper.GetElementPtr(ItSetCount), NewLocation, ActiveCategories, ToggledBooleans, TestBaseActor, Duration, TabSize + 2, true, TextColor, ArrayIndex);
			}
		}
		// Unsupported types.
		return NewLocation;
	}
}

FVector UPMTCDebugFunctionLibrary::DrawDebugString(UObject* WorldContextObject, const FVector& TextLocation,
	const FString& Text, class AActor* TestBaseActor,
	float Duration, int32 TabSize, bool bExpanded, FLinearColor TextColor)
{
#if ENABLE_DRAW_DEBUG
	UKismetSystemLibrary::DrawDebugString(WorldContextObject, TextLocation, PMTCommonHiddenClasses::GetTextToDisplay(TabSize, Text, bExpanded), TestBaseActor, TextColor, Duration);
	return TextLocation + FVector(0.0f, 0.0f, PMTCommonHiddenClasses::ZLocationPerText);
#else
	return FVector::ZeroVector;
#endif // ENABLE_DRAW_DEBUG
}

FVector UPMTCDebugFunctionLibrary::DrawDebugTagContainer(UObject* WorldContextObject, const FVector& TextLocation,
	const FString& TagContainerName, const FGameplayTagContainer& TagContainer, class AActor* TestBaseActor,
	float Duration, FLinearColor TextColor)
{
#if ENABLE_DRAW_DEBUG
	FString NewTagText = TagContainerName;
	if (TagContainer.Num() == 0)
	{
		NewTagText += "No tags in container";
	}
	FVector NewLocation = DrawDebugString(WorldContextObject, TextLocation, NewTagText, TestBaseActor, Duration, 0, false, TextColor);
	for (const FGameplayTag& SingleTag : TagContainer)
	{
		constexpr bool bExpanded = true;
		constexpr int32 TabSize = 4;
		NewLocation = DrawDebugString(WorldContextObject, NewLocation, SingleTag.ToString(), TestBaseActor, Duration, TabSize, bExpanded, TextColor);
	}
	return NewLocation;
#else
	return FVector::ZeroVector;
#endif // ENABLE_DRAW_DEBUG
}

FVector UPMTCDebugFunctionLibrary::DrawDebugGameplayAttribute(UObject* WorldContextObject, const FVector& TextLocation,
	const FString& AttributeName, const FGameplayAttributeData& AttributeData, class AActor* TestBaseActor,
	float Duration, int32 TabSize, bool bExpanded, FLinearColor TextColor)
{
#if ENABLE_DRAW_DEBUG
	FVector NewLocation = DrawDebugString(WorldContextObject, TextLocation, AttributeName, TestBaseActor, Duration, TabSize, bExpanded, TextColor);
	const FString BaseValueName(TEXT("Base Value: "));
	const FString CurrentValueName(TEXT("Current Value: "));
	NewLocation = DrawDebugString(WorldContextObject, NewLocation, BaseValueName + FString::SanitizeFloat(AttributeData.GetBaseValue(), 5), TestBaseActor, Duration, TabSize + 2, true, TextColor);
	NewLocation = DrawDebugString(WorldContextObject, NewLocation, CurrentValueName + FString::SanitizeFloat(AttributeData.GetCurrentValue(), 5), TestBaseActor, Duration, TabSize + 2, true, TextColor);
	return NewLocation;
#else
	return FVector::ZeroVector;
#endif // ENABLE_DRAW_DEBUG
}

void UPMTCDebugFunctionLibrary::ToggleDebugCategory(UObject* ObjectToDebug, const FString& CategoryName)
{
	if (!ObjectToDebug)
	{
		return;
	}
	PMTCommonHiddenClasses::ToggleBoolean(ObjectToDebug, PMTCommonHiddenClasses::GetCategoryPropertyName(FName(*CategoryName)));
}

void UPMTCDebugFunctionLibrary::ToggleDebugCategories(UObject* ObjectToDebug, const TArray<FString>& CategoryNames)
{
	for (const FString& CategoryName : CategoryNames)
	{
		ToggleDebugCategory(ObjectToDebug, CategoryName);
	}
}

FVector UPMTCDebugFunctionLibrary::DebugStep(UObject* ObjectToDebug, float Duration, const FVector& InTextLocation)
{
	FVector TextLocation = InTextLocation;
#if WITH_EDITOR
	void* ContainerAsVoid = (void*)ObjectToDebug;
	check(ObjectToDebug);
	AActor* ThisAsActor = Cast<AActor>(ObjectToDebug);
	if (!ThisAsActor)
	{
		UActorComponent* Component = Cast<UActorComponent>(ObjectToDebug);
		if (Component)
		{
			ThisAsActor = Component->GetOwner();
		}
	}
	TMap<FString, bool> ActiveCategories;
	TMap<FString, bool> ToggledBooleans;
	for (TFieldIterator<FProperty> It(ObjectToDebug->GetClass()); It; ++It)
	{
		bool bAlreadyDebugged = false;
		if (It->HasMetaData(PMTCommonHiddenClasses::NAME_DebugCategoryMetaTag))
		{
			const FString& DebugCategory = It->GetMetaData(PMTCommonHiddenClasses::NAME_DebugCategoryMetaTag);
			if (!ActiveCategories.Contains(DebugCategory))
			{
				FProperty* CategoryProperty = ObjectToDebug->GetClass()->FindPropertyByName(PMTCommonHiddenClasses::GetCategoryPropertyName(FName(*DebugCategory)));
				FBoolProperty* BoolProperty = CastFieldChecked<FBoolProperty>(CategoryProperty);
				ActiveCategories.Add(DebugCategory, BoolProperty->GetPropertyValue_InContainer(ContainerAsVoid, 0));
			}
			const bool bValue = ActiveCategories[DebugCategory];
			if (bValue)
			{
				TextLocation = PMTCommonHiddenClasses::DebugProperty(*It, ObjectToDebug, ContainerAsVoid, TextLocation, ActiveCategories, ToggledBooleans, ThisAsActor, Duration, 0, false, FLinearColor::White);
				bAlreadyDebugged = true;
			}
		}

		if (!bAlreadyDebugged && It->HasMetaData(PMTCommonHiddenClasses::NAME_DebugDebugToggleByMetaTag))
		{
			const FString& PropertyToggleName = It->GetMetaData(PMTCommonHiddenClasses::NAME_DebugDebugToggleByMetaTag);
			if (!ToggledBooleans.Contains(PropertyToggleName))
			{
				FProperty* CategoryProperty = ObjectToDebug->GetClass()->FindPropertyByName(FName(*PropertyToggleName));
				FBoolProperty* BoolProperty = CastFieldChecked<FBoolProperty>(CategoryProperty);
				ToggledBooleans.Add(PropertyToggleName, BoolProperty->GetPropertyValue_InContainer(ContainerAsVoid, 0));
			}
			const bool bValue = ToggledBooleans[PropertyToggleName];
			if (bValue)
			{
				TextLocation = PMTCommonHiddenClasses::DebugProperty(*It, ObjectToDebug, ContainerAsVoid, TextLocation, ActiveCategories, ToggledBooleans, ThisAsActor, Duration, 0, false, FLinearColor::White);
				bAlreadyDebugged = true;
			}
		}
	}
	return TextLocation;
#else
	return TextLocation;
#endif // WITH_EDITOR
	
}
