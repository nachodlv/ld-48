// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

// UE Includes
#include "AttributeSet.h"
#include "GameplayTagContainer.h"

#include "PMTCDebugFunctionLibrary.generated.h"

/** Simple macro to wrap code that shows debugging stuff. */
#define PMTC_DEBUG_IF_TRUE(Condition, ReturnName, Function, ...) \
if (Condition) \
{\
	ReturnName = Function(__VA_ARGS__); \
}

#define PMTC_GET_OBJECT_NAME(Object) \
(Object ? Object->GetName() : FString("None"))

#define PMTC_GET_ENUM_NAME(EnumValue) \
UEnum::GetValueAsString(EnumValue)

struct FGameplayAttributeData;

/**
 * Utility functions for debugging.
 */
UCLASS()
class PMTCOMMON_API UPMTCDebugFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Draw a string in 3D space and return the next location from where more text can be drawn. */
	UFUNCTION(BlueprintCallable, Category = "PMTCommon|Debug", meta = (WorldContext = "WorldContextObject", DevelopmentOnly))
	static FVector DrawDebugString(UObject* WorldContextObject, const FVector& TextLocation,
		const FString& Text, class AActor* TestBaseActor = nullptr,
		float Duration = 0.f, int32 TabSize = 0, bool bExpanded = false, FLinearColor TextColor = FLinearColor::White);

	/** Draw a container of tags and return the next location from where more text can be drawn. */
	UFUNCTION(BlueprintCallable, Category = "PMTCommon|Debug", meta = (WorldContext = "WorldContextObject", DevelopmentOnly))
	static FVector DrawDebugTagContainer(UObject* WorldContextObject, const FVector& TextLocation,
		const FString& TagContainerName, const FGameplayTagContainer& TagContainer, class AActor* TestBaseActor = nullptr,
		float Duration = 0.f, FLinearColor TextColor = FLinearColor::White);

	/** Draw an attribute and return the next location from where more text can be drawn. */
	UFUNCTION(BlueprintCallable, Category = "PMTCommon|Debug", meta = (WorldContext = "WorldContextObject", DevelopmentOnly))
	static FVector DrawDebugGameplayAttribute(UObject* WorldContextObject, const FVector& TextLocation,
		const FString& AttributeName, const FGameplayAttributeData& AttributeData, class AActor* TestBaseActor = nullptr,
		float Duration = 0.f, int32 TabSize = 0, bool bExpanded = false, FLinearColor TextColor = FLinearColor::White);
	
	/** Toggle the boolean that enables a category for the given object. */
	UFUNCTION(BlueprintCallable, meta = (DevelopmentOnly), Category = "PMTCommon|Debugging")
	static void ToggleDebugCategory(UObject* ObjectToDebug, const FString& CategoryName);
	/** Toggle the boolean that enables a category for the given object. */
	UFUNCTION(BlueprintCallable, meta = (DevelopmentOnly), Category = "PMTCommon|Debugging")
	static void ToggleDebugCategories(UObject* ObjectToDebug, const TArray<FString>& CategoryNames);
	/** Does a debug step and return the last location that can be used for more debugging. */
	UFUNCTION(BlueprintCallable, meta = (DevelopmentOnly), Category = "PMTCommon|Debugging")
	static FVector DebugStep(UObject* ObjectToDebug, float Duration, const FVector& InTextLocation = FVector::ZeroVector);
};
