// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PMTCUtilityFunctionLibrary.generated.h"

/**
 * Set of common functions used by many games.
 */
UCLASS()
class PMTCOMMON_API UPMTCUtilityFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Find unique hits to actors. Remove all hits that hit the same actor but different components.
	* This chooses randomly which component is hit. */
	UFUNCTION(BlueprintPure, Category = "PMTCommon|Utilities|Collisions")
	static TArray<FHitResult> RemoveDuplicateComponentHits(const TArray<FHitResult>& InHits);
	/** From the given hits, remove any other hits if both contain the same actor. */
	UFUNCTION(BlueprintPure, Category = "PMTCommon|Utilities|Collisions")
	static TArray<FHitResult> RemoveDuplicateFromExistentHits(const TArray<FHitResult>& InHits, const TArray<FHitResult>& ExistentHits);
	/** Remove hits by interface. */
	UFUNCTION(BlueprintPure, Category = "PMTCommon|Utilities|Collisions")
	static TArray<FHitResult> FilterHitsByInterface(const TArray<FHitResult>& InHits, TSubclassOf<UInterface> Interface);
	
};
