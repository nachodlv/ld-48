// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "GameplayTagContainer.h"
#include "UObject/Interface.h"

#include "PMTCCommonTypes.generated.h"

class UPMTCTagAttributeModifier;

/** Operation we apply (like GE do).
* The order of these are applied like the following:
* ((BaseValue + Additive) * Multiplicative) / Divide */
UENUM(BlueprintType)
enum class EPMTCOperationType : uint8
{
	Additive = 0,
	Multiplicative = 1,
	Divide = 2
};

/** Simple modifier we apply to a tag attribute. */
USTRUCT(BlueprintType)
struct FPMTCSimpleAttributeModifier
{
	GENERATED_BODY()

public:

	/** Value of the modifier. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Value = 0.0f;
	/** Modifier's class. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UPMTCTagAttributeModifier> ModifierClass = nullptr;
	/** Current modifier stacks. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 CurrentStacks = 0;
};

struct FPMTCTagAttributeArray;

/** Simple structure that binds a tag to a value with modifiers. 
* Tries to simulate the idea of GE with attribute sets but in a simpler way
* to avoid having the whole ability system component requirement.
* The functionality of this attribute is to have a base value that works as a "maximum"
* while the current value acts as the "state". */
USTRUCT(BlueprintType)
struct FPMTCTagAttribute : public FFastArraySerializerItem
{
	GENERATED_BODY()

public:

	FPMTCTagAttribute() {}

	FPMTCTagAttribute(FGameplayTag InTag, int32 InValue) : Tag(InTag), BaseValue(InValue), CalculatedValue(InValue) {}

	/** Set the base value and recalculate the final value.
	* @returns True if the value was modified, false otherwise. */
	bool SetBaseValueAndRecalculate(float InBaseValue);
	/** Modifies current value and clamps it.
	* @returns True if the value has changed, false otherwise. */
	bool SetCurrentValue(float InCurrentValue, float MinClamp = TNumericLimits<float>::Min());

	float GetAttributeValue() const { return CurrentValue; }
	float GetAttributeMaxValue() const { return CalculatedValue; }

	// ~ Begin FFastArraySerializerItem

	/** Cached the calculated value. */
	void PostReplicatedAdd(const FPMTCTagAttributeArray& InArraySerializer);
	/** Recalculate the total value. */
	void PostReplicatedChange(const FPMTCTagAttributeArray& InArraySerializer);

	// ~ End FFastArraySerializerItem

	/** Tag of the attribute. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag Tag;
	/** Base Value that corresponds to the tag. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseValue = 0.0f;
	/** Current value that corresponds to the tag. This is the one that will be modified. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentValue = 0.0f;
	/** Modifiers that are applied to this attribute. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FPMTCSimpleAttributeModifier> Modifiers;
	/** Cached value to avoid calculating with modifiers all the time. Acts as the max value permitted
	* for the attribute. */
	UPROPERTY(NotReplicated, Transient, BlueprintReadOnly)
	float CalculatedValue = 0.0f;

protected:

	/** Recalculate's attribute value by reapplying the modifiers. */
	void RecalculateValue();
};

class IPMTCTagAttributeHandlerInterface;

/** Special array for containing an array of attributes with efficient
* data replication. */
USTRUCT(BlueprintType)
struct FPMTCTagAttributeArray : public FFastArraySerializer
{
	GENERATED_BODY()

	/** Array of tag attributes attributes. */
	UPROPERTY()
	TArray<FPMTCTagAttribute> Attributes;
	/** Owner of the attributes. */
	UPROPERTY(NotReplicated)
	TScriptInterface<IPMTCTagAttributeHandlerInterface> Owner = nullptr;

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FPMTCTagAttribute, FPMTCTagAttributeArray>(Attributes, DeltaParms, *this);
	}

	PMTCOMMON_API void AddTagAttribute(const FPMTCTagAttribute& InTagAttribute, bool bReplaceOld = false);
	/** Retrieve attribute's current value. */
	PMTCOMMON_API float GetAttributeCurrentValue(FGameplayTag AttributeTag, bool* bExists = nullptr) const;
};

UINTERFACE(MinimalAPI)
class UPMTCTagAttributeHandlerInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface that can be used to be notified about tag attribute changes when replication occurs.
 */
class PMTCOMMON_API IPMTCTagAttributeHandlerInterface
{
	GENERATED_BODY()

public:

	/** Notify that a change ocurred in the attribute. */
	virtual void NotifyTagAttributeChanged(const FPMTCTagAttribute& InTagAttribute) = 0;
};

/**
 * Metadata about modifiers for tag attributes. Used as CDO's to save some data that is replicated.
 */
UCLASS(Blueprintable, BlueprintType)
class PMTCOMMON_API UPMTCTagAttributeModifier : public UObject
{
	GENERATED_BODY()

public:

	/** Modifies the given value. */
	virtual float ModifyValue(float InValue, float ModifierValue) const;
	/** Retrieve modifier's stack limit. */
	int32 GetStackLimit() const { return StackLimit; }
	/** Retrieve modifier's operation type. */
	EPMTCOperationType GetOperationType() const { return Operation; }
	/** Modifier's value if none is provided. Not used in "ModifyValue". */
	float GetModifierValue() const { return ModifierValue; }

protected:

	/** Modifier's unique identifier. */
	UPROPERTY(EditDefaultsOnly)
	FGameplayTag ModifierTagName;
	/** Modifier's operation. */
	UPROPERTY(EditDefaultsOnly)
	EPMTCOperationType Operation = EPMTCOperationType::Additive;
	/** Stack limit. */
	UPROPERTY(EditDefaultsOnly)
	int32 StackLimit = 0;
	/** Base value of the modifier. */
	UPROPERTY(EditDefaultsOnly)
	float ModifierValue = 0.0f;
};

/** Base structure used to replicate any custom data we want. To make use of it
* We need to add the NetSerialize function and the struct's traits.
*
* Follow the next example:
* 
USTRUCT()
struct FMyCustomData : public FPMTCCustomData
{
    GENERATED_BODY()

    public:
      UScriptStruct* GetScriptStruct() const override
      {
        return FMyCustomData::StaticStruct();
      }

      bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
      {
        Ar << MyData;
        return true;
      }

      FString MyData;
};

template<>
struct TStructOpsTypeTraits<FMyCustomData> : public TStructOpsTypeTraitsBase2<FMyCustomData>
{
   enum
   {
     WithNetSerializer = true,
   };
};

*/
USTRUCT()
struct PMTCOMMON_API FPMTCCustomData
{
	GENERATED_BODY()

public:

	FPMTCCustomData () {}

	/** Virtual destructor so we can inherit from this class. */
	virtual ~FPMTCCustomData() {}

	/** Returns the serialization data, must always be overridden */
	virtual UScriptStruct* GetScriptStruct() const
	{
		return FPMTCCustomData::StaticStruct();
	}
};

/** Base structure used to replicate any structure that inherits from FPMTCCustomData. */
USTRUCT(BlueprintType)
struct PMTCOMMON_API FPMTCCustomDataHandle
{
	GENERATED_BODY()

public:

	FPMTCCustomDataHandle () {}

	FPMTCCustomDataHandle(TSharedPtr<FPMTCCustomData> InData) : CustomData(InData) {}

	FPMTCCustomDataHandle(TSharedPtr<FPMTCCustomData>&& InData) : CustomData(InData) {}

	template<typename T>
	bool IsCustomDataA() const
	{
		if (CustomData.IsValid())
		{
			return CustomData->GetScriptStruct()->IsChildOf(T::StaticStruct());
		}
		return false;
	}

	template<typename T>
	const T* GetCustomDataAs() const
	{
		check(IsCustomDataA<T>());
		return static_cast<T*>(GetData().Get());
	}

	/** Kill data. */
	void Clear()
	{
		CustomData.Reset();
	}

	/** Verify if we have any data. */
	bool IsValid() const
	{
		return CustomData.IsValid();
	}

	/** Retrieve custom data. */
	TSharedPtr<FPMTCCustomData> GetData() const
	{
		return CustomData;
	}

	/** Simple equality check. */
	bool operator==(const FPMTCCustomDataHandle& Other) const
	{
		return !(IsValid() != Other.IsValid() || GetData() != Other.GetData());
	}

	/** Serialize for networking, handles polymorphism. */
	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess);

protected:

	/** Actual data that we send. */
	TSharedPtr<FPMTCCustomData> CustomData;
};

template<>
struct TStructOpsTypeTraits<FPMTCCustomDataHandle> : public TStructOpsTypeTraitsBase2<FPMTCCustomDataHandle>
{
	enum
	{
		WithCopy = true, // Necessary so that TSharedPtr<FPMTCCustomData> Data is copied around
		WithNetSerializer = true,
		WithIdenticalViaEquality = true,
	};
};

/** Custom deleter used to delete the created data when replicating. */
struct FPMTCCustomDataDeleter
{
	FORCEINLINE void operator()(FPMTCCustomData* Object) const
	{
		check(Object);
		UScriptStruct* ScriptStruct = Object->GetScriptStruct();
		check(ScriptStruct);
		ScriptStruct->DestroyStruct(Object);
		FMemory::Free(Object);
	}
};

/** Simple helper to make our life easier to create a custom data handle. */
template<typename T, typename... ArgsType>
FPMTCCustomDataHandle CreateCustomData(ArgsType&&... Args)
{
	TSharedPtr<FPMTCCustomData> Data(new T(Forward<ArgsType>(Args)...));
	return FPMTCCustomDataHandle(Data);
}

template<typename T>
struct FPMTCCircularBuffer
{

public:

	FPMTCCircularBuffer() {}

	FPMTCCircularBuffer(const TArray<T>& InData, int32 InMaxElements) : Data(InData), StartIndex(0), MaxElements(InMaxElements)
	{
		check(MaxElements > 0);
		check(InData.Num() <= MaxElements);
	}

	int32 Num() const
	{
		return Data.Num();
	}

	void SetMaxElements(int32 InMaxElements, bool bReserve = true)
	{
		check(Data.Num() < InMaxElements);
		if (bReserve)
		{
			Data.Reserve(InMaxElements);
		}
		MaxElements = InMaxElements;
	}

	/** Verify if all remaining space was used. */
	bool IsFull() const
	{
		return Data.Num() == MaxElements;
	}

	void Reset(int32 Slack = 0)
	{
		Data.Reset(Slack);
		StartIndex = 0;
	}

	template<typename... ArgsType>
	T& EmplaceElement(ArgsType&&... Args)
	{
		if (IsFull())
		{
			const int32 CopyIndex = StartIndex;
			Data[StartIndex] = T(Forward<ArgsType>(Args)...);
			StartIndex = (StartIndex + 1) % MaxElements;
			return Data[CopyIndex];
		}
		return Data.Emplace_GetRef(Forward<ArgsType>(Args)...);
	}

	T& AddElement(const T& InElement)
	{
		if (IsFull())
		{
			const int32 CopyIndex = StartIndex;
			Data[StartIndex] = InElement;
			StartIndex = (StartIndex + 1) % MaxElements;
			return Data[CopyIndex];
		}
		return Data.Add_GetRef(InElement);
	}

	TArray<T> GenerateContiguousArray() const
	{
		TArray<T> ContiguousData;
		ContiguousData.Reserve(MaxElements);

		for (int32 CurrentIndex = StartIndex; CurrentIndex < Data.Num(); ++CurrentIndex)
		{
			ContiguousData.Add(Data[CurrentIndex]);
		}

		for (int32 CurrentIndex = 0; CurrentIndex < StartIndex; ++CurrentIndex)
		{
			ContiguousData.Add(Data[CurrentIndex]);
		}
		return ContiguousData;
	}

private:

	int32 StartIndex = 0;
	int32 MaxElements = 0;
	TArray<T> Data;
};
