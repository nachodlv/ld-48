﻿#pragma once

// PMTCommon Includes
#include "PMTCPooleable.h"

#include "PMTCObjectPooler.generated.h"

typedef TFunction<TScriptInterface<IPMTCPooleable>()> PooleableBuilder;

/** Pool of objects. The objects need to implement IPooleable */
UCLASS()
class PMTCOMMON_API UPMTCObjectPooler : public UObject
{
	GENERATED_BODY()
public:
	UPMTCObjectPooler();

	/**
	 * Instantiates the 'Quantity' amount of objects.
	 * These objects are instantiated with the function 'NewBuilder'
	 */
	void InstantiateInitialObjects(int32 Quantity, PooleableBuilder NewBuilder);

	/** Gets a deactivated object. If there are no more deactivated objects then it will instantiate more */
	template <typename T>
	T* GetObject()
	{
		if (DeActivatedObjects.Num() > 0)
		{
			const TScriptInterface<IPMTCPooleable> PooleableInterface = DeActivatedObjects[0];
			DeActivatedObjects.RemoveAt(0);
			ActivatedObjects.Add(PooleableInterface);
			if (IPMTCPooleable* Pooleable = static_cast<IPMTCPooleable*>(PooleableInterface.GetInterface()))
			{
				Pooleable->Activate();
			}
			return Cast<T>(PooleableInterface.GetObject());
		}

		Grow();
		return GetObject<T>();
	}

private:
	/** Pool of activated objects */
	UPROPERTY(Transient)
	TArray<TScriptInterface<IPMTCPooleable>> ActivatedObjects;

	/** Pool of de activated objects */
	UPROPERTY(Transient)
	TArray<TScriptInterface<IPMTCPooleable>> DeActivatedObjects;

	/** Functions use to instantiate objects */
	PooleableBuilder Builder;

	/** Grows the pool of objects */
	void Grow();

	void ObjectDeactivated(TScriptInterface<IPMTCPooleable> Pooleable);
};
