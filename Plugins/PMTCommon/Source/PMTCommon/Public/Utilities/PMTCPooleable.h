﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "PMTCPooleable.generated.h"

UINTERFACE()
class PMTCOMMON_API UPMTCPooleable : public UInterface
{
	GENERATED_BODY()
};


DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeActivate, TScriptInterface<class IPMTCPooleable>)

/** Provides functionality so the class can be used with the PMTObjectPooler */
class PMTCOMMON_API IPMTCPooleable
{
	GENERATED_BODY()

public:
	virtual bool IsActive() const = 0;

	/** Activates the pooleable. While activated, it cannot be pooled from the ObjectPooler */
	virtual void Activate() { check(!IsActive()); }

	/**
	 * Deactivates the pooleable. While deactivated, it can be pooled from the ObjectPooler.
	 * The override of this method should call the delegate FOnDeActivate
	 */
	virtual void DeActivate() { check(IsActive()); }

	/** Returns the delegate that should be broadcast when the pooleable is deactivated */
	virtual FOnDeActivate GetOnDeactivateDelegate() const = 0;
};
