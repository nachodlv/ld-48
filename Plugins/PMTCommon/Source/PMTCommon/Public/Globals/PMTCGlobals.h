// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PMTCGlobals.generated.h"


#ifdef WITH_EDITOR

/** Simple macro to toggle on/off debugging 3d for any object. */
#define PMTC_REGISTER_UNREGISTER_FOR_3DLOG(Object, Conditional) \
if (Object && Object->GetWorld() && Object->GetWorld()->IsPlayInEditor()) \
{ \
	UPMTCGlobals::RegisterOrUnregisterObjectForDebugging(Object, Conditional); \
}
#else

#define PMTC_REGISTER_UNREGISTER_FOR_3DLOG(Object, Conditional)

#endif // WITH_EDITOR

/** Minimal struct used to completely debug an actor without overlapping text. */
USTRUCT()
struct FPMTCActorDebugging
{
	GENERATED_BODY()

public:

	UPROPERTY(Transient)
	AActor* ActorToDebug = nullptr;
	UPROPERTY(Transient)
	TSet<UActorComponent*> ComponentsToDebug;

	/** Whenever we should debug the actor or not. */
	bool bDebugActor = false;
};

/**
 * Global object for common utilities like debugging.
 */
UCLASS(BlueprintType, Config=Game)
class PMTCOMMON_API UPMTCGlobals : public UObject
{
	GENERATED_BODY()

public:

	UPMTCGlobals();

	/** Get a reference to the common globals. */
	static UPMTCGlobals& Get(); 
	/** Get a reference to the common globals. */
	UFUNCTION(BlueprintPure, Category = "PMTCommon|Globals")
	static UPMTCGlobals* GetPMTCommonGlobals();
	/** Change the time we show debug info. */
	UFUNCTION(BlueprintCallable, meta = (DevelopmentOnly), Category = "PMTCommon|Debugging")
	static void ChangeDebugTime(UObject* WorldContext, float InTime = 0.015f);
	/** Register the given object for debugging. */
	UFUNCTION(BlueprintCallable, meta = (DevelopmentOnly), Category = "PMTCommon|Debugging")
	static void RegisterOrUnregisterObjectForDebugging(UObject* InObject, bool bRegister);

	/** The class to instantiate as the globals object. Defaults to this class but can be overridden */
	UPROPERTY(Config)
	FSoftClassPath PMTCommonClassName;

private:

	/** Check if we have any object to debug. */
	bool HasAnyObjectToDebug(UObject* InWorldContext, bool& bValidWorld) const;

	/** Callback called when the timer ticked. Debug all objects. */
	void OnDebugTimerFinished();

	/** Track actors that required debugging. */
	UPROPERTY(Transient)
	TArray<FPMTCActorDebugging> ActorsToDebug;
	/** Objects that are being debugged. */
	UPROPERTY(Transient)
	TSet<UObject*> DebuggingObjects;
	UPROPERTY(Transient)
	UWorld* CachedWorld = nullptr;

	/** Timer handle used for debugging. */
	FTimerHandle DebugTimerHandle;
	
};
