// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/InputComponent.h"

// PMTC Includes
#include "Utilities/PMTCCommonTypes.h"

#include "PMTCInputListenerComponent.generated.h"

/** Describe a type of event for action events. */
UENUM(BlueprintType)
enum class EPMTCInputActionEventType : uint8
{
	Pressed = 0,
	Released = 1,
	Both = 2,
	Any = 3
};

/** Determines how do we send input to the server. */
UENUM(BlueprintType)
enum class EPMTCInputReplication : uint8
{
	None = 0,
	Reliable = 1,
	Unreliable = 2
};

/** Describes an input press moment. */
USTRUCT(BlueprintType)
struct PMTCOMMON_API FPMTCInputTimestamp
{
	GENERATED_BODY()

public:

	FPMTCInputTimestamp() {}

	FPMTCInputTimestamp(FName InBindingName, float InTime, EInputEvent InInputEvent, float InValue = 0.0f) :
		BindingName(InBindingName), Time(InTime), Value(InValue), InputEvent(InInputEvent) {}

	/** Name of the action (if any). */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName BindingName;
	/** Time that this key was pressed/released. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Time = 0.0;
	/** In case the binding is an axis, then this has the value of the axis. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Value = 0.0f;
	/** Type of the event. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EInputEvent> InputEvent;

	bool IsAxisInput() const { return Value != 0.0f; }
};

/** Describes a set of actions as a sequence of ocurrence. */
USTRUCT(BlueprintType)
struct PMTCOMMON_API FPMTCActionSequence
{
	GENERATED_BODY()

public:

	FPMTCActionSequence() {}

	FPMTCActionSequence(const TArray<FName>& InActions) : Actions(InActions) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> Actions;

	int32 Num() const { return Actions.Num(); }

	FName operator[](int32 Index) const { return Actions[Index]; }

	friend bool operator==(const FPMTCActionSequence& Lhs, const FPMTCActionSequence& Rhs)
	{
		if (Lhs.Num() != Rhs.Num())
		{
			return false;
		}
		for (int32 Index = 0; Index < Lhs.Num(); ++Index)
		{
			if (Lhs[Index] != Rhs[Index])
			{
				return false;
			}
		}
		return true;
	}

	friend bool operator!=(const FPMTCActionSequence& Lhs, const FPMTCActionSequence& Rhs)
	{
		return !(Lhs == Rhs);
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPMTCInputListeningStarted, UPMTCInputListenerComponent*, InputListener);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPMTCInputListeningFinished, UPMTCInputListenerComponent*, InputListener, const TArray<FPMTCInputTimestamp>&, InputWindow);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPMTCInputEvent, UPMTCInputListenerComponent*, InputListener, const FPMTCInputTimestamp&, InputEvent);

/**
 * Input component used to listen for inputs and save the moment they were pressed so
 * it can be used as a "combo system" to look up for keys. This does not consume any input.
 * To use this, you need to define an enum for action and axis mapping like the following.
 * 
 * UENUM(BlueprintType)
 * enum class EMyActionInput : uint8
 * {
 *    Jump = 0,
 *    Kick = 1
 * }
 *
 * Then set ActionInputIdName = "EMyActionInput".
 * The number is not important, but they have to MATCH the actions defined in
 * project settings. This is similar as how GAS work for binding input to ability activation.
 */
UCLASS(NonTransient, BlueprintType, meta = (BlueprintSpawnableComponent))
class PMTCOMMON_API UPMTCInputListenerComponent : public UInputComponent
{
	GENERATED_BODY()

public:

	UPMTCInputListenerComponent();

	/** Simulate an input event. Useful to keep the same functionality for
	* characters and AI characters.
	* @returns True if the input is listening, false otherwise. */
	UFUNCTION(BlueprintCallable, Category = "Input|AI")
	bool SimulateInputEvent(const FPMTCInputTimestamp& InInput);
	/** Push the input listener to the given controller. Reset the input window.
	* Beware that if this is added to a pawn, the input component is added automatically by the controller.
	* So you'll end up adding the same component twice.
	* If the controller is an AI Controller, then this just permits event simulation but nothing else. */
	UFUNCTION(BlueprintCallable)
	void PushInputListener(AController* InController, bool bForcePush = false);
	/** Pop this input listener from the given player controller. */
	UFUNCTION(BlueprintCallable)
	void PopInputListener(AController* InController);
	/** Retrieve the input window. */
	UFUNCTION(BlueprintPure)
	TArray<FPMTCInputTimestamp> GetInputWindow() const { return InputWindow.GenerateContiguousArray(); }
	/** Verifies if we are listening for input. */
	UFUNCTION(BlueprintPure)
	bool IsListening() const;
	/** Verifies if the input window ended with the given action name.
	* @param InActionName - Name of the action we are looking for.
	* @param EventType - Type of the event we want to see at end.
	*	Pressed: If the key was pressed.
	*	Released: if the key was relesead.
	*	Both: The key had to be pressed at anytime but be the last one to be released.
	*	Any: The key had to be pressed or released at the end.
	* @param RequiredTimeToBeHeld How much time was the key held to be considered valid. If
	* 0.0f, then the time is ignored. Only valid for "Both" event type. */
	UFUNCTION(BlueprintPure)
	bool DoesInputWindowEndWithAction(FName InActionName, EPMTCInputActionEventType EventType = EPMTCInputActionEventType::Any, float RequiredTimeToBeHeld = 0.0f) const;
	/** Verify if the lasts actions match exactly the given sequence. */
	UFUNCTION(BlueprintPure)
	bool DoesInputWindowEndWithActionSequence(const FPMTCActionSequence& ActionSequences);

#if WITH_EDITOR
	// Deny "bBlockInput" from setting to true at all.
	bool CanEditChange(const FProperty* InProperty) const override;
#endif // WITH_EDITOR

	/** Delegate called when we start listening for input. */
	UPROPERTY(BlueprintAssignable)
	FPMTCInputListeningStarted StartedListening;
	/** Delegate called when we finished listening for input. */
	UPROPERTY(BlueprintAssignable)
	FPMTCInputListeningFinished FinishedListening;
	/** Delegate called when an input event ocurred. */
	UPROPERTY(BlueprintAssignable)
	FPMTCInputEvent InputOccurred;

protected:

	/** Notify server that we want to start listening for input. */
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void ServerStartListeningInput(AController* InController, float StartTime);
	/** Notify server that we want to stop listening for input. */
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void ServerStopListeningInput(AController* InController, float EndTime);
	/** Send input reliable to the server. */
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void ServerSendInputEventReliable(const FPMTCInputTimestamp& InInput);
	/** Send input unreliable to the server. */
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void ServerSendInputEventUnReliable(const FPMTCInputTimestamp& InInput);

	/** Bind input to listen for events. */
	void InitializeComponent() override;

	/** Callback called when an input key is pressed. */
	virtual void OnActionInputEvent(FName ActionName, EInputEvent InputEvent);
	/** Callback called when an axis key is pressed. */
	virtual void OnAxisInputEvent(float AxisValue, FName AxisName);

	/** Name of the input id we are going to use.*/
	UPROPERTY(EditAnywhere, Category = "Input")
	FString ActionInputIdName;
	/** Name of the input id we are going to use for axis mappings. Leave empty if axis tracking is not required. */
	UPROPERTY(EditAnywhere, Category = "Input")
	FString AxisInputIdName;
	/** Whenever we listen for pressed events. */
	UPROPERTY(EditAnywhere, Category = "Input")
	bool bListenForPressedEvents = true;
	/** Whenever we listen for released events. */
	UPROPERTY(EditAnywhere, Category = "Input")
	bool bListenForReleasedEvents = false;
	/** Determines how we send input replication.
	* None means no replication.
	* Unreliable means that input is sent using unreliable rpcs.
	* Reliable means that input is sent using reliable rpcs. */
	UPROPERTY(EditAnywhere, Category = "Input")
	EPMTCInputReplication InputReplication = EPMTCInputReplication::Unreliable;

	/** Whenever we track an infinite amount of input until we stop listening. */
	UPROPERTY(EditAnywhere, Category = "Limit")
	bool bUnlimitedInputTrack = false;
	/** How much input do we listen to.*/
	UPROPERTY(EditAnywhere, Category = "Limit", meta = (EditCondition = "!bUnlimitedInputTrack"))
	int32 MaxAmountOfInput = 50;
	/** Whenever we overwrite the limit or just stop when we reach MaxAmountOfInput. */
	UPROPERTY(EditAnywhere, Category = "Limit", meta = (EditCondition = "!bUnlimitedInputTrack"))
	bool bOverwriteOnLimit = true;



private:

	/** Verifies if the input comes from a client. */
	bool IsClientInput() const;
	/** Tries to send input to the server if applies. */
	void TryToSendInputToServer(const FPMTCInputTimestamp& InTimestamp);

	/** Input history until we popped the component. */
	FPMTCCircularBuffer<FPMTCInputTimestamp> InputWindow;

	/** Time where we pushed this component. */
	float StartTimePush = 0.0f;
	/** Time where we popped this component. */
	float EndTimePop = 0.0f;
	/** Whenever we are listening for inputs or not. */
	bool bListening = false;
	
};
