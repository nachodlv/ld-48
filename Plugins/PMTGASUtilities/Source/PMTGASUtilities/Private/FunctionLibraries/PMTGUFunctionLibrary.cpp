// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionLibraries/PMTGUFunctionLibrary.h"

// UE Includes
#include "AbilitySystemGlobals.h"

void UPMTGUFunctionLibrary::InitializeGASGlobalData()
{
	UAbilitySystemGlobals& Globals = UAbilitySystemGlobals::Get();
	if (!Globals.IsAbilitySystemGlobalsInitialized())
	{
		Globals.InitGlobalData();
	}
}
