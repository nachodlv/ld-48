// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTGUChainedAbilityRemovalComp.h"

// PMTGU Utilities
#include "Interfaces/PMTGUAbilityNotifierInterface.h"

// UE Includes
#include "AbilitySystemComponent.h"

UPMTGUChainedAbilityRemovalComp::UPMTGUChainedAbilityRemovalComp()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool UPMTGUChainedAbilityRemovalComp::AddChainedAbilities(UAbilitySystemComponent* ASCToGiveAbilities, const TArray<FPMTGUChainAbility>& InChainedAbilities)
{
	check(ASCToGiveAbilities);
	for (const FPMTGUChainAbility& ChainAbility : InChainedAbilities)
	{
		if (ChainAbility.ChainedAbilities.Num() == 0)
		{
			continue;
		}
		FGameplayAbilitySpec* Spec = ASCToGiveAbilities->FindAbilitySpecFromHandle(ChainAbility.MainAbilitySpecHandle);
		check(Spec);
		check(Spec->GetPrimaryInstance());
		IPMTGUAbilityNotifierInterface* NotifierInterface = Cast<IPMTGUAbilityNotifierInterface>(Spec->GetPrimaryInstance());
		if (NotifierInterface)
		{
			NotifierInterface->GetAbilityRemovedDelegate().BindUObject(this, &UPMTGUChainedAbilityRemovalComp::OnAbilityRemoved);
			TArray<FGameplayAbilitySpecHandle> GrantedAbilityHandles;
			GrantedAbilityHandles.Reserve(ChainAbility.ChainedAbilities.Num());
			for (const FGameplayAbilitySpec& ChainedSpec : ChainAbility.ChainedAbilities)
			{
				GrantedAbilityHandles.Emplace(ASCToGiveAbilities->GiveAbility(ChainedSpec));
			}
			ChainedAbilities.Add(ChainAbility.MainAbilitySpecHandle, GrantedAbilityHandles);
		}
	}
	return true;
}

void UPMTGUChainedAbilityRemovalComp::OnAbilityRemoved(UAbilitySystemComponent* ASC, FGameplayAbilitySpecHandle Handle)
{
	check(ASC);
	for (FGameplayAbilitySpecHandle Spec : ChainedAbilities[Handle])
	{
		ASC->ClearAbility(Spec);
	}
	ChainedAbilities.Remove(Handle);
}
