// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTGUAbilityRestorerComp.h"

// UE Includes
#include "AbilitySystemComponent.h"

UPMTGUAbilityRestorerComp::UPMTGUAbilityRestorerComp()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UPMTGUAbilityRestorerComp::StoreAbilities()
{
	if (OwnerASC)
	{
		StoredAbilities = OwnerASC->GetActivatableAbilities();
	}
}

void UPMTGUAbilityRestorerComp::ReplaceAbilities(const TArray<FGameplayAbilitySpec>& AbilitiesToReplace, bool bStoreAbilities)
{
	// Can't grant or clear an ability if we are not the authority.
	if (!OwnerASC || !OwnerASC->IsOwnerActorAuthoritative())
	{
		return;
	}
	for (const FGameplayAbilitySpec& AbilityToReplace : AbilitiesToReplace)
	{
		int32 OutIndex;
		FGameplayAbilitySpec MatchedAbility = GetMatchedAbility(AbilityToReplace, OutIndex);
		if (MatchedAbility.Ability)
		{
			OwnerASC->ClearAbility(MatchedAbility.Handle);
		}
		// Make sure to give the ability even if we don't have one.
		OwnerASC->GiveAbility(AbilityToReplace);
		if (bStoreAbilities)
		{
			StoredAbilities[OutIndex] = AbilityToReplace;
		}
	}
}

void UPMTGUAbilityRestorerComp::RestoreAbilities()
{
	// Can't grant or clear an ability if we are not the authority.
	if (!OwnerASC || !OwnerASC->IsOwnerActorAuthoritative())
	{
		return;
	}
	TArray<FGameplayAbilitySpec> CopyActivatableAbilities = OwnerASC->GetActivatableAbilities();
	for (const FGameplayAbilitySpec& AbilityToReplace : CopyActivatableAbilities)
	{
		int32 OutIndex;
		FGameplayAbilitySpec MatchedAbility = GetMatchedAbility(AbilityToReplace, OutIndex);
		// If we found one, it means it was one of the stored abilities, then don't do anything with it.
		if (MatchedAbility.Handle == AbilityToReplace.Handle)
		{
			continue;
		}
		OwnerASC->ClearAbility(AbilityToReplace.Handle);
		if (MatchedAbility.Ability)
		{
			OwnerASC->GiveAbility(MatchedAbility);
		}
	}
}

void UPMTGUAbilityRestorerComp::InitializeComponent()
{
	Super::InitializeComponent();
	OwnerASC = GetOwner()->FindComponentByClass<UAbilitySystemComponent>();
	ensureAlwaysMsgf(OwnerASC, TEXT("'%s': This component does not make sense if the owner '%s' does not contain an ASC."), *GetName(), *GetOwner()->GetName());
}

FGameplayAbilitySpec UPMTGUAbilityRestorerComp::GetMatchedAbility(const FGameplayAbilitySpec& InAbilitySpec, int32& OutIndex) const
{
	int32 Index = 0;
	for (const FGameplayAbilitySpec& AbilityStored : StoredAbilities)
	{
		if (AbilityStored.InputID == InAbilitySpec.InputID &&
			AbilityStored.Ability->AbilityTags == InAbilitySpec.Ability->AbilityTags)
		{
			OutIndex = Index;
			return AbilityStored;
		}
		++Index;
	}
	OutIndex = INDEX_NONE;
	return FGameplayAbilitySpec();
}
