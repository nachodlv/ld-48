// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilityTasks/PMTGUWaitTargetData.h"

// UE Includes
#include "AbilitySystemComponent.h"

UPMTGUWaitTargetData::UPMTGUWaitTargetData()
{}

UPMTGUWaitTargetData* UPMTGUWaitTargetData::WaitClientTargetData(UGameplayAbility* OwningAbility, FName TaskInstanceName, bool bInTriggerOnce)
{
	UPMTGUWaitTargetData* MyObj = NewAbilityTask<UPMTGUWaitTargetData>(OwningAbility, TaskInstanceName);
	MyObj->bTriggerOnce = bInTriggerOnce;
	return MyObj;
}

void UPMTGUWaitTargetData::Activate()
{
	Super::Activate();
	if (!Ability || !Ability->GetCurrentActorInfo()->IsNetAuthority())
	{
		return;
	}

	FGameplayAbilitySpecHandle	SpecHandle = GetAbilitySpecHandle();
	FPredictionKey ActivationPredictionKey = GetActivationPredictionKey();
	TargetDataDelegateHandle =
		AbilitySystemComponent->AbilityTargetDataSetDelegate(SpecHandle, ActivationPredictionKey).AddUObject(this, &UPMTGUWaitTargetData::OnTargetDataReplicatedCallback);
}

void UPMTGUWaitTargetData::OnTargetDataReplicatedCallback(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag)
{
	AbilitySystemComponent->ConsumeClientReplicatedTargetData(GetAbilitySpecHandle(), GetActivationPredictionKey());
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		ValidData.Broadcast(Data);
	}
	if (bTriggerOnce)
	{
		EndTask();
	}
}

void UPMTGUWaitTargetData::OnDestroy(bool AbilityEnded)
{
	if (AbilitySystemComponent)
	{
		FGameplayAbilitySpecHandle	SpecHandle = GetAbilitySpecHandle();
		FPredictionKey ActivationPredictionKey = GetActivationPredictionKey();
		AbilitySystemComponent->AbilityTargetDataSetDelegate(SpecHandle, ActivationPredictionKey).Remove(TargetDataDelegateHandle);
	}
	Super::OnDestroy(AbilityEnded);
}
