// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilityTasks/PMTGUWaitInputReleaseFeedback.h"

UPMTGUWaitInputReleaseFeedback::UPMTGUWaitInputReleaseFeedback()
{
	bTickingTask = true;
}

UPMTGUWaitInputReleaseFeedback* UPMTGUWaitInputReleaseFeedback::WaitInputReleaseFeedback(
	UGameplayAbility* OwningAbility, float InTimeToEndAutomatically, bool bTestAlreadyReleased)
{
	UPMTGUWaitInputReleaseFeedback* Task = NewAbilityTask<UPMTGUWaitInputReleaseFeedback>(OwningAbility);
	Task->bTestInitialState = bTestAlreadyReleased;
	Task->AccumulatedTime = 0.0f;
	Task->TimeToEndAutomatically = InTimeToEndAutomatically;
	return Task;
}

bool UPMTGUWaitInputReleaseFeedback::DidTimedOut() const
{
	return bTimedOut;
}

void UPMTGUWaitInputReleaseFeedback::TickTask(float DeltaTime)
{
	Super::TickTask(DeltaTime);

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		AccumulatedTime += DeltaTime;
		InputReleaseFeedback.Broadcast(AccumulatedTime);
		if (TimeToEndAutomatically > 0.0f && TimeToEndAutomatically >= AccumulatedTime)
		{
			bTimedOut = true;
			// Kill task.
			OnReleaseCallback();
		}
	}
}
