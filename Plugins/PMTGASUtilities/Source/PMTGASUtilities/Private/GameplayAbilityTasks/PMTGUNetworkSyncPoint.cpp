// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameplayAbilityTasks/PMTGUNetworkSyncPoint.h"

UPMTGUNetworkSyncPoint::UPMTGUNetworkSyncPoint()
{
	bTickingTask = true;
}

UPMTGUNetworkSyncPoint* UPMTGUNetworkSyncPoint::WaitPMTGUNetSync(UGameplayAbility* OwningAbility, EAbilityTaskNetSyncType InSyncType, float InTimeout)
{
	UPMTGUNetworkSyncPoint* MyObj = NewAbilityTask<UPMTGUNetworkSyncPoint>(OwningAbility);
	MyObj->SyncType = InSyncType;
	MyObj->Timeout = InTimeout;
	return MyObj;
}

void UPMTGUNetworkSyncPoint::TickTask(float DeltaTime)
{
	Super::TickTask(DeltaTime);
	AccumulatedTime += DeltaTime;

	if (Timeout > 0.0f && AccumulatedTime >= Timeout)
	{
		UE_LOG(LogTemp, Warning, TEXT("'%s' timeout"), *GetName());
		SyncTimeout.Broadcast();
		EndTask();
	}
}
