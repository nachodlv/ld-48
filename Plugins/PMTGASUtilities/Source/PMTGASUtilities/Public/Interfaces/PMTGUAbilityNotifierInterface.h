// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "GameplayAbilitySpec.h"

#include "PMTGUAbilityNotifierInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPMTGUAbilityNotifierInterface : public UInterface
{
	GENERATED_BODY()
};

class UAbilitySystemComponent;

DECLARE_DELEGATE_TwoParams(FPMTGUAbilityRemovedDelegate, UAbilitySystemComponent*, FGameplayAbilitySpecHandle);

/**
 * Interface used for abilities that want to notify when they are being removed or added.
 */
class PMTGASUTILITIES_API IPMTGUAbilityNotifierInterface
{
	GENERATED_BODY()

public:

	virtual FPMTGUAbilityRemovedDelegate& GetAbilityRemovedDelegate() = 0;
};
