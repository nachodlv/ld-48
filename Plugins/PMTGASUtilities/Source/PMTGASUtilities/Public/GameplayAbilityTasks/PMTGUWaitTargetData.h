// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "PMTGUWaitTargetData.generated.h"

/**
 * Wait for client to produce target data.
 */
UCLASS()
class PMTGASUTILITIES_API UPMTGUWaitTargetData : public UAbilityTask
{
	GENERATED_BODY()

public:

	UPMTGUWaitTargetData();

	/** Make the server wait for client target data. */
	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", HideSpawnParms = "Instigator"), Category = "PMTGU|Ability|Tasks")
	static UPMTGUWaitTargetData* WaitClientTargetData(UGameplayAbility* OwningAbility, FName TaskInstanceName, bool bInTriggerOnce = true);

	void Activate() override;
	
	UPROPERTY(BlueprintAssignable)
	FWaitTargetDataDelegate ValidData;

protected:

	/** Broadcast valid data delegate if valid. */
	UFUNCTION()
	void OnTargetDataReplicatedCallback(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag);
	/** Unbind delegates. */
	void OnDestroy(bool AbilityEnded) override;
	
	/** Whenever the task listen for target data infinitely or just once. */
	bool bTriggerOnce = false;

	/** Handle used to unbind the target data delegate. */
	FDelegateHandle TargetDataDelegateHandle;
};
