// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask_NetworkSyncPoint.h"
#include "PMTGUNetworkSyncPoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPMTGUSyncTimeoutDelegate);

/**
 * Sync point that gives the possibility to timeout.
 * This is useful since the base class will wait infinitely for the client to notify the server
 * if the latter is waiting.
 */
UCLASS()
class PMTGASUTILITIES_API UPMTGUNetworkSyncPoint : public UAbilityTask_NetworkSyncPoint
{
	GENERATED_BODY()

public:

	/** Tick task. */
	UPMTGUNetworkSyncPoint();

	/**
	 *
	 *	Synchronize execution flow between Client and Server. Depending on SyncType, the Client and Server will wait for the other to reach this node or another WaitNetSync node in the ability before continuing execution.
	 *
	 *	BothWait - Both Client and Server will wait until the other reaches the node. (Whoever gets their first, waits for the other before continueing).
	 *	OnlyServerWait - Only server will wait for the client signal. Client will signal and immediately continue without waiting to hear from Server.
	 *	OnlyClientWait - Only client will wait for the server signal. Server will signal and immediately continue without waiting to hear from Client.
	 *
	 *	Note that this is "ability instance wide". These sync points never affect sync points in other abilities.
	 *
	 *	In most cases you will have both client and server execution paths connected to the same WaitNetSync node. However it is possible to use separate nodes
	 *	for cleanliness of the graph. The "signal" is "ability instance wide".
	 *
	 *  Timeout param ends this sync point if the time is reached. This is useful to not make the server wait infinitely by a malicious client. A value of 0.0 means no timeout.
	 *
	 */
	UFUNCTION(BlueprintCallable, Category = "PMTGU|Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UPMTGUNetworkSyncPoint* WaitPMTGUNetSync(UGameplayAbility* OwningAbility, EAbilityTaskNetSyncType InSyncType, float InTimeout = 0.0f);

	/** Delegate fired when a timeout occured. */
	UPROPERTY(BlueprintAssignable)
	FPMTGUSyncTimeoutDelegate SyncTimeout;

protected:

	/** Take into account the elapsed time to timeout. */
	void TickTask(float DeltaTime) override;


	/** How much time does the sync point will wait. */
	float Timeout = 0.0f;
	/** Time accumulated of the sync point. */
	float AccumulatedTime = 0.0f;
	
};
