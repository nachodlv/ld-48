// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "PMTGUWaitInputReleaseFeedback.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWaitInputReleaseFeedbackDelegate, float, SecondsElapsed);

/**
 * Ability task that notifies each tick for accumulated time.
 */
UCLASS()
class PMTGASUTILITIES_API UPMTGUWaitInputReleaseFeedback : public UAbilityTask_WaitInputRelease
{
	GENERATED_BODY()

public:

	/** Set task to tick. */
	UPMTGUWaitInputReleaseFeedback();

	/** Create a task that waits for the release event and also broadcasts an event each time it ticks.
	* @param OwningAbility - Ability that owns this task.
	* @param InTimeToEndAutomatically - Whenever we want to release the key when the task reaches the given time. 0.0 means that it doesn't have a limited time.
	* @param bTestAlreadyReleased - Verify if the input was already released when the task was created. */
	UFUNCTION(BlueprintCallable, Category = "PMTGU|Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UPMTGUWaitInputReleaseFeedback* WaitInputReleaseFeedback(UGameplayAbility* OwningAbility, float InTimeToEndAutomatically = 0.0f, bool bTestAlreadyReleased = false);

	/** Verifies if the task timed out. */
	bool DidTimedOut() const;

	/** Delegate fired each time the task ticks. */
	UPROPERTY(BlueprintAssignable)
	FOnWaitInputReleaseFeedbackDelegate InputReleaseFeedback;

protected:

	/** Broadcast InputReleaseFeedback delegate. */
	void TickTask(float DeltaTime) override;

private:

	/** Track the amount of time it has elapsed since we started the task. */
	float AccumulatedTime = 0.0f;
	/** Time to end the task automatically. */
	float TimeToEndAutomatically = 0.0f;
	/** Whenever the task timed out. */
	bool bTimedOut = false;
};
