// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PMTGUFunctionLibrary.generated.h"

/**
 * Utility functions for general stuff about GAS.
 */
UCLASS()
class PMTGASUTILITIES_API UPMTGUFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Function exposed to blueprints to initialize global data for ability system globals. */
	UFUNCTION(BlueprintCallable)
	static void InitializeGASGlobalData();
	
};
