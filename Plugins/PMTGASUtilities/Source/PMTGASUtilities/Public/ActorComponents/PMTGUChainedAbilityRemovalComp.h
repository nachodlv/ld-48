// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "GameplayAbilitySpec.h"

#include "PMTGUChainedAbilityRemovalComp.generated.h"

class UAbilitySystemComponent;

/** Simple structure that relates an ability spec to an array of abilities. */
USTRUCT()
struct FPMTGUChainAbility
{
	GENERATED_BODY()
public:

	FPMTGUChainAbility() {}

	FPMTGUChainAbility(FGameplayAbilitySpecHandle InSpecHandle, const TArray<FGameplayAbilitySpec>& InChainedAbilities) :
		MainAbilitySpecHandle(InSpecHandle), ChainedAbilities(InChainedAbilities) {}

	FGameplayAbilitySpecHandle MainAbilitySpecHandle;
	TArray<FGameplayAbilitySpec> ChainedAbilities;
};

/** Component that listen for ability removal to remove other abilities
* (used when there's no gameplay effect involved). */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTGASUTILITIES_API UPMTGUChainedAbilityRemovalComp : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPMTGUChainedAbilityRemovalComp();

	/** Add abilities to the given ASC that will be removed when the main ability is removed. */
	bool AddChainedAbilities(UAbilitySystemComponent* ASCToGiveAbilities, const TArray<FPMTGUChainAbility>& InChainedAbilities);

protected:

	virtual void OnAbilityRemoved(UAbilitySystemComponent* ASC, FGameplayAbilitySpecHandle Handle);

private:

	TMap<FGameplayAbilitySpecHandle, TArray<FGameplayAbilitySpecHandle>> ChainedAbilities;

};
