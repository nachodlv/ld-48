// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "GameplayAbilitySpec.h"

#include "PMTGUAbilityRestorerComp.generated.h"

class UGameplayAbility;

class UAbilitySystemComponent;

/** Component that manages the change of abilities for a character with an ASC.
* You need to give the base ability set that has to store and then any ability
* should be added by using this component instead so we can recover the abilities
* if needed. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTGASUTILITIES_API UPMTGUAbilityRestorerComp : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPMTGUAbilityRestorerComp();

	/** Store all the abilities that are present in the ASC.  */
	UFUNCTION(BlueprintCallable, Category = "Ability Restorer")
	void StoreAbilities();
	/** Store owner abilities and replace them with the given ones. if bStoreAbilities is false, then
	* the new abilities will be overriden and the old ones discarded. */
	UFUNCTION(BlueprintCallable, Category = "Ability Restorer")
	void ReplaceAbilities(const TArray<FGameplayAbilitySpec>& AbilitiesToReplace, bool bStoreAbilities = true);
	/** Restore the saved abilities on the OnwerASC. */
	UFUNCTION(BlueprintCallable, Category = "Ability Restorer")
	void RestoreAbilities();

protected:

	/** Cache the owner ASC. */
	void InitializeComponent() override;

private:

	/** Return the stored ability that matches the given one by verifying that the input id and ability tags are the same. */
	FGameplayAbilitySpec GetMatchedAbility(const FGameplayAbilitySpec& InAbilitySpec, int32& OutIndex) const;

	/** Ability system component that we will track for abilities. */
	UPROPERTY(Transient)
	UAbilitySystemComponent* OwnerASC = nullptr;

	/** Abilities that we will restore if requested. */
	UPROPERTY(Transient)
	TArray<FGameplayAbilitySpec> StoredAbilities;
};
