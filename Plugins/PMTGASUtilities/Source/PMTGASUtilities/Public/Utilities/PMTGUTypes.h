// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PMTGUTypes.generated.h"

/**
 * 
 */
UCLASS()
class PMTGASUTILITIES_API UPMTGUTypes : public UObject
{
	GENERATED_BODY()
	
};
